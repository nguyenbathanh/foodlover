module Mongoid
  module Document
    module TaggableOn
      def self.included(base)
        base.class_eval do |base1|
          include InstanceMethods
          extend ClassMethods          
        end
      end
      
      module InstanceMethods        
      end

      module ClassMethods
        def cw_taggable_on(*fields)
          initialize_taggable_on_attrs(*fields)
        end
        
        def initialize_taggable_on_attrs(*fields)
          class_attribute :tag_types
          self.tag_types = fields
          
          fields = fields.to_a.flatten.compact.map(&:to_sym)
          fields.each do |field|            
            class_eval %(
              self.field "#{field.to_s}_tag_list".to_sym, :type => Array
              self.index "#{field.to_s}_tag_list".to_sym
              
              def #{field.to_s.pluralize}
                # self.class.all.only("#{field.to_s}_tag_list".to_sym).collect{ |ms| ms.send("#{field.to_s}_tag_list") }.flatten.uniq.compact
                send("#{field.to_s}_tag_list") || []
                # self[:#{field.to_s}_tag_list] || []
              end
              
              def #{field.to_s}_list
                # NOTE: I need to change this, because I can't use it like an array, and I need id
                self[:#{field.to_s.pluralize}].join(", ")
                # self[:#{field.to_s}_tag_list] = [] if not self[:#{field.to_s}_tag_list].kind_of? Array
                # self[:#{field.to_s}_tag_list]
              end

              def #{field.to_s}_list=(tags)
                variable_name = "@#{field.to_s}_tag_list"
                tags = [tags] unless tags.is_a? Array
                # instance_variable_set(variable_name, tags.each.collect{ |t| t.strip }.delete_if{ |t| t.blank? })

                # NOTE: Don't work, now we are defining #{field.to_s}_tag_list function, we can't use it
                self.#{field.to_s}_tag_list = tags.each.collect{ |t| t.strip }.delete_if{ |t| t.blank? }
                
                # self[:#{field.to_s}_tag_list] = tags.each.collect{ |t| t.strip }.delete_if{ |t| t.blank? }
              end
              
              def #{field.to_s.pluralize}=(tags)
                variable_name = "@#{field.to_s}_tag_list"
                tags = [tags] unless tags.is_a? Array
                # instance_variable_set(variable_name, tags.each.collect{ |t| t.strip }.delete_if{ |t| t.blank? })
                
                # NOTE: Don't work, now we are defining #{field.to_s}_tag_list function, we can't use it
                self.#{field.to_s}_tag_list = tags.each.collect{ |t| t.strip }.delete_if{ |t| t.blank? }

                # self[:#{field.to_s}_tag_list] = tags.each.collect{ |t| t.strip }.delete_if{ |t| t.blank? }
              end
            )
          end
        end
        
        def tagged_with(_tags, options = {})
          return [] if _tags.empty?
          _tags = [_tags] unless _tags.is_a? Array
          context = options.delete(:on)
          if context.blank?
            criteria = []
            self.tag_types.each {|t| criteria.push({"#{t.to_s}_tag_list".to_sym => {"$in" => _tags}})}
            puts criteria.join(",")
            self.criteria.any_of(criteria)
          else
            self.criteria.in("#{context}_tag_list" => _tags)
          end
        end
      end
    end
  end
end
