Feature: Foodpedia listing
  As a user
  I want foodpedia to present information about foods

  Background:
    Given a food

  Scenario: View food page with feed of corresponding meals
    When I go to the food page
    Then I should see food information
    Then I should see food statistics
    And I should see food posts