Given /^a food$/ do
  @food = FactoryGirl.create(:food)
end

Then /^I should see food information$/ do
  within("#food section") do
    should have_css('h2', text: @food.name.humanize)
    should have_content(@food.description)
    should have_link("Wikipedia", href: @food.external_uri)
  end
end

#TODO implement more granular testing

Then /^I should see food statistics$/ do
  within("#food aside") do
    should have_css('li.likes')
    should have_css('li.subscribers')
    should have_css('li.similar-foods')
  end
end

Then /^I should see food posts$/ do
  within("#posts") do
    should have_css('ul.posts')
  end
end
