require 'spork'

Spork.prefork do
  require 'cucumber/rails'
  require 'capybara/poltergeist'

  Capybara.default_selector = :css
  Capybara.register_driver :poltergeist do |app|
    Capybara::Poltergeist::Driver.new(app)#, debug: true, inspector: true)
  end
  Capybara.javascript_driver = :poltergeist
  ActionController::Base.allow_rescue = false

  begin
    DatabaseCleaner.strategy = :truncation
  rescue NameError
    raise "You need to add database_cleaner to your Gemfile (in the :test group) if you wish to use it."
  end
end

Spork.each_run do
  FactoryGirl.reload
end
