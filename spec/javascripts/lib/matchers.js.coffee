beforeEach ->
  @addMatchers
    toBeEmptyObject: -> this.actual.length == 0
    toIncludeValue: (value) -> _.include @actual, value
