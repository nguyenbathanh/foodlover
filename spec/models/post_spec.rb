require 'spec_helper'

describe Post do
  let(:post) { FactoryGirl.create(:post) }

  it { should have_fields(:food_names, :message) }
  it { should belong_to(:user) }
  it { should belong_to(:picture) }
  it { should belong_to(:album) }
  it { should have_and_belong_to_many(:foods) }

  describe "parse food_names" do
    let(:food) { FactoryGirl.create(:food, name: "Hamburger") }
    let(:post) { FactoryGirl.build(:post, food_names: "Hamburger,Pepperoni Pizza") }

    before(:each) do
      food
      post.stub!(:add_to_timeline)
    end

    it "should associate with existing food with matching name" do
      post.save!
      post.foods.should include(food)
    end

    it "should create new food with passed name" do
      post.save!
      pizza = Food.where(name: "Pepperoni Pizza").first
      post.foods.should include(pizza)
    end
  end
end
