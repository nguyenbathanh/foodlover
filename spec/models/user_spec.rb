require 'spec_helper'

describe User do
  subject { FactoryGirl.build(:user) }

  it { should be_timestamped_document }

  it { should have_many(:posts) }
end
