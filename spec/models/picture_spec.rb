require 'spec_helper'

describe Picture do
  subject { FactoryGirl.build(:picture) }

  it { should have_fields :file_uid }
  it { should have_one(:post) }
  it { should belong_to(:user) }
end
