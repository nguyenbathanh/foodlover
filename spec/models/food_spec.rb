require 'spec_helper'

describe Food do
  subject { FactoryGirl.build(:food) }

  it { should have_fields :name, :description, :external_uri, :popularity_score }
  it { should have_many(:subscriptions)}

  it { should validate_presence_of(:name) }

  describe "popularity score" do
    it "should be updated when subscription created"
    it "should be updated when subscription deleted"
    it "should be updated when post tagged"
    it "should be updated when post untagged"
    it "should be updated when post deleted"
  end
end
