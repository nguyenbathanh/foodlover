FactoryGirl.define do
  sequence :email do |n|
    "person#{n}@example.com"
  end

  factory :user do
    username 'John Doe'
    email
    password 'password'
    password_confirmation 'password'
  end
end
