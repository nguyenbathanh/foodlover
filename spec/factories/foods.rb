FactoryGirl.define do
  factory :food do
    name "hot dog"
    description "A hot dog is a sausage served in a sliced bun. It is very often garnished with mustard, ketchup, onions, mayonnaise, relish, and/or sauerkraut."
    external_uri "http://en.wikipedia.org/wiki/Hot_dog"
  end
end
