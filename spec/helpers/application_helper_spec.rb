require 'spec_helper'

describe ApplicationHelper do
  describe '#page_class' do
    it "should return controller and action name concatenated" do
      controller.should_receive(:controller_name).and_return('foo')
      controller.should_receive(:action_name).and_return('bar')
      page_class.should == 'foo bar'
    end
  end
end