require 'faker'

# user
Factory.sequence :email do |n|
  "new#{n}@email.com"
end

Factory.sequence :username do |n|
  "user#{n}"
end

Factory.sequence :facebook_id do |i|
  i
end

Factory.define :user do |u|
  u.email { Factory.next :email }
  u.password 'password'
  u.username { Factory.next :username }
  u.profile_image { File.new("#{Rails.root}/db/profile/#{rand(8) + 1}.jpeg") }
end

Factory.define :post do |p|
  p.message { Faker::Lorem.sentence }
  p.image {File.new("#{Rails.root}/db/post/#{rand(1) +1}.jpeg")}
end

Factory.define :outlet do |o|
  o.username { Faker::Name.name }
  o.description { Faker::Lorem.paragraph }
  o.profile_image {File.new("#{Rails.root}/db/outlet/1.jpeg")}
end

Factory.define :badge do
end