#TODO remove dummy data from this file
#this should naturally be used for seeding basic things like default categories, zip codes

require 'factory_girl'
require './factories.rb'

User.delete_all
Post.delete_all
Outlet.delete_all
Relationship.delete_all
Conversation.delete_all
Message.delete_all


puts 'CREATE FIRST USER'
User.create!(email: "demo@foodstreet.com", password: "123456", username: "demo",
             password_confirmation: "123456", facebook_id: 775417639,
             profile_image: open("http://graph.facebook.com/775417639/picture").read)
puts 'SUCCESS'

puts 'CREATE SECOND USER'
User.create!(email: "demo2@foodstreet.com", password: "123456", username: "demo2",
             password_confirmation: "123456", facebook_id: 1235013640,
             profile_image: open("http://graph.facebook.com/1235013640/picture").read)
puts 'SUCCESS'

puts 'CREATE THIRD USER'
User.create!(email: "demo3@foodstreet.com", password: "123456", username: "demo3",
             password_confirmation: "123456", facebook_id: 558439479,
             profile_image: open("http://graph.facebook.com/558439479/picture").read)
puts 'SUCCESS'

puts 'CREATE FOURTH USER'
User.create!(email: "demo4@foodstreet.com", password: "123456", username: "demo4",
             password_confirmation: "123456", facebook_id: 100000378619378,
             profile_image: open("http://graph.facebook.com/100000378619378/picture").read)
puts 'SUCCESS'

puts 'CREATE 30 USERS...'
30.times do
  user = Factory.build(:user)
  user.save!
end
puts 'SUCCESS'

puts 'EVERY USER FOLLOWS THE FIRST USER'
user1 = User.where(username: 'demo').first
users = User.all
users.each do |u|
  u.follow! user1
end
puts 'SUCCESS'

puts 'FIRST USER FOLLOWS EVERY USER'
users.each do |u|
  user1.follow! u
end
puts 'SUCCESS'

puts 'CREATE 5 OUTLETS'
5.times do
  outlet = Factory.build(:outlet)
  outlet.save!
end
puts 'SUCCESS'
