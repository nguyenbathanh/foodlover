(function() {
  _.extend(Backbone.History.prototype, {
    route : function(router, route, callback) {
      this.handlers.unshift({router : router, route : route, callback : callback});
    },

    loadUrl : function() {
      var fragment = this.fragment = this.getFragment();
      var matched = false;
      // To keep track of called routers, to prevent problem like, in one router:
      // "/new" < get called
      // "/:id" < get called
      // Now, there is only one action in one particular router gets called.
      var matched_routers = [];
      _.each(this.handlers, function(handler) {
        if (!(_.include(matched_routers, handler.router)) && handler.route.test(fragment)) {
          matched_routers.push(handler.router);
          handler.callback(fragment);
          matched = true;
        }
      });
      return matched;
    }
  });
}).call(this);

