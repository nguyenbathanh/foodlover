 jQuery(document).ready ->
#  Like
  jQuery('.post_like').click ->
    if (window.current_user_id == '')
      window.location.href = '/login'
    else
      $this = jQuery('.post_like')
      $commentList = jQuery('ul.comments')
      params =
        url: '/like'
        type: 'POST'
        data: "id=" + jQuery('.stream_item.clearfix').attr('id')
        async: true
        success: (data) ->
          $this.html("unlike")
          like_counter = jQuery('span').closest(".like_count", $this)
          new_count = parseInt(like_counter.text().split(' ')[0]) + 1
          like_counter.text(new_count + ' Liked')
          $this.addClass("post_unlike")
          $this.removeClass("post_like")
          statusHTML = "
            <li class=\"status animation fadeIn\">You like this.</li>
          "
          $commentList.prepend(statusHTML)
        error: (data) ->
          alert "error"
      $.ajax params
      return false

# Unlike
  jQuery('.post_unlike').click ->
    $this = jQuery('.post_unlike')
    $commentList = jQuery('ul.comments')
    params =
      url: '/like'
      type: 'delete'
      data: "id=" + jQuery('.stream_item.clearfix').attr('id')
      async: true
      success: (data) ->
        $this.html("Like")
        like_counter = jQuery('span').closest(".like_count", $this)
        new_count = parseInt(like_counter.text().split(' ')[0]) - 1
        like_counter.text(new_count + ' Liked')
        $this.addClass("post_like")
        $this.removeClass("post_unlike")
        $commentList.find("li.status").hide()
      error: (data) ->
        alert "error"

    $.ajax params
    return false
# Comment Post
  jQuery('.comment_input').click ->
    if (window.current_user_id == '')
      window.location.href = '/login'
    else
      if jQuery('.comment_input').val() != ""
        if (event.which == 13)
          $commentList = jQuery('ul.comments')
          message = jQuery('.comment_input').val()
          params =
            url: '/comments'
            type: 'POST'
            data: 'id=' + jQuery('.stream_item.clearfix').attr('id') + ";message=" + message
            success: (data) ->
              commentHTML = "
                <li class=\"comment_entry animation fadeIn new\">
                  <span class=\"commentor top\">
                    <a href=\"#\"><img src=\"#{data.image_32}\" /></a>
                  </span><span class=\"comment_message top\">
                    <div class=\"comment\">
                      <a href=\"#\" class=\"commentor_name\">#{data.author}</a><span class=\"message\">#{data.message}</span>
                    </div>
                    <div class=\"footer\">#{data.created_at_time}</div>
                  </span>
                </li>
              "
              $commentList.append(commentHTML)
              t = setTimeout("jQuery('li.new').removeClass('new')", 4000)
              jQuery('.comment_input').val("")
              comment_counter = jQuery('span').closest(".comment_count", $commentList)
              new_count = parseInt(comment_counter.text().split(' ')[0]) + 1
              if (new_count == 1)
                counter_object = ' Comment'
              else
                counter_object = ' Comments'
              comment_counter.text(new_count + counter_object)
            error: (data) ->
              alert "error"
      
          $.ajax params
          return false

# Reshare post
  jQuery('.post_reshare').click ->
    $this = jQuery('.post_reshare')
    $commentList = jQuery('ul.comments')
    $reshare_counter = jQuery('.reshare_count')
    params =
      url: '/posts/' + jQuery('.stream_item.clearfix').attr('id') + '/reshare'
      type: 'POST'
      async: true
      success: (data) ->
        statusHTML = "
          <li class=\"status animation fadeIn\">You reshare this.</li>
        "
        $commentList.prepend(statusHTML)
        new_count = parseInt($reshare_counter.text().split(' ')[0]) + 1
        $reshare_counter.text(new_count + ' Reshared')
      error: (data) ->
        alert "error"

    $.ajax params
    return false

# Repin post
  jQuery('.post_repin').click ->
    $.colorbox({href:"/posts/" + jQuery('.stream_item.clearfix').attr('id') + "/repin"})
    return false
# Flag post
  jQuery('.post_flag').click ->
    $.colorbox({href:"/posts/" + jQuery('.stream_item.clearfix').attr('id') + "/flag"})
    return false
# Promote post
  jQuery('.post_promotion').click ->
    $this = jQuery('.post_promotion')
    $commentList = jQuery('ul.comments')
    params =
      url: '/posts/' + jQuery('.stream_item.clearfix').attr('id') + '/promote'
      type: 'POST'
      async: true
      success: (data) ->
        statusHTML = "
          <li class=\"status animation fadeIn\">You promoted this.</li>
        "
        $commentList.prepend(statusHTML)
        $this.text('Promoted').removeClass("post_promotion").addClass("disable_post_promotion")
      error: (data) ->
        alert "error"

    $.ajax params
    return false
