#= require_self
#= require_tree ./app

window.App =
  Models: {}
  Collections: {}
  Routers: {}
  Views: {}

$ ->
  if $("#new_post").length > 0
    window.NewPostView = new App.Views.Posts.NewView(el: "#new_post")
