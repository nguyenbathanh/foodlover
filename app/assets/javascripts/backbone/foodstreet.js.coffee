#= require_self
#= require_tree ./templates
#= require_tree ./models
#= require_tree ./views
#= require_tree ./routers

window.Foodstreet =
  Models: {}
  Collections: {}
  Routers: {}
  Views: {}
  Public:
    alertShown: false
    ajaxRequest: (params) ->
      ajax_params =
        url: params.url
        data: params.data
        async: true
        type: if params.type? then params.type else 'GET'
        dataType: 'json'
        processData: false
        success: (data) ->
          params.success(data)
        error: (data) ->
          params.error(data)

      $.ajax ajax_params

    follow: (slug) ->
      Foodstreet.Public.ajaxRequest
        url: "/follow/"
        type: "POST"
        data: "slug=#{slug}"
        success: (data) ->
        error: ->
          # TODO: error

$(document).ready ->
  window.streams_collection = new Foodstreet.Collections.StreamsCollection()
  window.users_collection = new Foodstreet.Collections.UsersCollection()
  window.message_collection = new Foodstreet.Collections.MessagesCollection()
  window.conversation_collection = new Foodstreet.Collections.ConversationsCollection()
  window.notifications_collection = new Foodstreet.Collections.NotificationsCollection()
  window.foods_collection = new Foodstreet.Collections.FoodsCollection()
  window.navbar_router = new Foodstreet.Routers.NavbarsRouter()
  window.footer_router = new Foodstreet.Routers.FootersRouter()
#   window.sidebar_router = new Foodstreet.Routers.SideBarsRouter()
  window.stream_router = new Foodstreet.Routers.StreamsRouter()
  window.brand_router = new Foodstreet.Routers.BrandsRouter()
  window.photo_router = new Foodstreet.Routers.PhotosRouter()
  window.post_router = new Foodstreet.Routers.PostsRouter()
  window.friends_router = new Foodstreet.Routers.FriendsRouter()
  window.messages_router = new Foodstreet.Routers.MessagesRouter()
  window.users_router = new Foodstreet.Routers.UsersRouter()
  window.profilebars_router = new Foodstreet.Routers.ProfileBarsRouter()
  window.notification_router = new Foodstreet.Routers.NotificationRouter()
  window.food_router = new Foodstreet.Routers.FoodRouter()
  Backbone.history.start()

