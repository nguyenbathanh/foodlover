App.Views.Posts ||= {}

class App.Views.Posts.NewView extends Backbone.View
  initialize: ->
    @initUploader()
    @initTagsInput()
    @initCreatePost()
    @initTagChooseAlbum()
    # This is for choose album field.
    @initTagsAtWhere()
    @initShare()

  initCreatePost: ->
    form = $('#new_post_form')
    $('#new_post').on 'hide', =>
      @closeCreateAlbum()


    validate_image_uploading = () ->
      result = 'true'
      target = $(".preview img").attr("src")
      if (target == undefined)
        alert("Select image first!")
        result = 'false'


    validate_food_name = (e) ->
      result = 'true'
      target = $("#post_food_names_addTag").find('input')
      placeholder = target.data('default')

      val = target.val().replace(placeholder, '')
      tags = $("#post_food_names_tagsinput .tag").length
      if val.length < 3 && tags < 1
        alert("Select food name!")
        result = 'false'


    $('#new_post_submit').click (ev) ->
      result = validate_food_name()
      result2 = validate_image_uploading()


      if result != 'false'  && result2 != 'false'
        $('.progress-bar').html 'Loading...'
        $.post "#{form.attr("action")}.json", form.serialize(), (data)->
          $('.progress-bar').html ''

          $('#new_post').modal('hide')
          $("#post_food_names").importTags('')
          $("#posts_user_names").importTags('')
          $('#post_album_id').val('')
          $('input[name="location"]').val('')
          $('#post_message').val('')
          $('#post_picture_id').val('')
          $('.btn.btn-success.fileinput-button').css('display': 'block')
          $('#picture_uploader .preview').html('<img>')
          if ($('#share_facebook').val() == '1')
            $('#share_facebook').val('')
            $('.modal-footer .btn-facebook').removeClass 'active'
          if ($('#share_twitter').val() == '1')
            $('#share_twitter').val('')
            $('.modal-footer .btn-twitter').removeClass 'active'
          window.stream_router.index()
      else
        #        alert("error")

  initTagsInput: ->
    $("#post_food_names").tagsInput
      'autocomplete_url': "/foods.json",
      'height': '20px',
      'width': '320px',
      'interactive': true,
      'defaultText': 'Add food name',
      'removeWithBackspace': true,
      'minChars': 3,
      'placeholderColor': '#999'

    $("#posts_user_names").tagsInput
      'autocomplete_url': "/users/search.json",
      'height': '20px',
      'width': '330px',
      'interactive': true,
      'defaultText': 'Add username',
      'removeWithBackspace': true,
      'minChars': 3,
      'placeholderColor': '#999'

    $("#post_food_names_tag").blur (e)->
      setTimeout ()->
        if not $('.ui-autocomplete').is(':visible')
          if not $("#post_food_names").tagExist $("#post_food_names_tag").val()
            $("#post_food_names").addTag $("#post_food_names_tag").val()
            $("#post_food_names_tag").val ''
      , 200

    $("#posts_user_names_tag").blur (e)->
      setTimeout ()->
        if not $('.ui-autocomplete').is(':visible')
          names = []
          term_value = $("#posts_user_names_tag").val()
          $.getJSON "/users/search.json", {term: term_value}, (data)->
            names = data
              .success ->
                # If there is not a user with this name: we clear the textfield
                # else we iterate over names
                if names.length == 0
                  $("#posts_user_names_tag").val ''
                else
                  $.each names, (index, value)->
                    if value.label == term_value
                      if not $("#posts_user_names").tagExist $("#posts_user_names_tag").val()
                        $("#posts_user_names").addTag $("#posts_user_names_tag").val()
                        $("#posts_user_names_tag").val ''
                    else
                      $("#posts_user_names_tag").val ''
                      $("#posts_user_names_tag").removeClass 'not_valid'
      , 200

    $('#post_food_names_tag').keyup (e)->
      if $.inArray(String(e.keyCode), ['35', '36', '37', '39', '45', '46', '16']) == -1
        $(@).val ($(@).val().replace /[{}\(\)\^$&._%#!@=<>:;,~\`\'\"\s\t\*\?\/\+\|\[\\\]\-]+/g, '-')


  initTagChooseAlbum: ->
    #     $("#post_album_id").autocomplete
    #       source: "/albums/search",
    #       minLength: 3
    $('.post-album-arrow').click =>
      @toggleCreateAlbum()

    # Connect click with input value
    self = @
    $('.modal-choose-album .album').click ->
      text_val = $("#post_album_id").val()
      $("#post_album_id").val($("#post_album_id").val() + ',') if (text_val.match(/\,\s*$/) == null) and (text_val.match(/^\s*$/) == null)
      $("#post_album_id").val $("#post_album_id").val() + $(@).html() + ","
      self.toggleCreateAlbum()

    $('#post_album_id').keyup (e)->
      if $.inArray(String(e.keyCode), ['35', '36', '37', '39', '45', '46', '16']) == -1
        $(@).val ($(@).val().replace /[{}\(\)\^$&._%#!@=<>:;,~\`\'\"\s\t\*\?\/\+\|\[\\\]\-]+/g, '-')

    #     $("#post_album_id").keyup =>
    #       @closeCreateAlbum()


    # Create a new album
    $('.modal-choose-album .create').click =>
      @createNewAlbum()
    $('.modal-choose-album .name').keyup (e)=>
      if (e.keyCode == 13)
        @createNewAlbum()

  afterGetCurrentPosition: ->
    lat = $('input[name="location"]').attr 'data-latitude'
    long = $('input[name="location"]').attr 'data-longitude'

    #     $('input[name="location"]').tagsInput
    #       'autocomplete_url': "/location/search/" + String(lat) + '/' + String(long),
    #       'height':'20px',
    #       'width':'330px',
    #       'interactive':true,
    #       'defaultText':'Add location',
    #       'removeWithBackspace' : true,
    #       'minChars' : 3,
    #       'placeholderColor' : '#999'

    #$('input[name="location"]').autocomplete
    #  source: "/location/search/" + String(lat) + '/' + String(long),
    #  minLength: 3


  initTagsAtWhere: ->
    #     if(navigator.geolocation)
    #       navigator.geolocation.getCurrentPosition (pos)=>
    #         $('input[name="location"]').attr 'data-latitude', pos.coords.latitude
    #         $('input[name="location"]').attr 'data-longitude', pos.coords.longitude
    #         @afterGetCurrentPosition()
    #     else
    #       # Kuala Lumpur
    #       $('input[name="location"]').attr 'data-latitude', "3.1475"
    #       $('input[name="location"]').attr 'data-longitude', "101.693333"
    # Kuala Lumpur
    $('input[name="location"]').attr 'data-latitude', "3.1475"
    $('input[name="location"]').attr 'data-longitude', "101.693333"
    @afterGetCurrentPosition()



  # Toogle between autocomplete field and new album popup.
  # see initTagChooseAlbum
  toggleCreateAlbum: ->
    $('.modal-choose-album').toggle()

    # This condition in necessary to resolve a visual issue.
    choose_album_field_offset = $('#post_album_id').offset()
    if not $('.modal-choose-album').attr('offset-left')
      $('.modal-choose-album').attr('offset-left', choose_album_field_offset.left)
    $('.modal-choose-album').offset {
    left: $('.modal-choose-album').attr('offset-left'),
    top: choose_album_field_offset.top + $('#post_album_id').outerHeight()
    }
    $('.modal-choose-album').attr 'offset-left', choose_album_field_offset.left


  closeCreateAlbum: ->
    $('.modal-choose-album').hide()
    $.get "/current_user/update_dashboard", (data2) ->
      $(".photos_wrapper").html data2
      window.sortable()

  createNewAlbum: ->
    album_name = String($('.modal-choose-album .name').val()).trim()
    return if album_name == ''
    $('.modal-choose-album .albums').append '<li class="album">' + album_name + '</li>'
    $("#post_album_id").val album_name
    $('.modal-choose-album .name').val ''

    # Connect click with input value
    self = @
    $('.modal-choose-album .album:last-child').click ->
      $("#post_album_id").val $(@).html()
      self.toggleCreateAlbum()

    @toggleCreateAlbum()

  initUploader: ->
    uploader = $("#picture_uploader .file-field").fileupload
      formData:
        { authenticity_token: $("meta[name=csrf-token]").attr("content"), user_slug: window.current_user_slug }
      add: (e, data)->
        jqXHR = data.submit().success (data)->
          $("#picture_uploader .fileinput-button").hide()
          $("#picture_uploader .preview").show()
          $("#picture_uploader .preview img").attr("src", data.medium_sized_url)
          $("#post_picture_id").val(data.id)

  initShare: ->
    $('a.btn.btn-large.btn-facebook').click =>
      @toggleFacebook()

    $('a.btn.btn-large.btn-twitter').click =>
      @toggleTwitter()

  toggleFacebook: ->
    if ($('#share_facebook').val() == '1')
      $('#share_facebook').val('')
      $('.modal-footer .btn-facebook').removeClass 'active'
    else
      $('#share_facebook').val('1')
      $('.modal-footer .btn-facebook').addClass 'active'

  toggleTwitter: ->
    if ($('#share_twitter').val() == '1')
      $('#share_twitter').val('')
      $('.modal-footer .btn-twitter').removeClass 'active'
    else
      $('#share_twitter').val('1')
      $('.modal-footer .btn-twitter').addClass 'active'

# #
# #    @options.friends.bind('add', @addOne)
# #    @options.friends.bind('reset', @addAll)
# 
# #    $("form#new_post").bind(
# #      "submit",
# #    (e) ->
# #      if $("#post_message").val() == "Share new food..."
# #        $(".post_error").fadeIn()
# #        e.preventDefault()
# #      else
# #        $(".post_button").val("Posting...")
# #        $(".post_loading").show()
# #      if $("form#new_post").valid() == false
# #        $(".post_button").val("Post")
# #        $(".post_loading").hide()
# #    )
# #
# #    $("input#post_message").focus ->
# #      if ($(this).val() == "Share new food...")
# #        $(this).val("")
# #      $(this).keypress ->
# #        $(".post_error").fadeOut()
# #
# #    $("input#post_message").blur ->
# #      if ($(this).val() == "")
# #        $(this).val("Share new food...")
# #
# #    $("form#new_post").validate(
# #      rules: {
# #      'post[message]': "required",
# #      'post[image]': "required"
# #      }
# #      messages: {
# #      'post[image]':
# #        required: "Please upload a photo for your food."
# #      }
# #    )

