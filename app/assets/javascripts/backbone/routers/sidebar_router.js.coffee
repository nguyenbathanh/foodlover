class Foodstreet.Routers.SideBarsRouter extends Backbone.Router
  initialize: ->
    @outlets_featured  = new Foodstreet.Collections.OutletsCollection
    @outlets_promotion = new Foodstreet.Collections.OutletsCollection

  routes:
    "*actions"  : "index"

  index: ->
    @outlets_featured.reset [], {silent: true}
    @outlets_featured.url = '/outlets/featured'
    @outlets_featured.fetch
      success: (collection, response) ->
        @featuredView = new Foodstreet.Views.SideBar.FeaturedView
          outlets  : collection
        $('#featured').html(@featuredView.render().el)
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here

    @outlets_promotion.reset [], {silent: true}
    @outlets_promotion.url = '/outlets/promotion'
    @outlets_promotion.fetch
      success: (collection, response) ->
        @promotionView = new Foodstreet.Views.SideBar.PromotionView
          outlets  : collection
        $('#promoted').html(@promotionView.render().el)
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here




