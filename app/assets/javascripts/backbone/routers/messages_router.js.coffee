class Foodstreet.Routers.MessagesRouter extends Backbone.Router
  initialize: ->
    @messages = message_collection
    @conversation = conversation_collection

  routes:
    "inbox": "index"
    "inbox/#conversations/:slug": "show"

  index: ->
    @messages.reset [], {silent: true}
    @messages.fetch
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here
    @view = new Foodstreet.Views.Messages.IndexView
      messages: @messages
    $('#content_bar').html(@view.render().el)

  show: (slug) ->
    @conversation.reset [], {silient: true}
    @conversation.url = '/conversations/' + slug
    @conversation.fetch
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here
    @view = new Foodstreet.Views.Messages.ShowView
      messages: @conversation
      slug: slug
    $('#content_bar').html(@view.render().el)
