class Foodstreet.Routers.StreamsRouter extends Backbone.Router
  initialize: ->
    @streams = streams_collection

  routes:
    "stream": "index"

  index: ->
    if (window.current_user_id == '')
      window.location.href = '/login'
    else
      $(".loading").show()
      @streams.reset [], {silent: true}
      @streams.url = '/posts' + '?page=1'
      @streams.fetch
        success: ->
          $(".loading").hide()
        error: (model, response) ->
          if response.status == 401
            window.location = "/"
          # TODO: ERROR here
      
      @view = new Foodstreet.Views.Stream.IndexView
        streams: @streams
      $('#content_bar').html(@view.render().el)