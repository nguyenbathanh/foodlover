class Foodstreet.Routers.NavbarsRouter extends Backbone.Router
  initialize: ->
    @you = new Foodstreet.Models.User
    @you.url = '/users/you'

  routes:
    "*actions": "index"

  index: ->
    if (window.current_user_id == '')
      @headerView = new Foodstreet.Views.NavBar.IndexView
      $('header').html(@headerView.default().el)
    else
      @you.fetch
        success: (model, response) ->
          @headerView = new Foodstreet.Views.NavBar.IndexView
            model: model
          $('header').html(@headerView.render().el)

        error: (model, response) ->
          if response.status == 401
            window.location = "/"
          # TODO: ERROR here

