class Foodstreet.Routers.BrandsRouter extends Backbone.Router
  initialize: ->
    @you = new Foodstreet.Models.User
    @you.url = '/users/you'

  routes:
    "brand": "index"

  index: ->
    if (window.current_user_id == '')
      @view = new Foodstreet.Views.Brand.IndexView
      $('#content_bar').html(@view.render().el)
    else
      @you.fetch
        success: (model, response) ->
          @view = new Foodstreet.Views.Brand.IndexView
            model: model
          $('#content_bar').html(@view.render().el)
        error: (model, response) ->
          if response.status == 401
            window.location = "/"
          # TODO: ERROR here

