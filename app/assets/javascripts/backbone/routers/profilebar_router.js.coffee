class Foodstreet.Routers.ProfileBarsRouter extends Backbone.Router
  routes:
    "users/:slug"                  : "show"
    ":slug/photos"                 : "show"
    ":slug/albums/:album_slug"     : "show"
    "*actions"                      : "index"

  show: (slug) ->
    @user = new Foodstreet.Models.User
    @user.url = '/users/' + slug
    @user.fetch
      success: (model, response) ->
        @view = new Foodstreet.Views.ProfileBar.IndexView
          model: model
        $('#profile_bar').html(@view.render().el)
        window.profile = model
        window.profile.tag_subscriptions = new Foodstreet.Collections.TagSubscriptions(model.get("tag_subscriptions"))
        @tag_view = new Foodstreet.Views.TagSubscription.Index
          tags: window.profile.tag_subscriptions
        @tag_view.render().el
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here

  index: ->
    if (window.current_user_id == '')
      @view = new Foodstreet.Views.ProfileBar.IndexView
      $('#profile_bar').html(@view.default().el)
    else
      @user  = new Foodstreet.Models.User
      @user.url = '/users/you'
      @user.fetch
        success: (model, response) ->
          @view = new Foodstreet.Views.ProfileBar.IndexView
            model: model
          $('#profile_bar').html(@view.render().el)
          window.profile = model
          window.profile.tag_subscriptions = new Foodstreet.Collections.TagSubscriptions(model.get("tag_subscriptions"))
          @tag_view = new Foodstreet.Views.TagSubscription.Index
            tags: window.profile.tag_subscriptions
          @tag_view.render().el
        error: (model, response) ->
          if response.status == 401
            window.location = "/"
          # TODO: ERROR here

