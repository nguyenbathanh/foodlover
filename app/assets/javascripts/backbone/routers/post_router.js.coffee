class Foodstreet.Routers.PostsRouter extends Backbone.Router
  initialize: ->
    @post = new Foodstreet.Models.Stream

  routes:
    "post/:id": "show"

  show: (id) ->
    @post.url = '/posts/' + id
    @post.fetch
      success: (model, response) ->
        @view = new Foodstreet.Views.Post.IndexView
          model: model
        $('#content_bar').html(@view.render().el)
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here

