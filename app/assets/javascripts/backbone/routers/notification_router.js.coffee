class Foodstreet.Routers.NotificationRouter extends Backbone.Router
  initialize: ->
    @notifications = notifications_collection
    
  routes:
    "notifications" : "index"

  index: () ->
    @notifications.fetch
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here
      success: (model, response) ->
        @view = new Foodstreet.Views.Stream.IndexView
          streams: model
        $('#content_bar').html(@view.render().el)
