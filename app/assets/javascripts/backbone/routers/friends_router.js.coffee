class Foodstreet.Routers.FriendsRouter extends Backbone.Router
  initialize: ->
    @friends = new Foodstreet.Collections.FriendsCollection

  routes:
    ":slug/following" : "following"
    ":slug/followers" : "followers"

  following: (slug) ->
    @friends.reset [], {silent: true}
    @friends.url = '/follow/following?slug=' + slug
    @friends.fetch
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here
    @view = new Foodstreet.Views.Friends.IndexView
      friends: @friends
    $('#content_bar').html(@view.render().el)

  followers: (slug) ->
    @friends.reset [], {silent: true}
    @friends.url = '/follow/follower?slug=' + slug
    @friends.fetch
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here
    @view = new Foodstreet.Views.Friends.IndexView
      friends: @friends
    $('#content_bar').html(@view.render().el)


