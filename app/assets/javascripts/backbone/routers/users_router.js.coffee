class Foodstreet.Routers.UsersRouter extends Backbone.Router
  initialize: ->
    @streams = streams_collection

  routes:
    "users/:slug" : "index"
    "users/:slug/edit" : "edit"

  index: (slug) ->
    @streams.reset [], {silent: true}
    @streams.url = '/posts/others/?slug=' + slug
    @streams.fetch
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here
    @view = new Foodstreet.Views.User.IndexView
      streams: @streams
    $('#content_bar').html(@view.render().el)

  edit: (slug) ->
    if (window.current_user_id == '')
      window.location.href = '/login'
    else
      @user = new Foodstreet.Models.User({slug: current_user_slug})
      $("#content_bar").html(new Foodstreet.Views.User.Edit({model: @user}).render().el)
