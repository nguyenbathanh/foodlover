class Foodstreet.Routers.FootersRouter extends Backbone.Router
  routes:
    "*actions": "index"

  index: ->
    @view = new Foodstreet.Views.Footer.IndexView
    $('footer').html(@view.render().el)

