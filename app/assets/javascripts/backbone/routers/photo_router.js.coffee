class Foodstreet.Routers.PhotosRouter extends Backbone.Router
  initialize: ->
    @photos = new Foodstreet.Collections.PhotosCollection()

  routes:
    ":user_slug/photos": "index"
    ":user_slug/albums/:slug": "album"

  index: (user_slug) ->
    @photos.reset [], {silient: true}
    @photos.fetch
      data:
        slug: user_slug
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here
      success: (model, response) ->
        @view = new Foodstreet.Views.Photo.IndexView
          photos: model
          slug: user_slug
          owner_name: user_slug
        $('#content_bar').html(@view.render().el)

  album: (user_slug, slug) ->
    @photos.reset [], {silient: true}
    @photos.fetch
      data:
        slug: user_slug
        tag: slug
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here
      success: (model, response) ->
        @view = new Foodstreet.Views.Album.ShowView
          photos: model
          owner_name: model.first().get("owner_name")
          album_name: slug.replace(/-/g, ' ')
        $('#content_bar').html(@view.render().el)