class Foodstreet.Routers.FoodRouter extends Backbone.Router
  initialize: ->
    @foods = foods_collection

  routes:
    "foods/:tag": "index"

  index: (tag) ->
    @foods.fetch
      data:
        tag: tag
      error: (model, response) ->
        if response.status == 401
          window.location = "/"
        # TODO: ERROR here
      success: (model, response) ->
        @view = new Foodstreet.Views.Stream.IndexView
          streams: model
        $('#content_bar').html(@view.render().el)
    