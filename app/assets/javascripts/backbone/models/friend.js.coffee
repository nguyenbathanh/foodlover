class Foodstreet.Models.Friend extends Backbone.Model
  paramRoot: 'follow'

  defaults: 
    username: null
    profile_image: null

class Foodstreet.Collections.FriendsCollection extends Backbone.Collection
  model: Foodstreet.Models.Friend
  url: '/follow'

