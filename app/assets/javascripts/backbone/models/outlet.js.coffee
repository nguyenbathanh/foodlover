class Foodstreet.Models.Outlet extends Backbone.Model
  paramRoot: 'outlet'

  defaults:
    name: null
    slug: null

class Foodstreet.Collections.OutletsCollection extends Backbone.Collection
  model: Foodstreet.Models.Outlet
  url: '/outlets'
