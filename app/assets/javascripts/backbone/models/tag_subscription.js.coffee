class Foodstreet.Models.TagSubscription extends Backbone.Model
  paramRoot: 'tag_subscription'
  urlRoot: 'tag_subscriptions'

  defaults:
    name: null

class Foodstreet.Collections.TagSubscriptions extends Backbone.Collection
  model: Foodstreet.Models.TagSubscription
  url: '/tag_subscriptions'