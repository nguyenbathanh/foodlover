class Foodstreet.Models.User extends Backbone.Model
  paramRoot: 'user'
  url: '/users'

  defaults:
    id: null
    email: null
    role: 1
    
class Foodstreet.Collections.UsersCollection extends Backbone.Collection
  model: Foodstreet.Models.User
  url: '/users'

  current_user: ->
    @.get(current_user_id)

