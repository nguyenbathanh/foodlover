class Foodstreet.Models.Conversation extends Backbone.Model
  paramRoot: 'conversation'

  defaults:
    username: null
    body: null

class Foodstreet.Collections.ConversationsCollection extends Backbone.Collection
  model: Foodstreet.Models.Conversation
