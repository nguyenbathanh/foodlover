class Foodstreet.Models.Message extends Backbone.Model
  paramRoot: 'conversations'
  url: '/conversations'

  defaults:
    recipient: null
    subject: null
    body: null
    username: null
    id: null
    created_time: null

class Foodstreet.Collections.MessagesCollection extends Backbone.Collection
  model: Foodstreet.Models.Message
  url: '/conversations'
