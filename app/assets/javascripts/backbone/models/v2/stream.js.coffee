class Foodstreet.Models.Stream extends Backbone.Model
  paramRoot: 'post'

  defaults:
    message: null
    post_image: null
    created_at_time: null
    username: null
    profile_image: null
    like_count: null
    reshare_count: null
    repin_count: null
    comment_count: null
    like: null
    id: null
    comments: null
    user_id: null
    repin_message: ''
    flagger_ids: []
    promoter_ids: []
    
  initialize: ->
      _.bindAll(this, "validate")
      
  validate: (attrs) ->
    if (attrs.message.trim() == "")
      return "Message can't be blank"

class Foodstreet.Collections.StreamsCollection extends Backbone.Collection
  model: Foodstreet.Models.Stream
  url: '/feeds'