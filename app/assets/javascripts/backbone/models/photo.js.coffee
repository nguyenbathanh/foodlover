class Foodstreet.Models.Photo extends Backbone.Model
  paramRoot: 'photo'

  defaults:
    images: null
    id: null

class Foodstreet.Collections.PhotosCollection extends Backbone.Collection
  model: Foodstreet.Models.Photo
  url: '/photos'
