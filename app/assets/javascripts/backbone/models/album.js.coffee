class Foodstreet.Models.Album extends Backbone.Model
  paramRoot: 'photo'
  url: '/photos'

  defaults:
    name: 'default'
    count: 0

class Foodstreet.Collections.AlbumsCollection extends Backbone.Collection
  model: Foodstreet.Models.Album
  url: '/photos/albums'