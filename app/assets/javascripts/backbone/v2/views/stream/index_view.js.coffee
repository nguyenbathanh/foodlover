Foodstreet.Views.Stream ||= {}

class Foodstreet.Views.Stream.IndexView extends Backbone.View
  template: JST["backbone/v2/templates/stream/index"]

  initialize: ->
    _.bindAll(@, 'addOne', 'addAll', 'render')

    @options.streams.bind('add', @addOne)
    @options.streams.bind('reset', @addAll)

  events:
    "click .load-more"        : "loadMoreFeeds"

  addAll: ->
    @options.streams.each(@addOne)

  addOne: (stream) ->
    view = new Foodstreet.Views.Stream.StreamView
      model  : stream
      streams: @options.streams
    @$(".stream_wrapper").append(view.render().el)

  render: ->
    $(@el).html(@template())
    @addAll()

    @
    
  loadMoreFeeds: ->
    @options.streams.fetch
      data:
        last_post_id: window.streams_collection.last().get("id")
      error: (model, response) ->
        # TODO: ERROR here
    return false


