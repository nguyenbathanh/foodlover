Foodstreet.Views.Stream ||= {}

class Foodstreet.Views.Stream.StreamView extends Backbone.View
  template: JST["backbone/v2/templates/stream/stream"]
  editTemplate: JST["backbone/v2/templates/stream/edit"]

  events:
    "click .post_like"        : "likePost"
    "click .post_unlike"      : "unlikePost"
    "keypress .comment_input" : "commentPost"
    "click .post_remove"      : "removePost"
    "click .post_edit"        : "editPost"
    "submit #edit-post"       : "updatePost"
    "click .cancelEditPost"   : "cancelEdit"
    "click .saveEditPost"     : "updatePost"
    "click .post_reshare"     : "resharePost"
    "click .post_repin"       : "repinPost"
    "click .post_flag"        : "flagPost"
    "click .post_promotion"   : "promotePost"
    "click .comment_post"     : "commentPost"

  likePost: ->
    $this = @$('.post_like')
    $commentList = @$('ul.comments')
    $like_counter = @$('.like_count')
    params =
      url: '/like'
      type: 'POST'
      data: "id=" + @model.attributes.id
      async: true
      success: (data) ->
        $this.html("Unlike")
        new_count = parseInt($like_counter.text().split(' ')[0]) + 1
        $like_counter.text(new_count + ' Liked')
        $this.addClass("post_unlike")
        $this.removeClass("post_like")
        statusHTML = "
          <li class=\"status animation fadeIn\">You like this.</li>
        "
        $commentList.prepend(statusHTML)
      error: (data) ->
        alert "error"

    $.ajax params
    return false

  unlikePost: ->
    $this = @$('.post_unlike')
    $commentList = @$('ul.comments')
    $like_counter = @$('.like_count')
    params =
      url: '/like'
      type: 'delete'
      data: "id=" + @model.attributes.id
      async: true
      success: (data) ->
        $this.html("Like")
        new_count = parseInt($like_counter.text().split(' ')[0]) - 1
        $like_counter.text(new_count + ' Liked')
        $this.addClass("post_like")
        $this.removeClass("post_unlike")
        $commentList.find("li.status").hide()
      error: (data) ->
        alert "error"

    $.ajax params
    return false

  commentPost: (event) ->
    if @$('.comment_input').val() != ""
      if (event.which == 13)
        $commentList = @$('ul.comments')
        message = @$('.comment_input').val()
        comment_counter = @$(".comment_count")
        params =
          url: '/comments'
          type: 'POST'
          data: 'id=' + @model.attributes.id + ";message=" + message
          success: (data) ->
            commentHTML = "
              <li class=\"comment_entry animation fadeIn new\">
                <span class=\"commentor top\">
                  <a href=\"#\"><img src=\"#{data.image_32}\" /></a>
                </span><span class=\"comment_message top\">
                  <div class=\"comment\">
                    <a href=\"#\" class=\"commentor_name\">#{data.author}</a><span class=\"message\">#{data.message}</span>
                  </div>
                  <div class=\"footer\">#{data.created_at_time}</div>
                </span>
              </li>
            "
            $commentList.append(commentHTML)
            t = setTimeout("$('li.new').removeClass('new')", 4000)
            $('.comment_input').val("")
            
            new_count = parseInt(comment_counter.text().split(' ')[0]) + 1
            if (new_count == 1)
              counter_object = ' Comment'
            else
              counter_object = ' Comments'
            comment_counter.text(new_count + counter_object)
          error: (data) ->
            alert "error"
      
        $.ajax params
        return false

  render: ->
    $(@el).html(@template(@model.toJSON()))
    @
    
  removePost: (event) ->
    if (@model.get("user_id") == window.current_user_id)
      ConfirmStatus = confirm("Are you sure you want to delete this post?")
      if (ConfirmStatus == true)
        @model.url = 'posts/'+@model.get("id")
        $el = $(@el)
        event.preventDefault()
        event.stopPropagation()
        @model.destroy
          success: (model, response) ->
            $el.remove()
          error: (model, response) ->
            alert("Please retry.")
    return false
    
  editPost: (event) ->
    if (@model.get("user_id") == window.current_user_id)
      $(@el).html(@editTemplate(@model.toJSON()))
      $("#edit-post-message").focus()
    return false
    
  cancelEdit: (event) ->
    $(@el).html(@template(@model.toJSON()))
    return false

  updatePost: (event) ->
    $that = this
    $el = $(@el)
    @model.url = 'posts/' + @model.get("id")
    @model.save {message: this.$("#edit-post-message").val()},
      success: (model, resp) ->
        $("#new_post").attr('src', '/posts/new')
#        $.map(resp.all_taggings, (n) ->
#          window.availableTags.push(n)
#        )
#        window.availableTags = jQuery.unique(window.availableTags)
        
        view = new Foodstreet.Views.Stream.StreamView
          model  : model
        $el.fadeOut('slow').html(view.render().el).fadeIn('slow')
      error: (model, resp) ->
        $that.$("#edit-post-message").after('<div class="post_error animated bounce">'+resp+'</div>')
    return false
    
  resharePost: ->
    $this = @$('.post_reshare')
    $commentList = @$('ul.comments')
    $reshare_counter = @$('.reshare_count')
    params =
      url: '/posts/' + @model.attributes.id + '/reshare'
      type: 'POST'
      async: true
      success: (data) ->
        statusHTML = "
          <li class=\"status animation fadeIn\">You reshare this.</li>
        "
        $commentList.prepend(statusHTML)
        new_count = parseInt($reshare_counter.text().split(' ')[0]) + 1
        $reshare_counter.text(new_count + ' Reshared')
      error: (data) ->
        alert "error"

    $.ajax params
    return false
    
  repinPost: ->
    $.colorbox({href:"/posts/" + @model.attributes.id + "/repin"})
    return false
    
  flagPost: ->
    $.colorbox({href:"/posts/" + @model.attributes.id + "/flag"})
    return false
    
  promotePost: ->
    $this = @$('.post_promotion')
    $commentList = @$('ul.comments')
    params =
      url: '/posts/' + @model.attributes.id + '/promote'
      type: 'POST'
      async: true
      success: (data) ->
        statusHTML = "
          <li class=\"status animation fadeIn\">You promoted this.</li>
        "
        $commentList.prepend(statusHTML)
        $this.text('Promoted').removeClass("post_promotion").addClass("disable_post_promotion")
      error: (data) ->
        alert "error"

    $.ajax params
    return false
  
  commentPost: ->
    $(".post_comment").toggle()
