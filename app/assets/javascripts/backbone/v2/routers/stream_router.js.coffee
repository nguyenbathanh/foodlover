class Foodstreet.Routers.StreamsRouter extends Backbone.Router
  initialize: ->
    @streams = streams_collection

  routes:
    "/": "index"
    "": "index"

  index: ->
    if (window.current_user_id == '')
      window.location.href = '/login'
    else
      @streams.reset [], {silent: true}
      @streams.fetch
        error: (model, response) ->
          if response.status == 401
            window.location = "/"
          # TODO: ERROR here
      @view = new Foodstreet.Views.Stream.IndexView
        streams: @streams
      $('#content_bar').html(@view.render().el)

