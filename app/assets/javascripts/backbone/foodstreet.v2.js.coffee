#= require_self
#= require_tree ./v2/templates
#= require_tree ./v2/models
#= require_tree ./v2/views
#= require_tree ./v2/routers

window.Foodstreet =
  Models: {}
  Collections: {}
  Routers: {}
  Views: {}
  Options: {}
  Public:
    alertShown: false
    ajaxRequest: (params) ->
      ajax_params =
        url: params.url
        data: params.data
        async: true
        type: if params.type? then params.type else 'GET'
        dataType: 'json'
        processData: false
        success: (data) ->
          params.success(data)
        error: (data) ->
          params.error(data)

      $.ajax ajax_params

    follow: (slug) ->
      Foodstreet.Public.ajaxRequest
        url: "/follow/"
        type: "POST"
        data: "slug=#{slug}"
        success: (data) ->
        error: ->
          # TODO: error