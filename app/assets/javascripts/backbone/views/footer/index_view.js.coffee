Foodstreet.Views.Footer ||= {}

class Foodstreet.Views.Footer.IndexView extends Backbone.View
  template: JST["backbone/templates/footer/index"]

  render: ->
    $(@el).html(@template())

    @

