Foodstreet.Views.TagSubscription ||= {}

class Foodstreet.Views.TagSubscription.Show extends Backbone.View
  template: JST["backbone/templates/tag_subscriptions/show"]
  editTemplate: JST["backbone/templates/tag_subscriptions/edit"]
  
  events:
    "click .tag_subscription_remove" : "RemoveTagSubscription"
    "click .tag_subscription_edit" : "EditTagSubscription"
    "submit #edit-tag-subscription-form" : "UpdateTagSubscription"

  render: ->
    $(@el).html(@template(@model.toJSON()))
    @
    
  RemoveTagSubscription: ->
    if (@model.get("user_id") == window.current_user_id)
      ConfirmStatus = confirm("Are you sure you want to delete this post?")
      if (ConfirmStatus == true)
        $el = $(@el)
        @model.destroy
          success: (model, response) ->
            $el.remove()
            if (window.location.hash == '#stream' || window.location.hash == '#users/'+window.current_user_slug)
              streams_collection.fetch()
          error: (model, response) ->
            alert("Please retry.")
    return false
  
  EditTagSubscription: ->
    if (@model.get("user_id") == window.current_user_id)
      $(@el).html(@editTemplate(@model.toJSON()))
      $("#edit-tag-subscription-field").focus()
    return false
    
  UpdateTagSubscription: ->
    $this = @$('#edit-tag-subscription-field')
    $el = $(@el)
    @model.save {name: $this.val()},
      success: (data) ->
        view = new Foodstreet.Views.TagSubscription.Show
          model: data
        $el.html(view.render().el)
        if (window.location.hash == '#stream' || window.location.hash == '#users/'+window.current_user_slug)
          streams_collection.fetch()
      error: ->
        alert "Something went wrong. Please try again later."
    return false