Foodstreet.Views.TagSubscription ||= {}

class Foodstreet.Views.TagSubscription.Index extends Backbone.View
  initialize: ->
    _.bindAll(@, 'addOne', 'addAll', 'render')
    
    @options.tags.bind('add', @addOne)
    @options.tags.bind('reset', @addAll)
    
  addAll: ->
    @options.tags.each(@addOne)
  
  addOne: (tag) ->
    view = new Foodstreet.Views.TagSubscription.Show
      model: tag
    $("ul.follow_categories li.label_medium").after(view.render().el)
    
  render: ->
    @addAll()
    @