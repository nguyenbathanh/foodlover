Foodstreet.Views.Stream ||= {}

class Foodstreet.Views.Stream.IndexView extends Backbone.View
  template: JST["backbone/templates/stream/index"]
  
  initialize: ->
    _.bindAll(@, 'addOne', 'addAll', 'render')
    
    
    
    window.indexView = @
    $(window).scroll(@checkScroll)
      
    @options.streams.bind('add', @addOne)
    @options.streams.bind('reset', @addAll)
  page: 1
  lastpage: false
  lastId: null
  events:
    "focus input.post_input"   : "focusSearch"
    "blur  input.post_input"   : "blurSearch"
  isLoading: false
    
  focusSearch: ->
    $this = $("input.post_input")
    if ($this.val() == "Share new food...")
      $this.val("")

  blurSearch: ->
    $this = $("input.post_input")
    if ($this.val() == "")
      $this.val("Share new food...")
  
  checkScroll:  ->
    triggerPoint = 100
    $window = $ window
    $document = $ document
    scrollTop = $window.scrollTop()
    if(!window.indexView.lastpage && !window.indexView.isLoading && (scrollTop > ($document.height() - $window.height()) - triggerPoint))
      self = window.indexView
      $(".loading").show()
      self.isLoading = true
      current_page = self.page + 1

      self.options.streams.url = 'posts?page=' + current_page 
      self.options.streams.fetch
        success: (data) ->
          if (data.length == 10)       
            self.page = self.page + 1          
          else
            self.lastpage = true
          $(".loading").hide()
          window.indexView.isLoading = false
        error: (model, response) ->
          window.indexView.isLoading = false
          if response.status == 401
            window.location = "/"
  addAll: ->
    if (@options.streams.length > 0)
      
      func = ->
        if $(".stream_item").length > 0
          $.ajax "/posts/last_since/" + $(".stream_item").first().attr("id"),
            type: "GET"
            dataType: "json"
            success: (feeds, textStatus, jqXHR) ->
              if (feeds.length > 0)
                $(".show_more_beginning", ".stream_wrapper").remove()
                newdiv = $("<div/>")
                newdiv.addClass('show_more_beginning')
                newdiv.html('Show '+ feeds.length + ' more recent updates')
                
                $(".stream_wrapper").prepend(newdiv)
                
                newdiv.click(->
                  window.stream_router.index()
                )
              setTimeout func, 15000
        else
          setTimeout func, 15000
            
            
      func()
      
    @options.streams.each(@addOne)

  addOne: (stream) ->
    view = new Foodstreet.Views.Stream.StreamView
      model  : stream
      streams: @options.streams

    $(".stream_wrapper").append(view.render().el)

  render: ->
    $(@el).html(@template())
    @addAll()
    @


