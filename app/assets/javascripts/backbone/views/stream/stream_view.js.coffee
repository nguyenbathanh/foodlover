Foodstreet.Views.Stream ||= {}

class Foodstreet.Views.Stream.StreamView extends Backbone.View
  template: JST["backbone/templates/stream/stream"]
  editTemplate: JST["backbone/templates/stream/edit"]

  events:
    "click .post_like"        : "likePost"
    "click .post_unlike"      : "unlikePost"
    "keypress .comment_input" : "commentPost"
    "click .post_remove"      : "removePost"
    "click .post_edit"        : "editPost"
    "submit #edit-post"       : "updatePost"
    "click .cancelEditPost"   : "cancelEdit"
    "click .saveEditPost"     : "updatePost"
    "click .post_reshare"     : "resharePost"
    "click .post_repin"       : "repinPost"
    "click .post_flag"        : "flagPost"
    "click .post_promotion"   : "promotePost"
    "click .comment_more_link": "commentMore"
    
  commentMore: (event) ->
    event.stopImmediatePropagation()
    comment_counter = @$(".comment_count")
    $commentList = @$('ul.comments')
    $(".loading").show()
    params =
      url: '/comments'
      type: 'GET'
      data: 'id=' + @model.attributes.id
      success: (data) ->
        $(".loading").hide()
        commentHTML = ""
        _.each data, (comment) ->
          commentHTML += "
                <li class=\"comment_entry animation fadeIn new\">
                  <span class=\"commentor top\">
                    <a href=\"#\"><img src=\"#{comment.image_32}\" /></a>
                  </span><span class=\"comment_message top\">
                    <div class=\"comment\">
                      <a href=\"#\" class=\"commentor_name\">#{comment.author}</a><span class=\"message\">#{comment.message}</span>
                    </div>
                    <div class=\"footer\">#{comment.created_at_time}</div>
                  </span>
                </li>
              "
        $commentList.html commentHTML
        t = setTimeout("$('li.new').removeClass('new')", 4000)
        $(".comment_input").val ""
        
        new_count = parseInt(comment_counter.text().split(' ')[0]) + 1
        if (new_count == 1)
          counter_object = ' Comment'
        else
          counter_object = ' Comments'
        comment_counter.text(new_count + counter_object)
      error: (data) ->
        alert "error"
  
    $.ajax params
    return false
  likePost: ->
    $this = @$('.post_like')
    $commentList = @$('ul.comments')
    $like_counter = @$('.like_count')
    $(".loading").show()
    params =
      url: '/like'
      type: 'POST'
      data: "id=" + @model.attributes.id
      async: true
      success: (data) ->
        $(".loading").hide()
        $this.html("Unlike")
        new_count = parseInt($like_counter.text().split(' ')[0]) + 1
        $like_counter.text(new_count + ' Liked')
        $this.addClass("post_unlike")
        $this.removeClass("post_like")
        statusHTML = "
          <li class=\"status animation fadeIn\">You like this.</li>
        "
        $commentList.prepend(statusHTML)
      error: (data) ->
        alert "error"

    $.ajax params
    return false

  unlikePost: ->
    $this = @$('.post_unlike')
    $commentList = @$('ul.comments')
    $like_counter = @$('.like_count')
    $(".loading").show()
    params =
      url: '/like'
      type: 'delete'
      data: "id=" + @model.attributes.id
      async: true
      success: (data) ->
        $(".loading").hide()
        $this.html("Like")
        new_count = parseInt($like_counter.text().split(' ')[0]) - 1
        $like_counter.text(new_count + ' Liked')
        $this.addClass("post_like")
        $this.removeClass("post_unlike")
        $commentList.find("li.status").hide()
      error: (data) ->
        alert "error"

    $.ajax params
    return false

  commentPost: (event) ->
    if @$('.comment_input').val() != ""
      if (event.which == 13)
        $commentList = @$('ul.comments')
        message = @$('.comment_input').val()
        comment_counter = @$(".comment_count")
        id = @model.attributes.id
        $(".loading").show()
        params =
          url: '/comments'
          type: 'POST'
          data: 'id=' + id + ";message=" + message
          success: (data) ->
            paramslist =
              url: '/comments'
              type: 'GET'
              data: 'id=' + id
              success: (comments) ->
                commentHTML = ""
                _.each comments, (comment) ->
                  commentHTML += "
                        <li class=\"comment_entry animation fadeIn new\">
                          <span class=\"commentor top\">
                            <a href=\"#\"><img src=\"#{comment.image_32}\" /></a>
                          </span><span class=\"comment_message top\">
                            <div class=\"comment\">
                              <a href=\"#\" class=\"commentor_name\">#{comment.author}</a><span class=\"message\">#{comment.message}</span>
                            </div>
                            <div class=\"footer\">#{comment.created_at_time}</div>
                          </span>
                        </li>
                      "
                $commentList.html commentHTML
                t = setTimeout("$('li.new').removeClass('new')", 4000)
                $(".comment_input").val ""
                
                new_count = parseInt(comment_counter.text().split(' ')[0]) + 1
                if (new_count == 1)
                  counter_object = ' Comment'
                else
                  counter_object = ' Comments'
                comment_counter.text(new_count + counter_object)
                $(".loading").hide()
              error: (error) ->
                alert "error"
          
            $.ajax paramslist
          error: (data) ->
            alert "error"
      
        $.ajax params
        return false

  render: ->
    $(@el).html(@template(@model.toJSON()))
    @

  removePost: (event) ->
    if (@model.get("user_id") == window.current_user_id)
      ConfirmStatus = confirm("Are you sure you want to delete this post?")
      if (ConfirmStatus == true)
        @model.url = 'posts/'+@model.get("id")
        $el = $(@el)
        event.preventDefault()
        event.stopPropagation()
        $(".loading").show()
        @model.destroy
          success: (model, response) ->
            $el.remove()
            $(".loading").hide()
          error: (model, response) ->
            alert("Please retry.")
            $(".loading").hide()
    return false
    
  editPost: (event) ->
    if (@model.get("user_id") == window.current_user_id)
      $(@el).html(@editTemplate(@model.toJSON()))
      $("#edit-post-message").focus()
    return false
    
  cancelEdit: (event) ->
    $(@el).html(@template(@model.toJSON()))
    return false

  updatePost: (event) ->
    $that = this
    $el = $(@el)
    @model.url = 'posts/' + @model.get("id")
    @model.save {message: this.$("#edit-post-message").val()},
      success: (model, resp) ->
        $("#new_post").attr('src', '/posts/new')
#        $.map(resp.all_taggings, (n) ->
#          window.availableTags.push(n)
#        )
#        window.availableTags = jQuery.unique(window.availableTags)
        
        view = new Foodstreet.Views.Stream.StreamView
          model  : model
        $el.fadeOut('slow').html(view.render().el).fadeIn('slow')
      error: (model, resp) ->
        $that.$("#edit-post-message").after('<div class="post_error animated bounce">'+resp+'</div>')
    return false
    
  resharePost: ->
    $this = @$('.post_reshare')
    $commentList = @$('ul.comments')
    $reshare_counter = @$('.reshare_count')
    $(".loading").show()
    params =
      url: '/posts/' + @model.attributes.id + '/reshare'
      type: 'POST'
      async: true
      success: (data) ->
        $(".loading").hide()
        statusHTML = "
          <li class=\"status animation fadeIn\">You reshare this.</li>
        "
        $commentList.prepend(statusHTML)
        new_count = parseInt($reshare_counter.text().split(' ')[0]) + 1
        $reshare_counter.text(new_count + ' Reshared')
      error: (data) ->
        alert "error"
        $(".loading").hide()

    $.ajax params
    return false
    
  repinPost: ->
    self = @$(".content_bottom")
    $(".loading").show()
    if $(".repin_container",self).length > 0
      $(".repin_container",self).remove()
    else
      params =
        url: "/posts/" + @model.attributes.id + "/repin"
        async: true
        success: (data) ->
          $(".loading").hide()
          htmltext = "<div class='repin_container'>"
          htmltext += data
          htmltext += "</div>"
          self.append htmltext
        error: (data) ->
          alert "error"
          $(".loading").hide()
  
      $.ajax params
    
    return false
    
    #alert('repin')
    #colorbox = $.colorbox({href:"/posts/" + @model.attributes.id + "/repin", class: "repin"})
    #setTimeout (->
    #    colorbox.resize()
    #  ), 2000
    return false
    
  flagPost: ->
    self = @$(".content_bottom")
    if $(".flag_container",self).length > 0
      $(".flag_container",self).remove()
    else
      $(".loading").show()
      params =
        url: "/posts/" + @model.attributes.id + "/flag", id: "flag_box"
        async: true
        success: (data) ->
          $(".loading").hide()
          htmltext = "<div class='flag_container'>"
          htmltext += data
          htmltext += "</div>"
          self.append htmltext
        error: (data) ->
          alert "error"
          $(".loading").hide()
  
      $.ajax params
    
    #$.colorbox({href:"/posts/" + @model.attributes.id + "/flag", id: "flag_box"})
    return false
    
  promotePost: ->
    $this = @$('.post_promotion')
    $commentList = @$('ul.comments')
    $(".loading").show()
    params =
      url: '/posts/' + @model.attributes.id + '/promote'
      type: 'POST'
      async: true
      success: (data) ->
        $(".loading").hide()
        statusHTML = "
          <li class=\"status animation fadeIn\">You promoted this.</li>
        "
        $commentList.prepend(statusHTML)
        $this.text('Promoted').removeClass("post_promotion").addClass("disable_post_promotion")
      error: (data) ->
        alert "error"

    $.ajax params
    return false