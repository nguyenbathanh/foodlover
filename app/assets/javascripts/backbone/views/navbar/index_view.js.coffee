Foodstreet.Views.NavBar ||= {}

class Foodstreet.Views.NavBar.IndexView extends Backbone.View
  template: JST["backbone/templates/navbar/index"]
  default_template: JST["backbone/templates/navbar/default_index"]

  events:
    "focus input.search_input"  : "focusSearch"
    "blur  input.search_input"  : "blurSearch"

  focusSearch: ->
    $this = $("input.search_input")
    if ($this.val() == "Find food here...")
      $this.val("")
      $this.css({'color':'#606060', 'background':'#FFFFFF'})

  blurSearch: ->
    $this = $("input.search_input")
    if ($this.val() == "")
      $this.val("Find food here...")
      $this.css({'background':'#E4F7FE','color':'#87CEEB'})
    else 
      $this.css({'color':'#606060', 'background':'#FFFFFF'})

  render: ->
    $(@el).html(@template(@model.toJSON()))

    @
    
  default: ->
    $(@el).html(@default_template())

    @

