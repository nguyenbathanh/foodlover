Foodstreet.Views.ProfileBar ||= {}

class Foodstreet.Views.ProfileBar.IndexView extends Backbone.View
  template: JST["backbone/templates/profilebar/index"]
  default_template: JST["backbone/templates/profilebar/default_index"]

  events:
    "click .follow_button"   : 'followRestaurant'
    "click .unfollow_button" : 'unfollowRestaurant'
    "focus .add_category"    : 'selectWords'
    "blur .add_category"     : 'deselectWords'
    "submit .user_tag_subscription_form" : "submitTagSubscription"

  followRestaurant: ->
    if (window.current_user_id == '')
      window.location.href = '/login'
    else
      $this = $('.follow_button')
      params =
        url: '/follow'
        type: 'POST'
        data: "slug=" + @model.attributes.slug
        async: true
        success: (data) ->
          $this.html("Following")
          $this.addClass("unfollow_button")
          $this.removeClass("follow_button")
        error: (data) ->
          alert "error"

      $.ajax params
      return false


  unfollowRestaurant: ->
    $this = $('.unfollow_button')
    params =
      url: '/follow'
      type: 'delete'
      data: "slug=" + @model.attributes.slug
      async: true
      success: (data) ->
        $this.html("Follow")
        $this.addClass("follow_button")
        $this.removeClass("unfollow_button")
      error: (data) ->
        alert "error"
        
    $.ajax params
    return false

  render: ->
    $(@el).html(@template(@model.toJSON()))

    @
    
  default: ->
    $(@el).html(@default_template())
    
    @

  selectWords: ->
    $this = @$('.add_category')
    if ($this.val() == "Add a category")
      $this.val("")

  deselectWords: ->
    $this = @$('.add_category')
    if ($this.val() == "")
      $this.val("Add a category")
  
  submitTagSubscription: (event) ->
    $this = @$('.add_category')
    subscription = new Foodstreet.Models.TagSubscription
      name: $this.val()
    subscription.save {},
      success: (data) ->
        window.profile.tag_subscriptions.add([subscription])
        @$('.add_category').val("")
        if (window.location.hash == '#stream' || window.location.hash == '#users/'+window.current_user_slug)
          streams_collection.fetch()
      error: ->
        alert "Something went wrong. Please try again later."
    return false