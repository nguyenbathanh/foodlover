Foodstreet.Views.Album ||= {}

class Foodstreet.Views.Album.IndexView extends Backbone.View
  initialize: ->
    _.bindAll(@, 'addOne', 'addAll', 'render')
    
    @options.albums.bind('add', @addOne)
    @options.albums.bind('reset', @addAll)
    
    
  addAll: ->
    @options.albums.each(@addOne)
  
  addOne: (album) ->
    view = new Foodstreet.Views.Album.AlbumView
      model  : album
    $("ul.albums_collection").append(view.render().el)
    
  render: ->
    @addAll()
    @