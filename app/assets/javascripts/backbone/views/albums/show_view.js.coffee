Foodstreet.Views.Album ||= {}

class Foodstreet.Views.Album.ShowView extends Backbone.View
  template: JST["backbone/templates/albums/show"]

  initialize: ->
    _.bindAll(@, 'addOne', 'addAll', 'render')
    
    @options.photos.bind('add', @addOne)
    @options.photos.bind('reset', @addAll)
    
    
  addAll: ->
    @options.photos.each(@addOne)
  
  addOne: (photo) ->
    view = new Foodstreet.Views.Photo.PhotoView
      model  : photo
      photos: @options.photos
    @$("li.photos").prepend(view.render().el)
    
  render: ->
    $(@el).html(@template(owner_name: @options.owner_name, album_name: @options.album_name))
    @addAll()
    @
