Foodstreet.Views.Album ||= {}

class Foodstreet.Views.Album.AlbumView extends Backbone.View
  template: JST["backbone/templates/albums/album_view"]

  render: ->
    $(@el).html(@template(@model.toJSON()))
    @
