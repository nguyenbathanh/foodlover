Foodstreet.Views.Messages ||= {}

class Foodstreet.Views.Messages.SingleConversationView extends Backbone.View
  template: JST["backbone/templates/messages/single_message"]

  render: ->
    if @model.collection != undefined
      @model.attributes.indexData = @model.collection.indexOf(@model)
    else
      @model.attributes.indexData = 1
      
    $(@el).html(@template(@model.toJSON()))

    @
