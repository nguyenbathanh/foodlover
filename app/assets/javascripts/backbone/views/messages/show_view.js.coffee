Foodstreet.Views.Messages ||= {}

class Foodstreet.Views.Messages.ShowView extends Backbone.View
  template: JST["backbone/templates/messages/show"]

  initialize: ->
    _.bindAll(@, 'addOne', 'addAll', 'render', 'addRecipient')

    @options.messages.bind('add', @addOne)
    @options.messages.bind('reset', @addAll)
    @options.messages.bind('reset', @addRecipient)
    @options.messages.bind('reset', @addReply)

  events:
    "click #reply-msg" : "replyMessage"

  addRecipient: ->
    window.Foodstreet.Public.ajaxRequest
      url: '/conversations/' + @options.slug + '/recipient'
      type: 'GET'
      success: (data) ->
        $('h1.page-title').html("Message With #{data.username}")
        $("#img-profile-reply").attr("src", window.profile.attributes.profile_image_68)

  replyMessage: ->
    $(".loading-reply").show()
    $('.message-post-reply').attr('readonly', true)
    body = $('.message-post-reply').val()
    $('.btn-skin').html("Sending...")
    slug = @options.slug
    if $('.message-post-reply').val() != ""
      conversations = new Foodstreet.Models.Conversation
        body: body
        conversation_id: slug
      conversations.url = '/conversations'
      conversations.save {},
        success: (data) =>
          @addOne(data)
          $(".message-post-reply").val("").attr('readonly', false)
          $('.btn-skin').html("Post Reply")
          $(".loading-reply").hide()
    else
      $('.message-post-reply').attr('readonly', false)
      $('.btn-skin').html("Post Reply")
      $(".loading-reply").hide()

  addAll: ->
    @options.messages.each(@addOne)

  addOne: (message) ->
    view = new Foodstreet.Views.Messages.SingleConversationView
      model  : message
      messages: @options.messages
      
    if message.collection != undefined
      # elem = "#main-expand"
      elem = "#conversations-msg"
    else
      elem = "#new-reply"
            
    @$(elem).append(view.render().el)

  render: ->
    $(@el).html(@template())
    @addAll()
    @
