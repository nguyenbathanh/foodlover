Foodstreet.Views.Messages ||= {}

class Foodstreet.Views.Messages.MessageView extends Backbone.View
  template: JST["backbone/templates/messages/message"]

  events:
    'change .mark_read' : 'markRead'

  markRead: ->
    if $('.mark_read').attr('checked') == "checked"
      window.Foodstreet.Public.ajaxRequest
        url: '/conversations/read'
        data: "id=" + @model.attributes.id
        success: (data) ->
          $thisRel = this.data
          $("li[rel='" + $thisRel + "']").removeClass('unread')
        error: ->
          alert "error"

  render: ->
    $(@el).html(@template(@model.toJSON()))

    @

