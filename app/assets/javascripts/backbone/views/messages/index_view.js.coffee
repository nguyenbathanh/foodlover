Foodstreet.Views.Messages ||= {}

class Foodstreet.Views.Messages.IndexView extends Backbone.View
  template: JST["backbone/templates/messages/index"]

  initialize: ->
    _.bindAll(@, 'addOne', 'addAll', 'render')

    @options.messages.bind('add', @addOne)
    @options.messages.bind('reset', @addAll)

  events:
    "click .wide_input.to" : "searchUsername"
    "click .send_button"   : "postMessage"
    "click #close-new-message" : "closeMessage"       
    
  closeMessage: ->
    $("#send-msg-success, #send-msg-failed").hide()
    $("#send-msg-form").show()
    $('#recipient-message, #body-message, #subject-message').attr('readonly', false).val("")
    $('.send_button').html("Send Message")    

  postMessage: ->
    recipient = $('#recipient-message').val()
    subject = $('#subject-message').val()
    body = $('#body-message').val()
    $('#recipient-message, #body-message, #subject-message').attr('readonly', true)
    if recipient != "" && subject != "" && body != ""
      conversation = new Foodstreet.Models.Message
        recipient: recipient
        subject: subject
        body: body
      conversation.save {},
        success: (data) =>
          message_collection.add([conversation])
          $("#send-msg-form").hide()
          $("#send-msg-success").show()          
          $('#recipient-message, #body-message, #subject-message').attr('readonly', false)
          $('.send_button').html("Send")    
        error: =>
          $("#send-msg-failed").show()          
          $('#recipient-message, #body-message, #subject-message').attr('readonly', false)
          $('.send_button').html("Rety")
    else
      $("#send-msg-failed").show()          
      $('#recipient-message, #body-message, #subject-message').attr('readonly', false)
      $('.send_button').html("Rety")

  searchUsername: ->
    window.Foodstreet.Public.ajaxRequest
      url: '/search/username'
      success: (data) ->
        #do nothing temporary

  addAll: ->
    if $("#new-msg").html() == ""      
      @options.messages.each(@addOne)

  addOne: (message) ->
    if $("#message-" + message.attributes.slug).length == 0
      view = new Foodstreet.Views.Messages.MessageView
        model  : message
        messages: @options.messages
      $("#new-msg").append(view.render().el)

  render: ->
    $(@el).html(@template())
    @addAll()
    
    @$("#recipient-message").autocomplete
      source: "/users/search.json"
      minLength: 2
      
    @
