Foodstreet.Views.Brand ||= {}

class Foodstreet.Views.Brand.IndexView extends Backbone.View
  template: JST["backbone/templates/brand/index"]

  render: ->
    $(@el).html(@template())

    @

