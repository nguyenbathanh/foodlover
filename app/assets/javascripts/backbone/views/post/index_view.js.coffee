Foodstreet.Views.Post ||= {}

class Foodstreet.Views.Post.IndexView extends Backbone.View
  template: JST["backbone/templates/post/index"]

  events:
    "click .post_like"        : "likePost"
    "click .post_unlike"      : "unlikePost"
    "keypress .comment_input" : "commentPost"
    "click .post_reshare"     : "resharePost"
    "click .post_repin"       : "repinPost"
    "click .post_flag"        : "flagPost"
    "click .post_promotion"   : "promotePost"

  commentPost: (event) ->
    if (window.current_user_id == '')
      window.location.href = '/login'
    else
      if $('.comment_input').val() != ""
        if (event.which == 13)
          $commentList = @$('ul.comments')
          message = $('.comment_input').val()
          params =
            url: '/comments'
            type: 'POST'
            data: 'id=' + @model.attributes.id + ";message=" + message
            success: (data) ->
              commentHTML = "
                <li class=\"comment_entry animation fadeIn new\">
                  <span class=\"commentor top\">
                    <a href=\"#\"><img src=\"#{data.image_32}\" /></a>
                  </span><span class=\"comment_message top\">
                    <div class=\"comment\">
                      <a href=\"#\" class=\"commentor_name\">#{data.author}</a><span class=\"message\">#{data.message}</span>
                    </div>
                    <div class=\"footer\">#{data.created_at_time}</div>
                  </span>
                </li>
              "
              $commentList.append(commentHTML)
              t = setTimeout("$('li.new').removeClass('new')", 4000)
              $('.comment_input').val("")
              comment_counter = $('span').closest(".comment_count", $commentList)
              new_count = parseInt(comment_counter.text().split(' ')[0]) + 1
              if (new_count == 1)
                counter_object = ' Comment'
              else
                counter_object = ' Comments'
              comment_counter.text(new_count + counter_object)
            error: (data) ->
              alert "error"
      
          $.ajax params
          return false

  likePost: ->
    if (window.current_user_id == '')
      window.location.href = '/login'
    else
      $this = $('.post_like')
      $commentList = @$('ul.comments')
      params =
        url: '/like'
        type: 'POST'
        data: "id=" + @model.attributes.id
        async: true
        success: (data) ->
          $this.html("unlike")
          like_counter = $('span').closest(".like_count", $this)
          new_count = parseInt(like_counter.text().split(' ')[0]) + 1
          like_counter.text(new_count + ' Liked')
          $this.addClass("post_unlike")
          $this.removeClass("post_like")
          statusHTML = "
            <li class=\"status animation fadeIn\">You like this.</li>
          "
          $commentList.prepend(statusHTML)
        error: (data) ->
          alert "error"
      $.ajax params
      return false

  unlikePost: ->
    $this = $('.post_unlike')
    $commentList = @$('ul.comments')
    params =
      url: '/like'
      type: 'delete'
      data: "id=" + @model.attributes.id
      async: true
      success: (data) ->
        $this.html("Like")
        like_counter = $('span').closest(".like_count", $this)
        new_count = parseInt(like_counter.text().split(' ')[0]) - 1
        like_counter.text(new_count + ' Liked')
        $this.addClass("post_like")
        $this.removeClass("post_unlike")
        $commentList.find("li.status").hide()
      error: (data) ->
        alert "error"

    $.ajax params
    return false

  render: ->
    $(@el).html(@template(@model.toJSON()))

    @

  resharePost: ->
    $this = @$('.post_reshare')
    $commentList = @$('ul.comments')
    $reshare_counter = @$('.reshare_count')
    params =
      url: '/posts/' + @model.attributes.id + '/reshare'
      type: 'POST'
      async: true
      success: (data) ->
        statusHTML = "
          <li class=\"status animation fadeIn\">You reshare this.</li>
        "
        $commentList.prepend(statusHTML)
        new_count = parseInt($reshare_counter.text().split(' ')[0]) + 1
        $reshare_counter.text(new_count + ' Reshared')
      error: (data) ->
        alert "error"

    $.ajax params
    return false

  repinPost: ->
    $.colorbox({href:"/posts/" + @model.attributes.id + "/repin"})
    return false
    
  flagPost: ->
    $.colorbox({href:"/posts/" + @model.attributes.id + "/flag"})
    return false
    
  promotePost: ->
    $this = @$('.post_promotion')
    $commentList = @$('ul.comments')
    params =
      url: '/posts/' + @model.attributes.id + '/promote'
      type: 'POST'
      async: true
      success: (data) ->
        statusHTML = "
          <li class=\"status animation fadeIn\">You promoted this.</li>
        "
        $commentList.prepend(statusHTML)
        $this.text('Promoted').removeClass("post_promotion").addClass("disable_post_promotion")
      error: (data) ->
        alert "error"

    $.ajax params
    return false