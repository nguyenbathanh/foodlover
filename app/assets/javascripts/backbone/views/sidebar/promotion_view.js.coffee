Foodstreet.Views.SideBar ||= {}

class Foodstreet.Views.SideBar.PromotionView extends Backbone.View
  template: JST["backbone/templates/sidebar/promotion"]

  initialize: ->
    _.bindAll(@, 'addOne', 'addAll', 'render')

    @options.outlets.bind('add', @addOne)
    @options.outlets.bind('reset', @addAll)

  addAll: ->
    @options.outlets.each(@addOne)

  addOne: (outlet) ->
    view = new Foodstreet.Views.SideBar.PromotedOutletView
      model   : outlet
      outlets : @options.outlets
    @$(".promoted_wrapper").prepend(view.render().el)

  render: ->
    $(@el).html(@template())
    @addAll()
    @


