Foodstreet.Views.SideBar ||= {}

class Foodstreet.Views.SideBar.PromotedOutletView extends Backbone.View
  template: JST["backbone/templates/sidebar/promoted"]

  render: ->
    $(@el).html(@template(@model.toJSON()))
    @


