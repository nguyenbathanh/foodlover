Foodstreet.Views.SideBar ||= {}

class Foodstreet.Views.SideBar.FeaturedOutletView extends Backbone.View
  template: JST["backbone/templates/sidebar/featured_outlet"]

  events:
    "click .follow_button"   : 'followRestaurant'
    "click .unfollow_button" : 'unfollowRestaurant'

  followRestaurant: ->
    $this = @$('.follow_button')
    params =
      url: '/follow'
      type: 'POST'
      data: "slug=" + @model.attributes.slug
      async: true
      success: (data) ->
        $this.html("Following")
        $this.addClass("unfollow_button button_white")
        $this.removeClass("follow_button button_gray")
      error: (data) ->
        alert "error"

    $.ajax params
    return false


  unfollowRestaurant: ->
    $this = @$('.unfollow_button')
    params =
      url: '/follow'
      type: 'delete'
      data: "slug=" + @model.attributes.slug
      async: true
      success: (data) ->
        $this.html("Follow")
        $this.addClass("follow_button button_gray")
        $this.removeClass("unfollow_button button_white")
      error: (data) ->
        alert "error"

    $.ajax params
    return false

  render: ->
    $(@el).html(@template(@model.toJSON()))
    @





