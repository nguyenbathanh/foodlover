Foodstreet.Views.SideBar ||= {}

class Foodstreet.Views.SideBar.FeaturedView extends Backbone.View
  template: JST["backbone/templates/sidebar/index"]

  initialize: ->
    _.bindAll(@, 'addOne', 'addAll', 'render')

    @options.outlets.bind('add', @addOne)
    @options.outlets.bind('reset', @addAll)

  addAll: ->
    @options.outlets.each(@addOne)

  addOne: (outlet) ->
    view = new Foodstreet.Views.SideBar.FeaturedOutletView
      model   : outlet
      outlets : @options.outlets
    @$(".restaurant_wrapper").prepend(view.render().el)

  render: ->
    $(@el).html(@template())
    @addAll()
    @





