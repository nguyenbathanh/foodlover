Foodstreet.Views.User ||= {}

class Foodstreet.Views.User.IndexView extends Backbone.View
  template: JST["backbone/templates/users/index"]

  initialize: ->
    _.bindAll(@, 'addOne', 'addAll', 'render')

    @options.streams.bind('add', @addOne)
    @options.streams.bind('reset', @addAll)

  addAll: ->
    @options.streams.each(@addOne)

  addOne: (stream) ->
    view = new Foodstreet.Views.Stream.StreamView
      model  : stream
      streams: @options.streams
    @$(".stream_wrapper").prepend(view.render().el)

  render: ->
    $(@el).html(@template())
    @addAll()

    @


