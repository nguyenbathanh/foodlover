Foodstreet.Views.User ||= {}

class Foodstreet.Views.User.Edit extends Backbone.View
  template: JST["backbone/templates/users/edit"]
  
  render: ->
    $(@el).html(@template(@model.toJSON()))
    @
