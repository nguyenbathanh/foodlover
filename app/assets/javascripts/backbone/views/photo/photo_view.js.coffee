Foodstreet.Views.Photo ||= {}

class Foodstreet.Views.Photo.PhotoView extends Backbone.View
  template: JST["backbone/templates/photo/photo"]

  render: ->
    $(@el).html(@template(@model.toJSON()))
    @
