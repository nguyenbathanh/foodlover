Foodstreet.Views.Photo ||= {}

class Foodstreet.Views.Photo.IndexView extends Backbone.View
  template: JST["backbone/templates/photo/index"]

  initialize: ->
    _.bindAll(@, 'addOne', 'addAll', 'render', 'showAlbums')
    
    @options.photos.bind('add', @addOne)
    @options.photos.bind('reset', @addAll)
    
    
  addAll: ->
    @options.photos.each(@addOne)
  
  addOne: (photo) ->
    view = new Foodstreet.Views.Photo.PhotoView
      model  : photo
      photos: @options.photos
    @$("li.photos").prepend(view.render().el)
    
  render: ->
    $(@el).html(@template(owner_name: @options.owner_name))
    @addAll()
    @showAlbums(@options.slug)
    @

  showAlbums: (user_slug) ->
    @albums = new Foodstreet.Collections.AlbumsCollection()
    @albums.fetch
      data:
        slug: user_slug
      success: (model, response) ->
        @view = new Foodstreet.Views.Album.IndexView
          albums: model
        @view.render().el