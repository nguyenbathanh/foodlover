Foodstreet.Views.Friends ||= {}

class Foodstreet.Views.Friends.IndexView extends Backbone.View
  template: JST["backbone/templates/friends/index"]
  
  initialize: ->
    _.bindAll(@, 'addOne', 'addAll', 'render')

    @options.friends.bind('add', @addOne)
    @options.friends.bind('reset', @addAll)

  addAll: ->
    @options.friends.each(@addOne)

  addOne: (friend) ->
    view = new Foodstreet.Views.Friends.FriendView
      model: friend
      friends: @options.friends
    @$(".foodies").prepend(view.render().el)

  render: ->
    $(@el).html(@template())
    @addAll()
    @

