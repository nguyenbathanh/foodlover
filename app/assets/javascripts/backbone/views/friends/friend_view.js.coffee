Foodstreet.Views.Friends ||= {}

class Foodstreet.Views.Friends.FriendView extends Backbone.View
  template: JST["backbone/templates/friends/friend"]

  events:
    "click .follow_button"   : 'followRestaurant'
    "click .unfollow_button" : 'unfollowRestaurant'

  followRestaurant: ->
    if (window.current_user_id == '')
      window.location.href = '/login'
    else
      $this = @$('.follow_button')
      params =
        url: '/follow'
        type: 'POST'
        data: "slug=" + @model.attributes.slug
        async: true
        success: (data) ->
          $this.html("Following")
          $this.addClass("unfollow_button button_white")
          $this.removeClass("follow_button button_green")
        error: (data) ->
          alert "error"

      $.ajax params
      return false


  unfollowRestaurant: ->
    $this = @$('.unfollow_button')
    params =
      url: '/follow'
      type: 'delete'
      data: "slug=" + @model.attributes.slug
      async: true
      success: (data) ->
        $this.html("Follow")
        $this.addClass("follow_button button_green")
        $this.removeClass("unfollow_button button_white")
      error: (data) ->
        alert "error"

    $.ajax params
    return false

  render: ->
    $(@el).html(@template(@model.toJSON()))
    @
