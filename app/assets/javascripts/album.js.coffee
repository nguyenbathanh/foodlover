window.sortable = ->
  $(".sortable").sortable(
    {opacity: 0.6, cursor: 'crosshair', revert: true, scrollSensitivity: 40,
    start: -> $(".draggable").css("border", '3px dotted gray'),
    stop: -> $(".draggable").css("border", 'none'),
    update: ->
      array_tags = []
      $(".album_destroy").each -> array_tags.push($(this).attr('id'))
      $.ajax({url: "/current_user/update_album_position", type: "get",
      data:{positions: array_tags }, dataType: "json"})
    })


delete_dialog = (id_of_photo_to_delete = 'nil') =>
  if id_of_photo_to_delete != 'nil'
    current_user = $('body').data('current_user')
    url_to_set = "/"+current_user+"/albums/" + id_of_photo_to_delete + "/remove"
    $(".actions .action.delete a").attr("href", url_to_set)

  jQuery('.dialog_overlay').toggle()
  jQuery('.dialog.delete').toggle()
  jQuery('.dialog.delete').css ({
  top: "34%",
  left: "50%",
  "margin-left": "-" + (jQuery('.dialog.delete').width() / 2) + "px"
  "margin-top": "-" + (jQuery('.dialog.delete').height() / 2) + "px"
  })

close_edit = (title)->
  title = jQuery('.photos_wrapper .label_large .title input').val() if not title

  data_slug = jQuery('.photos_wrapper .label_large .title').attr 'data-slug'
  data_quantity = jQuery('.photos_wrapper .label_large .title').attr 'data-quantity'

  jQuery('.photos_wrapper .label_large .title').html data_slug + "'s Album " + '"' + title + '" (' + data_quantity + ')'
  jQuery('.photos_wrapper .label_large .title').attr 'data-title', title
  jQuery('.photos_wrapper .label_large .title').attr 'data-opened', ''
# toggle_edit()


#function_for_toggle_edit = ->


toggle_edit = ->
  $('.action_om.edit').live "click", () ->
    $(this).closest(".album_header").find(".rename.rename_om").show()
    $(this).closest(".album_header").find(".album_options, .album_destroy, .header_text").hide()
    $(this).closest(".album_header").find(".rename.rename_om form input.text").focus().select()



  jQuery('.photos_wrapper .label_large .actions .edit').click ->
    return if jQuery('.photos_wrapper .label_large .title').attr 'data-opened'
    jQuery(@).attr 'data-opened', true
    data_title = jQuery('.photos_wrapper .label_large .title').attr 'data-title'
    data_slug = jQuery('.photos_wrapper .label_large .title').attr 'data-slug'
    data_quantity = jQuery('.photos_wrapper .label_large .title').attr 'data-quantity'
    jQuery('.photos_wrapper .label_large .title').html '<form action="' + window.location.pathname + '/update" method="get"><input name="new_tag_name" class="text" type="text" value="' + data_title + '"/> <button class="submit" type="submit"></button></form>'

    jQuery('.photos_wrapper .label_large .title .text').keyup (e)->
      if (e.keyCode == 27)
        close_edit(data_title)

    jQuery('.photos_wrapper .label_large .title button').click ->
      new_name = jQuery('.photos_wrapper .label_large .title .text').val()
      new_name = new_name.replace(/[^a-zA-Z0-9]/g, ' ').replace(/^\s*/g, '').replace(/\s*$/g, '')
      close_edit(new_name)
      window.location = window.location.pathname + '/update?new_tag_name=' + new_name


jQuery(document).ready ->
  toggle_edit()
  window.sortable() if String(location.href).split("/")[3] == $('body').data('current_user')


  ###############################################
  # Delete dialog
  jQuery('.photos_wrapper .dialog_overlay').appendTo 'body'
  jQuery('.photos_wrapper .dialog.delete').appendTo 'body'

  jQuery('.dialog.delete .action.cancel, .dialog.delete .header .close').click ->
    jQuery('.dialog_overlay').toggle()
    jQuery('.dialog.delete').toggle()


  jQuery('.action_om.delete').live "click", () ->
    id_of_photo_to_delete = $(this).attr("id")
    delete_dialog(id_of_photo_to_delete)


  jQuery('.photos_wrapper .label_large .actions .action.delete').click ->
    delete_dialog()

  ###############################################

  # return
  ###############################################
  # Album grid
  # Detect '.container' width and fixs it's width
  container_width = jQuery('.container').width()
  jQuery('.container').width(container_width)

  # Detect a single '.photo' width as reference
  photo_width = jQuery('.photo:first').outerWidth(true)
  # If there aren't photos, we have nothing to do with columns and photos.
  if photo_width
    # Calculate number of columns
    columns_q = Math.floor(container_width / photo_width)

    # Creates columns
    jQuery('div.album').append('<ul class="column"></ul>') for col in [1..columns_q]

    # Fixs the album column by photo_width 
    jQuery('div.album .column').width photo_width

    # Append each photo to a column
    col_it = 1
    jQuery('.photo').each ->
      jQuery(@).appendTo 'div.album .column:nth-child(' + col_it + ')'
      if ((col_it / columns_q) == 1)
        col_it = 1
      else
        col_it = col_it + 1

###############################################
    
      
