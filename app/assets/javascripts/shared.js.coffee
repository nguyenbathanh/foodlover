#= require underscore

#= require jquery
#= require jquery.tokeninput
#= require jquery.tagsinput
#= require jquery-ui-1.8.21.custom.min
#= require uploader/jquery.iframe-transport
#= require uploader/jquery.fileupload

#= require backbone
#= require backbone_rails_sync
#= require backbone_datalink
#= require backbone_router_filter
#= require backbone_multiple_routes

#= require backbone/application
