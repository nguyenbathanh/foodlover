#= require jquery
#= require jquery_ujs
#= require underscore
#= require backbone
#= require backbone_rails_sync
#= require backbone_datalink
#= require backbone_router_filter
#= require backbone_multiple_routes
#= require jquery.validate
#= require jquery.reveal
#= require jquery.tokeninput
#= require jquery.htmlClean-min
#= require jquery.caret.1.02.min
#= require jquery-ui-1.8.21.custom.min
#= require auto
#= require jquery.colorbox-min
#= require backbone/foodstreet.v2
  
window.escapeTag = (str) ->
  return str.replace(/\([a-zA-Z0-9_ ]*\)/g, (m) ->
    return m.replace('(', '').replace(')', '')).replace(/{[a-zA-Z0-9_ ]*}/g, (m) ->
      return m.replace('{', '').replace('}', '')).replace(/@\w+/g, (m) ->
        return m.replace('@', ''))
        
window.escapeAlbumPrefix = (str) ->
  return str.replace(/grouped\sunder\s/g, '')
