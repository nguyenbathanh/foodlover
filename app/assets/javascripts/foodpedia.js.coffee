#= require shared

#= require bootstrap
#= require bootstrap-transition
#= require jquery.masonry.min

$ ->
  $('#posts .list.posts').masonry
    itemSelector: 'li.post'