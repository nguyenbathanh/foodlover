$(document).ready ->
  $('.profile .two-sides-panel .right').height $(".two-sides-panel").height()
  $('.profile .two-sides-panel .left').height $(".two-sides-panel").height()

  # Change cover image button
  # Only excecuted if the user is browsing in his own profile
  if $('.change-profile-cover').size()
    $('.profile-banner .img').hover ->
      $('.change-profile-cover').show()
    , ->
      $('.change-profile-cover').hide()
      
  # Change cover photo button
  # Only excecuted if the user is browsing in his own profile
  if $('.change-profile-avatar').size()
    $('.profile-banner .user-avatar-wrapper').hover ->
      $('.change-profile-avatar').show()
    , ->
      $('.change-profile-avatar').hide()

