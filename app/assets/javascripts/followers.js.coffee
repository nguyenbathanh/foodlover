jQuery(document).ready ->
#  Follow restaurant
  jQuery('.follow_button').click ->
    if (window.current_user_id == '')
      window.location.href = '/login'
    else
      $this = jQuery(this)
      params =
        url: '/follow'
        type: 'POST'
        data: "slug=" + jQuery(this).attr('slug')
        async: true
        success: (data) ->
          $this.html("Following")
          $this.addClass("unfollow_button button_white")
          $this.removeClass("follow_button button_green")
        error: (data) ->
          alert "error"

      $.ajax params
      return false
      
# Unfollow restaurant
  jQuery('.unfollow_button').click ->
    $this = jQuery(this)
    params =
      url: '/follow'
      type: 'delete'
      data: "slug=" + jQuery(this).attr('slug')
      async: true
      success: (data) ->
        $this.html("Follow")
        $this.addClass("follow_button button_green")
        $this.removeClass("unfollow_button button_white")
      error: (data) ->
        alert "error"

    jQuery.ajax params
    return false
  
