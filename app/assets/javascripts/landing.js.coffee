$(document).ready ->
  $("input.search_input").focus( ->
    if ($(this).val() == "Find food here...")
      $(this).val("")
      $this.css({'color':'#606060', 'background':'#FFFFFF'})
  )

  $("input.search_input").blur( ->
    if ($(this).val() == "")
      $(this).val("Find food here...")
      $(this).css({'background':'#0070C0','color':'#FFFFFF'})
    else 
      $(this).css({'color':'#606060', 'background':'#FFFFFF'})
  )
