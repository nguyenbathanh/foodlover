#= require shared

#= require jquery.validate
#= require jquery.reveal
#= require jquery.htmlClean-min
#= require jquery.caret.1.02.min
#= require jquery.colorbox-min
#= require jquery_ujs
#= require modernizr-2.5.3.min

#window.addPost = (data) ->
#  model = new Foodstreet.Models.Stream()
#  model.id = data._id
#  model.set
#    id: data._id
#    like: data.like
#    message: data.message
#    post_image: data.image_200
#    created_at_time: data.created_at_time
#    username: data.username
#    profile_image: data.image_68
#    like_count: data.like_count
#    comment_count: data.comment_count
#    user_id: window.current_user_id
#    creator_slug: data.creator_slug
#  streams_collection.add([model])
##  $("#new_post").attr('src', '/posts/new')
#
##  $.map(data.all_taggings, (n) ->
##    window.availableTags.push(n)
##  )
##  window.availableTags = jQuery.unique(window.availableTags)
  
window.escapeTag = (str) ->
  return str.replace(/\([a-zA-Z0-9_ ]*\)/g, (m) ->
    return m.replace('(', '').replace(')', '')).replace(/{[a-zA-Z0-9_ ]*}/g, (m) ->
      return m.replace('{', '').replace('}', '')).replace(/@\w+/g, (m) ->
        return m.replace('@', ''))
        
window.escapeAlbumPrefix = (str) ->
  return str.replace(/grouped\sunder\s/g, '')

$(document).ready ->
  $("form#user_new").validate(
    rules:
      'user[username]':
        required: true
        minlength: 2
      'user[email]':
        required: true
        email: true
      'user[password]':
        required: true
        minlength: 6
    messages:
      'user[username]':
        required: "* Please provide a username"
        minlength: "* Your username must consist of at least 2 characters" 
      'user[email]':
        required: "* Please provide your email"
        email: "* Please provide a valid email"
      'user[password]':
        required: "* Please provide a password"
        minlength: "* Your password must be at least 6 characters long"
  )
