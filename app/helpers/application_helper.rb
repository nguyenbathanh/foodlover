module ApplicationHelper
  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = "current #{sort_direction}"
    direction = sort_direction == "asc" ? "desc" : "asc"
    link_to title, {:sort => column, :direction => direction}, {:class => css_class}
  end

  def page_class
    controller.controller_name.dasherize + " " + controller.action_name.dasherize
  end
  
  # Set header title
  def set_title(name)
    @title_name = name
  end
  
  # Get header title
  def title
    @title_name || "I am food lover"
  end

  def time_distance_to_now(happened_at)
    distance_of_time_in_words_to_now(happened_at, true) + " ago"
  end

  def link_to_user(user)
    link_to user.slug, slug_path(user.slug), :class => 'link'
  end

  def link_to_post(post)
    link_to "post "+truncate(strip_tags(post.message), :length => 120), single_post_path(post.id), :class => 'link'
  end

  def append_notified_time(notification_info, notification)
    notification_info.gsub("*timedistance*", "<div class='notified-time'>
    #{time_distance_to_now(notification.created_at)}</div>")
  end

end
