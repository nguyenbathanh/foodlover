module V2::ActivitiesHelper
#TODO Need to remove after everything goes smooth.
=begin
  def activity_info(activity)
    raw activity.actor.slug + ' ' + case activity.event_type
    when 'followed'
      "followed #{link_to activity.subject.slug, v2_path(activity.subject.slug)}"
    when 'commented'
      "commented on a post #{link_to truncate(strip_tags(activity.secondary_subject.message), :length => 120), v2_post_path(activity.subject.id)}."
    when 'subscribed'
      tag = activity.actor.tag_subscriptions.find(activity.subject_id).try(:name)
      "subscribed to food tag #{link_to tag, tagged_with_v2_posts_path(tag)}"
    when 'pinned'
      "repinned a photo #{link_to truncate(strip_tags(activity.subject.message), :length => 120), v2_post_path(activity.subject.id)}."
    when 'shared'
      "shared a post #{link_to truncate(strip_tags(activity.subject.message), :length => 120), v2_post_path(activity.subject.id)} to Facebook and/or Twitter."
    when 'tagged_food'
      tags = activity.subject.food_tag_list
      msg = "tagged food "
      arr = []
      tags.each {|t| arr << "#{link_to t, tagged_with_v2_posts_path(t)}"}
      msg += "#{arr.join(', ')} in post #{link_to truncate(strip_tags(activity.subject.message), :length => 120), v2_post_path(activity.subject.id)}."
    when 'tagged_user'
      "tagged #{link_to activity.subject.slug, v2_path(activity.subject.slug)} in post #{link_to truncate(strip_tags(activity.secondary_subject.message), :length => 120), v2_post_path(activity.secondary_subject.id)}."
    when 'liked'
      "love a post #{link_to truncate(strip_tags(activity.subject.message), :length => 120), v2_post_path(activity.subject.id)}."
    else
      ''
    end      
  end
=end

  def append_actor_picture(user) 
    image_tag user.profile_image_34,
      :class => user.profile_image_34 == "sample-user.png" ? "profile-image profile-image-size" : "profile-image"
  end

  def activity_info(activity)
    activity_info = ""
    activity_info << append_actor_picture(activity.actor)
    activity_info << append_activity_icon(activity)
    activity_info << append_activity_message(activity)
    append_notified_time(activity_info, activity).html_safe    
  end

  def append_activity_icon(activity)
    activity_icons = { "tagged_food" => "new_images/tag_icon.png", "tagged_user" => "new_images/tag_icon.png",
      "followed" => "new_images/following_icon_new.png", "liked" => "new_images/like_icon.png",
      "commented" => "new_images/comment_icon.png", "pinned" => "new_images/pin_icon.png",
      "shared" => "new_images/share_icon.png", "subscribed" => "new_images/subscribed_icon.png" 
    }
    if activity_icons[activity.event_type]
      image_tag activity_icons[activity.event_type], :class => 
        ["tagged_food", "tagged_user"].include?(activity.event_type) ? "small-tag-icon" : ""
    else
      ""
    end
  end

  def append_activity_message(activity)
    case activity.event_type
    when "followed"
      "<div class='message'><div>#{link_to_user(activity.actor)} followed 
        #{link_to_user(activity.subject)}.</div>*timedistance*</div>"
    when "commented"
      "<div class='message'><div>#{link_to_user(activity.actor)} commented on a 
        #{link_to_post(activity.subject)}.</div>*timedistance*</div>"
    when "liked"
      "<div class='message'><div>#{link_to_user(activity.actor)} love a 
        #{link_to_post(activity.subject)}.</div>*timedistance*</div>"
    when "shared"
      "<div class='message'><div>#{link_to_user(activity.actor)} shared a 
        #{link_to_post(activity.subject)} to Facebook and/or Twitter.</div>*timedistance*</div>"
    when "pinned"
      "<div class='message'><div>#{link_to_user(activity.actor)} repinned photo of a
        #{link_to_post(activity.subject)}.</div>*timedistance*</div>"
    when "subscribed"
      tag = activity.actor.tag_subscriptions.find(activity.subject_id).try(:name)
      "<div class='message'><div>#{link_to_user(activity.actor)} subscribed to food tag 
        #{link_to tag, food_path(tag), :class => "link"}.</div>*timedistance*</div>"
    when "tagged_user"
      "<div class='message'><div>#{link_to_user(activity.actor)} tagged #{link_to_user(activity.subject)} 
        in #{link_to_post(activity.secondary_subject)}.</div>*timedistance*</div>"
    when "tagged_food"
      tags = activity.subject.food_tag_list
      tags.collect! { |tag| link_to tag, food_path(tag), :class => "link" }
      "<div class='message'><div>#{link_to_user(activity.actor)} tagged food #{tags.join(", ")} 
        in #{link_to_post(activity.subject)}.</div>*timedistance*</div>"
    else
      ""
    end
  end
end
