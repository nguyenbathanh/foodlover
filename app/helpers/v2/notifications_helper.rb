module V2::NotificationsHelper

  def notifications_count
    #TODO Need to fix count in future after second level comment notifications implemented.
    "Notifications (#{current_user.notifications.unread.size})"
  end

  def notification_info(notification)
    notification_info = ""
    notification_info << append_profile_picture(notification)
    notification_info << append_icon(notification)
    case notification.event_type
    when 'friend_joins'
      notification_info << "<div class='message'><div>Your Facebook friend #{link_to_user(notification.subject)} 
      is on IMFL. Follow them?</div>*timedistance*</div>"
    when 'being_tagged'
      notification_info << "<div class='message'><div>#{link_to_user(notification.subject.user)} 
      tagged you in #{link_to_post(notification.subject)}.</div>*timedistance*</div>"
    when 'being_followed'
      notification_info << "<div class='message'><div>#{link_to_user(notification.subject)} is 
      now following you on ImFoodLover.</div>*timedistance*</div>"
    when 'being_liked'
      notification_info << "<div class='message'><div>#{link_to_user(notification.secondary_subject)} love your 
      #{link_to_post(notification.subject)}.</div>*timedistance*</div>"
    when 'being_commented'
      if notification.secondary_subject_type == "User"
        user = notification.secondary_subject
      elsif notification.secondary_subject_type == "Comment"
        user = notification.secondary_subject.user
      end
      notification_info << "<div class='message'><div>#{link_to_user(user)} 
      commented on your #{link_to_post(notification.subject)}.</div>*timedistance*</div>"
    when 'being_repinned'
      notification_info << "<div class='message'><div>#{link_to_user(notification.secondary_subject)} 
      repinned your photo #{link_to_post(notification.subject)}.</div>*timedistance*</div>"
    when 'being_shared'
      notification_info << "<div class='message'><div>#{link_to_user(notification.secondary_subject)} 
      shared your #{link_to_post(notification.subject)} to Facebook and/or Twitter.</div>*timedistance*</div>"
    end
    
    #append notified time
    append_notified_time(notification_info, notification).html_safe
  end

  def append_icon(notification)
    notification_icons = { "friend_joins" => "new_images/like_icon.png", "being_tagged" => "new_images/like_icon.png",
      "being_followed" => "new_images/following_icon_new.png", "being_liked" => "new_images/like_icon.png",
      "being_commented" => "new_images/comment_icon.png", "being_repinned" => "new_images/pin_icon.png",
      "being_shared" => "new_images/share_icon.png"
    }
    image_tag notification_icons[notification.event_type]
  end

  def append_profile_picture(notification)
    if %w(friend_joins being_followed).include?(notification.event_type)
      subject = notification.subject
    elsif %w(being_liked being_repinned being_shared).include?(notification.event_type)
      subject = notification.secondary_subject
    elsif notification.event_type == "being_commented"
      if notification.secondary_subject_type == "User"
        subject = notification.secondary_subject
      elsif notification.secondary_subject_type == "Comment"
        subject = notification.secondary_subject.user
      end
    else
      subject = notification.subject.user
    end
    image_tag subject.profile_image_34,
      :class => subject.profile_image_34 == "sample-user.png" ? "profile-image profile-image-size" : "profile-image"
  end
end
