class Notification
  include Mongoid::Document
  include Mongoid::Timestamps
  
  belongs_to :user
  belongs_to :subject, polymorphic: true
  belongs_to :secondary_subject,  polymorphic: true
  
  field :event_type, type: String
  field :read, type: Boolean, default: false
  
  scope :unread, where(:read => false)
  scope :read, where(:read => true)
  
  EVENT_TYPE = ['being_tagged', 'friend_joins', 'being_followed', 'being_liked', 'being_commented', 'being_repinned', 'being_shared']
end
