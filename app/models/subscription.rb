class Subscription
  include Mongoid::Document

  belongs_to :food, index: true
  belongs_to :user, index: true
end