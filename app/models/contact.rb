class Contact
  include Mongoid::Document
  
  belongs_to :user
  
  field :name, type: String
  field :email, type: String
  
  scope :gmail, where(:_type => "Gmail")
  scope :yahoo, where(:_type => "Yahoo")
  scope :hotmail, where(:_type => "Hotmail")
end
