require "devise_fields"

class User < Entity
  include DeviseFields
  # Include default devise modules. Others available are:
  # :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :token_authenticatable

  has_many :pictures
  has_many :albums
  has_many :subscriptions
  has_many :albumpositions

  field :facebook_id,  type: Integer, default: 0
  field :token,        type: String
  # 1 - normal user
  # 2 - merchant
  # 3 - admin
  field :role,         type: Integer, default: 1
  field :about,        type: String
  field :website,      type: String
  field :devise_token, type: String

  #TODO add Authentication belongs_to User to store OAuth authentications
  field :twitter_oauth_token, type: String
  field :twitter_oauth_secret, type: String
  field :weibo_uid,    type: String
  field :weibo_access_token,    type: String
  field :weibo_access_token_secret,    type: String
  field :tqq_uid,    type: String
  field :tqq_access_token,    type: String
  field :tqq_access_token_secret,    type: String

  field :auto_publish_facebook, type: Boolean, default: true
  field :auto_publish_twitter, type: Boolean, default: true
  field :gmail_authenticated, type: Boolean, default: false
  field :yahoo_authenticated, type: Boolean, default: false
  field :hotmail_authenticated, type: Boolean, default: false
  field :country, type: String
  field :active, type: Boolean, default: true
  field :notification_settings, type: Hash, default: {
    :friend_joins => '1', :being_tagged => '1', :being_followed => '1', 
    :being_liked => '1', :being_commented => '1', :being_repinned => '1', 
    :being_shared => '1'
  }
  field :email_notification_settings, type: Hash, default: {
    :friend_joins => '1', :being_tagged => '1', :being_followed => '1', 
    :being_liked => '1', :being_commented => '1', :being_repinned => '1', 
    :being_shared => '1'
  }
  field :invited_by, type: String
  field :credits_count, type: Integer, default: 0
  field :points_count, type: Integer, default: 0
  field :popularities_count, type: Integer, default: 0

  field :folling_count
  field :follow_count

  has_many :relationships, dependent: :delete
  has_many :followships, dependent: :delete
  has_many :likeships, dependent: :delete
  has_many :posts, dependent: :delete
  has_many :comments, dependent: :delete
  has_many :notifications, dependent: :delete
  has_many :subject_notifications, class_name: 'Notification', as: :subject, dependent: :destroy
  has_many :secondary_subject_notifications, class_name: 'Notification', as: :secondary_subject, dependent: :destroy
  has_many :contacts, dependent: :delete
  has_many :gmails
  has_many :hotmails
  has_many :yahoos
  has_many :timelines, :as => :actor, dependent: :destroy
  has_many :invitees, class_name: 'User', foreign_key: 'invited_by'
  belongs_to :inviter, :class_name => 'User', :foreign_key => 'invited_by'
  has_many :rewards
  has_many :points
  has_many :credits
  has_many :popularities
  has_and_belongs_to_many :badges
  embeds_many :tag_subscriptions

  validates_presence_of :username
  validates_format_of :website, :with => /(^$)|(^(http|https):\/\/[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(([0-9]{1,5})?\/.*)?$)/ix
  validates_uniqueness_of :facebook_id, :allow_nil => true, :allow_blank => true
  
  before_save :ensure_authentication_token
  after_create :create_suggest_notification_on_freinds
  after_create :reward_user_points
  after_create :reward_user_credits
  after_create :reward_inviter_points, :if => Proc.new {|u| u.inviter.present?}
  after_create :reward_inviter_credits, :if => Proc.new {|u| u.inviter.present?}

  api_accessible :you do |u|
    u.add :slug
    u.add :following_count
    u.add :follow_count
    u.add :username
    u.add :email
    u.add :profile_image_24
    u.add :profile_image_68
    u.add :followers, template: :followers
    u.add :following, template: :following
    u.add :follow
    u.add :unread_notification, as: :notification_count
    u.add :id
    u.add :tag_subscriptions, template: :tag_subscriptions
    u.add :role
  end

  api_accessible :show do |u|
    u.add :slug
    u.add :following_count
    u.add :follow_count
    u.add :username
    u.add :email
    u.add :profile_image_24
    u.add :profile_image_68
    u.add :followers, template: :followers
    u.add :following, template: :following
    u.add :follow
    u.add :unread_notification, as: :notification_count
    u.add :id
    u.add :tag_subscriptions, template: :tag_subscriptions
  end
  
  api_accessible :edit do |u|
    u.add :slug
    u.add :id
    u.add :username
    u.add :email
    u.add :profile_image_68
  end

  api_accessible :foodies do |u|
    u.add :slug
    u.add :profile_image_50
    u.add :username
    u.add :follow
  end

  api_accessible :recipient do |u|
    u.add :username
  end

  api_accessible :seach do |u|
    u.add :id
    u.add :username, as: :label
  end

  

  def self.lookup(query)
    return [] if query.to_s.empty?
    where(slug: Regexp.new("^#{query}", Regexp::IGNORECASE))
  end
  
  # Return all albums tags name from all posts of the user
  def album_tags
    self.posts.all.collect {|p| p[:album_tag_list] }.flatten.uniq.compact
  end

  def follow
    if !User.current
      false
    elsif User.current.id == self.id
      return "self"
    else
      User.current.follow? self
    end
  end

  def profile_image_24
    self.profile_image ? self.profile_image.thumb('24x24#').url : '/assets/sample-user.png'
  end

  def profile_image_68
    self.profile_image ? self.profile_image.thumb("68x68#").url : '/assets/sample-user.png'
  end

  def profile_image_34
    #image_tag automatically prefix /assets/ for you.
    self.profile_image ? self.profile_image.thumb("34x34#").url : '/assets/sample-user.png'
  end

  def profile_image_50
    #image_tag automatically prefix /assets/ for you.
    self.profile_image ? self.profile_image.thumb("50x50#").url : '/assets/sample-user.png'
  end

  def followers
    follower = Followship.where(:target_id => self.id).desc(:created_at).limit(15)
    collection = Entity.find(follower.map { |f| f.user_id})
  end

  def following
    following = self.followships.desc(:created_at).limit(15)
    collection = Entity.find(following.map { |f| f.target_id})
  end

  def following_count
    self.followships.count
  end

  def self.find_for_facebook_oauth(access_token)
    User.where(:facebook_id => access_token["uid"]).first
  end

  def follow! user
    if user
      unless self.follow? user
        if self.id != user.id
          user.inc(:follow_count, 1)
          self.followships.create(target_id: user.id)
          return true
        end
      end
    end
    return false
  end

  def unfollow! user
    if user
      if self.follow? user
         user.update_attributes!(follow_count: user.follow_count - 1)
         self.followships.where(target_id: user.id).first.destroy
        return true
      end
    end
    return false
  end

  def follow? user
    self.followships.where(target_id: user.id).first.present?
  end

  def like! post
    if post
      unless self.like? post
        post.inc(:like_count, 1)
        self.likeships.create(target_id: post.id)
        return true
      end
    end
    return false
  end

  def unlike! post
    if post
      if self.like? post
         post.update_attributes!(like_count: post.like_count - 1)
         self.likeships.where(target_id: post.id).first.destroy
        return true
      end
    end
    return false
  end

  def like? post
    self.likeships.where(target_id: post.id).first.present?
  end

  def self.current
    Thread.current[:user]
  end

  def self.current=(user)
    Thread.current[:user] = user
  end

  # this would take a whole thing down at some point. avoid formatting in model, it belongs to view.
  #def self.existing_username_tags
  #  User.all.only(:username).collect{ |ms| "@"+ms.username}.flatten.uniq.compact
  #end
  
  def existing_album_tags
    self.posts.all.only(:album_tag_list).collect{ |ms| ms.album_tag_list.nil? ? nil : ms.album_tag_list.collect {|x| "(#{x})"}}.flatten.uniq.compact
  end
  
  def unread_notification
    self.notifications.unread.count
  end
  
  def reshare(post)
    self.publish_to_facebook(post) if self.auto_publish_facebook?
    self.publish_to_twitter(post) if self.auto_publish_twitter? && self.twitter_authenticated?
    post.inc(:reshare_count, 1)
    
    Timeline.add_activity(self, 'shared', post)
    
    return true if post.user == self
    
    post.user.popularities.create(:subject => self, :value => 2, :event_type => "reshare")
    
    post.notifications.create({
      :user_id => post.user.id, 
      :event_type => 'being_shared', 
      :secondary_subject_type => self.class.to_s, 
      :secondary_subject_id => self.id
    }) if post.user.notification_settings['being_shared'] == '1'
    UserMailer.being_shared(post.user, post, self).deliver if post.user.email_notification_settings['being_shared'] == '1'
    true
  end
  
  def promote!(post)
    post.promotions.create(:user => self)
  end
  
  def publish_to_facebook(post)
    return if self.token.blank?
    me = FbGraph::User.me(self.token)
    begin
      @album ||= me.album! :name => 'imfoodlover.com', :message => 'I\'m food lover'
      # We send a photo to facebook
      @album.photo!(
        :access_token => self.token,
        :source => open(Settings.app_url + post.image_497),
        :message => Sanitize.clean(post.message) )
#       me.feed!(
#         :message => Sanitize.clean(post.message),
#         :picture => Settings.app_url + post.image_497,
#         # NOTE: post.backbone_show_url use backbone url
#         #:link => post.backbone_show_url,
#         :link => Settings.app_url + "/post/#{post.id}",
#         :name => 'imfoodlover.com',
#         :description => 'Food Street Malaysia is created to be an internet-based promotions platform for F&B operators and hoteliers. We aim to be a prominent online source of reference for the best food in Malaysia and around the region.'
#       )
    rescue Exception => e
    end
  end
  
  def twitter_authenticated?
    !self.twitter_oauth_token.blank? && !self.twitter_oauth_secret.blank?
  end
  
  def publish_to_twitter(post)
    return unless self.twitter_authenticated?
    
    @tt_client ||= self.prepare_access_token
#     @tt_client.request(:post, "http://api.twitter.com/1/statuses/update.json", :status => "#{Sanitize.clean(post.message)} #{post.backbone_show_url}")
    @tt_client.request(:post, "http://api.twitter.com/1/statuses/update.json", :status => "#{Sanitize.clean(post.message)} #{Settings.app_url + "/post/#{post.id}"}")
  end

  def feeds
    following = self.followships.desc(:created_at).all
    

    Timeline.new_posts.any_of(
      {:actor_id => {"$in" => following.map{ |f| f.target_id}}, :repinned => false}, {:actor_id => {"$in" => [ self.id ]}}
    )
  end
  
  def activities
    following = self.followships.desc(:created_at).all
    
    Timeline.activities.any_of(
      {:actor_id => {"$in" => following.map{ |f| f.target_id}}}
    )
  end
  
  def subscribed_posts
    Post.tagged_with(self.tag_subscriptions.map(&:name), :on => 'food')
  end
  
  def user_has_granted_offline_perms?
    return false if self.token.blank?
    ret = HTTParty.get('https://graph.facebook.com/' + "#{self.facebook_id}" + '/permissions' + '?access_token=' + "#{self.token}")
    return false if ret.parsed_response['error']
    (ret.parsed_response['data'][0].has_key?('offline_access') && ret.parsed_response['data'][0]['offline_access'] == 1) ? true : false
  end
  
  def admin?
    self.role == 3
  end
  
  def deactivated?
    self.active == false
  end
  
  def active_for_authentication?
    super && !deactivated?
  end
  
  def inactive_message
    !deactivated? ? super : "Account deactivated."
  end

  #TODO this should go to view helper
  def role_in_name
    if self.deactivated?
      'Deactivated'
    else
      case self.role
      when 1
        if self.last_sign_in_at.nil? || self.last_sign_in_at < 1.week.ago
          'Dormant'
        else
          'Active'
        end
      when 2
        'Merchant'
      when 3
        'Admin'
      end
    end
  end
  
  def self.find_for_weibo_oauth(oauth)
    User.where(:weibo_uid => oauth['uid']).first
  end
  
  def self.find_for_tqq_oauth(oauth)
    User.where(:tqq_uid => oauth['extra']['user_hash']['data']['openid']).first
  end
  
  def reward_badge(badge)
    self.badges << badge
  end
  
  def remove_badge(badge)
    self.badges.delete(badge)
  end
  
  def level
    pts_lvl = self.points_count / Settings.leveling_gap.to_i
    pp_lvl = self.popularities_count / Settings.leveling_gap.to_i
    [pts_lvl, pp_lvl].min
  end
  
  protected
  def prepare_access_token
    return nil unless self.twitter_authenticated?
    
    consumer = OAuth::Consumer.new(Settings.twitter_consumer_key, Settings.twitter_consumer_secret, {:site => "http://api.twitter.com"})
    token_hash = {:oauth_token => self.twitter_oauth_token, :oauth_token_secret => self.twitter_oauth_secret}
    access_token = OAuth::AccessToken.from_hash(consumer, token_hash)
    return access_token
  end
  
  def create_suggest_notification_on_freinds
    return if self.token.blank?
    me = FbGraph::User.me(self.token)
    fb_friends = me.friends
    friends = User.any_in(:facebook_id => fb_friends.map(&:identifier))
    friends.each do |friend|
      self.subject_notifications.create({:user_id => friend.id, :event_type => 'friend_joins'}) if friend.notification_settings['friend_joins'] == '1'
      self.notifications.create({:subject => friend, :event_type => 'friend_joins'}) if self.notification_settings['friend_joins'] == '1'
    end
  end  
  
  def reward_user_points
    self.points.create(:subject => self, :value => 10, :event_type => 'sign_up')
  end
  
  def reward_user_credits
    self.credits.create(:subject => self, :value => 10, :event_type => 'sign_up')
  end
  
  def reward_inviter_points
    self.inviter.points.create(:subject => self, :value => 10, :event_type => 'referral')
  end
  
  def reward_inviter_credits
    self.inviter.credits.create(:subject => self, :value => 10, :event_type => 'referral')
  end
end

