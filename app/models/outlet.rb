class Outlet < Entity

  field :name,    type: String
  field :description,    type: String
  field :phone,          type: String
  field :email,          type: String
  field :address1,       type: String
  field :address2,       type: String
  field :state,          type: String
  field :postcode,       type: String
  field :city,           type: String
  field :operation_hour, type: String
  field :reservation,    type: Boolean
  field :take_away,      type: Boolean
  field :smoking_area,   type: Boolean
  field :halal,          type: Boolean
  field :attire,         type: String

  field :like_count,     type: Integer, default: 0
  field :comment_count,  type: Integer, default: 0

  api_accessible :all_outlets do |o|
    o.add :name
    o.add :slug
    o.add :follow
  end

  api_accessible :foodies do |u|
    u.add :profile_image_50
    u.add :username
    u.add :follow
    u.add :slug
  end

  def follow
    User.current.follow? self
  end

  def profile_image_34
    self.profile_image ? self.profile_image.thumb("34x34#").url : 'assets/sample-user.png'
  end

  def profile_image_50
    self.profile_image ? self.profile_image.thumb("50x50#").url : 'assets/sample-user.png'
  end
end
