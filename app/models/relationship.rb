class Relationship
  include Mongoid::Document
  include Mongoid::Timestamps

  field :target_id

  belongs_to :user
end

