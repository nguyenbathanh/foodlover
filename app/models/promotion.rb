class Promotion
  include Mongoid::Document
  include Mongoid::Timestamps
  embedded_in :post
  
  belongs_to :user
  
  validates_presence_of :user_id
  
  after_create :alert_admin
  
  protected
  def alert_admin
    AdminMailer.promoted_post(self).deliver
  end
end