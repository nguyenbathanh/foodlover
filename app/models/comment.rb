class Comment
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActionView::Helpers::DateHelper 
  acts_as_api

  field :message, type: String

  belongs_to :user
  belongs_to :post
  has_many :as_timeline_secondary_subjects, as: :secondary_subject, dependent: :destroy, :class_name => 'Timeline'
  has_many :rewards, as: :subject, dependent: :destroy
  
  before_create :increase_counter_to_post
  before_destroy :decrease_counter_to_post
  after_create :create_notification
  after_create :reward_user_popularity

  api_accessible :created do |u|
    u.add :message
    u.add :image_32
    u.add :author
    u.add :created_at_time
  end
  
  api_accessible :api_created, :extend => :created do |u|
    u.remove :image_32
    u.add :api_image_32, as: :image_32
  end

  def image_32
    self.user.profile_image_24
  end

  def author
    self.user.username
  end

  def created_at_time
    distance_of_time_in_words_to_now(created_at, true) + " ago"
  end
  
  protected
  def increase_counter_to_post
    # copy from previous developer style
    post.inc(:comment_count, 1)
  end
  
  def decrease_counter_to_post
    post.inc(:comment_count, -1)
  end
  
  def api_image_32
    Settings.app_url + image_32
  end
  
  def create_notification
    Timeline.add_activity(self.user, 'commented', self.post, self)
    
    return if post.user == self.user
    self.post.notifications.create({
      :user_id => self.post.user.id, 
      :event_type => 'being_commented',
      :secondary_subject_type => self.class.to_s, 
      :secondary_subject_id => self.id
    }) if post.user.notification_settings['being_commented'] == '1'
    
    UserMailer.being_commented(post.user, post, self).deliver if post.user.email_notification_settings['being_commented'] == '1'
  end
  
  def reward_user_popularity
    return if post.user == self.user
    post.user.popularities.create(:subject => self, :value => 1, :event_type => 'commented')
  end
end
