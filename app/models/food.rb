class Food
  include Mongoid::Document
  include Mongoid::Timestamps
  acts_as_api

  field :name, type: String
  field :description, type: String
  field :external_uri, type: String
  field :popularity_score, type: Integer, default: 0

  index :name

  validates :name, presence: true

  has_many :subscriptions
  attr_accessible :name, :description, :external_uri

  def posts
    Post.where(:food_ids => self.id)
  end

  def self.lookup(query)
    return [] if query.to_s.empty?
    where(name: Regexp.new("^#{query}", Regexp::IGNORECASE))
  end

  def subscriptions_count
    subscriptions.count
  end

  #TODO implement similar foods
  def similar_foods_count
    0
  end

  api_accessible :tags do |u|
    u.add :id
    u.add :name, as: 'label'
  end
end
