class Entity
  include Mongoid::Document
  include Mongoid::Timestamps
  include Mongoid::Slug
  acts_as_api

  field :username,        type: String
  field :follower_count,  type: Integer, default: 0
  field :following_count, type: Integer, default: 0 

  field :profile_image_uid
  image_accessor :profile_image

  slug :username

  api_accessible :followers do |u|
    u.add :slug
    u.add :profile_image_34
  end

  api_accessible :following do |u|
    u.add :slug
    u.add :profile_image_34
  end

  api_accessible :search_username do |u|
    u.add :username
  end
end
