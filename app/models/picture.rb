class Picture
  include Mongoid::Document
  acts_as_api

  field :file_uid, index: true
  #field :file_id, index: true
  #TODO: comment this line on production
  field :file_id, index: true
  image_accessor :file

  has_one :post
  belongs_to :user, index: true

  def medium_sized_url
    self.file.thumb('180x300').url
  end

  api_accessible :default do |u|
    u.add :id
    u.add :medium_sized_url
  end
end