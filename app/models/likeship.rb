class Likeship < Relationship
  has_many :as_timeline_secondary_subjects, as: :secondary_subject, dependent: :destroy, :class_name => 'Timeline'
  has_many :rewards, as: :subject, dependent: :destroy
  
  after_create :create_notification
  after_create :reward_user_popularity
  
  protected
  def create_notification
    post = Post.find(self.target_id)
    Timeline.add_activity(self.user, 'liked', post, self)
    return if post.user == self.user
    post.notifications.create({
      :user_id => post.user.id, 
      :event_type => 'being_liked', 
      :secondary_subject_type => self.user.class.to_s, 
      :secondary_subject_id => self.user.id
    }) if post.user.notification_settings['being_liked'] == '1'
    
    UserMailer.being_liked(post.user, self).deliver if post.user.email_notification_settings['being_liked'] == '1'
  end
  
  def reward_user_popularity
    post = Post.find(self.target_id)
    return if post.user == self.user
    post.user.popularities.create(:subject => self, :value => 1, :event_type => 'liked')
  end
end
