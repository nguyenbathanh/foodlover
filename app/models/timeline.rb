class Timeline
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::SanitizeHelper
  acts_as_api

  #Food tags related

  has_and_belongs_to_many :foods, inverse_of: nil
  before_create :copy_foods_from_post

  def copy_foods_from_post
    return if self.event_type != 'new_post'
    self.food_ids = self.subject.food_ids
  end

  def food_tags
    self.foods.collect(&:name)
  end

  def album_tags
    #if self.subject.albums.length == 0
    #  []
    #else
    #  [self.subject.albums.first.name]
    #end
    self.subject.albums.collect(&:name)
  end

  def friend_tags
    self.subject.shared_users
  end
  
  def location

    if !self.subject.location.nil?
      self.subject.location.name
    else
      ""
    end
    
  end

  field :event_type, type: String
  field :data,       type: Hash
  field :repinned, type: Boolean, default: false

  belongs_to :actor,              polymorphic: true
  belongs_to :subject,            polymorphic: true
  belongs_to :secondary_subject,  polymorphic: true
  
  scope :new_posts, where(:event_type => "new_post")
  scope :activities, where(:event_type => {"$ne" => 'new_post'})

  api_accessible :all_feeds do |u|
    u.add :subject_id, as: :id
    u.add :message
    u.add :repin_message
    u.add :image_200, as: :post_image
    u.add :username, as: :username
    u.add :image_68, as: :profile_image
    u.add :event_type
    u.add :like
    u.add :comment_count
    u.add :like_count
    u.add :friend_tags, template: :show
    u.add :reshare_count
    u.add :repin_count
    u.add :comments, template: :created
    u.add :created_at_time
    u.add :user_id
    u.add :creator_slug
    u.add :flagger_ids
    u.add :promoter_ids
    u.add :album_tags
    u.add :food_tags
    u.add :location
  end
  
  api_accessible :api_all_feeds, :extend => :all_feeds do |u|
    u.remove :message
    u.remove :image_200
    u.remove :event_type
    u.remove :post_image
    u.remove :profile_image
    u.remove :comments
    u.add :api_message, as: :message
    u.add :food_tags
    u.add :album_tags
    u.add :api_image_200, as: :image_200
    u.add :api_image_68, as: :profile_image
    u.add :comments, template: :api_created
    u.add :flagger_ids
    u.add :promoter_ids
  end

  def message
    self.subject.message
  end
  
  def repin_message
    self.subject.repin_message || ''
  end

  def image_68
    self.actor.profile_image_68
  end

  def username
    self.actor.username
  end

  def image_200
    self.subject.image_file.thumb('200x114#').url
  end

  def created_at_time
    distance_of_time_in_words_to_now(created_at, true) + " ago"
  end

  def like_count
    self.subject.like_count
  end

  def comment_count
    self.subject.comment_count
  end
  
  def reshare_count
    self.subject.reshare_count
  end

  def repin_count
    self.subject.repin_count
  end

  def like
    User.current ? (User.current.like? self.subject) : false
  end

  def comments
    self.subject.comments
  end
  
  def user_id
    self.subject.user_id.to_s
  end

  class << self
    def add_post(user, post)
      return if where(actor_type: user.class, actor_id: user.id, subject_type: post.class, subject_id: post.id, event_type: 'new_post').present?

      create(
        event_type: :new_post, actor: user, subject: post, repinned: post.repinned_post?,
        data: {
          user_username: user.username,
          post_image: post.image_file.thumb('200x114#').url,
          post_message: post.message,
        }
      )
    end
    
    def add_activity(actor, event_type, subject, secondary_subject=nil)
      find_or_create_by(
        event_type: event_type, 
        actor_type: actor.class, 
        actor_id: actor.id, 
        subject_type: subject.class, 
        subject_id: subject.id,
        secondary_subject_type: secondary_subject.try(:class),
        secondary_subject_id: secondary_subject.try(:id)
      )
    end
  end
  
  def creator_slug
    self.actor.slug
  end
  
  def api_image_200
    Settings.app_url + image_200
  end
  
  def api_message
    strip_links(self.subject.message)
  end
  
  def api_image_68
    Settings.app_url + image_68
  end
  
  protected

  def flagger_ids
    self.subject.flags.map(&:user_id)
  end
  
  def promoter_ids
    self.subject.promotions.map(&:user_id)
  end
end
