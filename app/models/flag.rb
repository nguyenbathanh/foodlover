class Flag
  include Mongoid::Document
  include Mongoid::Timestamps
  field :reason, type: String
  field :description, type: String
  
  belongs_to :post
  belongs_to :user
  
  validates_presence_of :reason, :user_id
  
  REASON = ['Spam', 'Incorrect Content']
  
  after_create :unfollow_spammer, :if => Proc.new {|f| f.spammy?}
  after_create :alert_admin, :unless => Proc.new {|f| f.spammy?}
  
  def spammy?
    self.reason == 'Spam'
  end
  
  protected
  def unfollow_spammer
    self.user.unfollow!(self.post.user) if self.user.follow?(self.post.user)
  end
  
  def alert_admin
    AdminMailer.flagged_post(self).deliver
  end
end