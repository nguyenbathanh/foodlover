class Tag
  include Mongoid::Document
  include Mongoid::Timestamps
  acts_as_api

  field :name, type: String
  field :context, type: String

  api_accessible :search_tag do |u|
    u.add :name
    u.add :context
  end
end
