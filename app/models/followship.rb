class Followship < Relationship
  has_many :as_timeline_secondary_subjects, as: :secondary_subject, dependent: :destroy, :class_name => 'Timeline'
  has_many :rewards, as: :subject, dependent: :destroy
  
  after_create :create_notification
  after_create :reward_user_popularity
  
  protected
  def create_notification
    user = User.find self.target_id
    self.user.subject_notifications.create({
      :user_id => self.target_id, 
      :event_type => 'being_followed'
    }) if user.notification_settings['being_followed'] == '1'
    
    UserMailer.being_followed(user, self.user).deliver if user.email_notification_settings['being_followed'] == '1'
    
    Timeline.add_activity(self.user, 'followed', user, self)
  end
  
  def reward_user_popularity
    user = User.find self.target_id
    user.popularities.create(:subject => self, :value => 1, :event_type => 'followed')
  end
end

