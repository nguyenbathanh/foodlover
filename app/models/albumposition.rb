class Albumposition
  include Mongoid::Document
  include Mongoid::Timestamps
  acts_as_api

  field :album, type: String
  field :user_id, type: String
  field :position, type: Integer


  api_accessible :default do |u|
    u.add :album_id
    u.add :user_id
    u.add :position
  end

  belongs_to :album
  belongs_to :user
end