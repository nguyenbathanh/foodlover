class TagSubscription
  include Mongoid::Document
  field :name, type: String
  embedded_in :user
  
  has_many :as_timeline_secondary_subjects, as: :secondary_subject, dependent: :destroy, :class_name => 'Timeline'
  has_many :timelines, as: :subject, dependent: :destroy
  
  after_create :create_notification
  
  acts_as_api
  
  api_accessible :tag_subscriptions do |u|
    u.add :id
    u.add :name
    u.add :user_id, as: :user_id
  end
  
  protected
  def user_id
    self.user.id.to_s
  end
  
  def create_notification
    Timeline.add_activity(self.user, 'subscribed', self)
  end
end