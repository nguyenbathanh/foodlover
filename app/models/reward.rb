class Reward
  include Mongoid::Document
  include Mongoid::Timestamps
  
  belongs_to :subject, polymorphic: true
  belongs_to :user
  
  field :value, type: Integer
  field :event_type, type: String
  
  validates_presence_of :user_id, :value, :event_type
  
  after_create :inc_user_counter
  before_destroy :pop_user_counter
  
  def inc_user_counter
    self.user.inc("#{self._type.pluralize.downcase}_count", self.value)
  end

  def pop_user_counter
    self.user.inc("#{self._type.pluralize.downcase}_count", -self.value)
  end
end
