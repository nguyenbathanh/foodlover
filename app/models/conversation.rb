class Conversation
  include Mongoid::Document
  include Mongoid::Search
  include Mongoid::Timestamps
  include Mongoid::Slug
  
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::TextHelper
  include ActionView::Helpers::TagHelper
  acts_as_api

  field :participant_a
  field :participant_b

  field :subject,               :type => String
  field :last_message,          :type => String
  field :count,                 :type => Integer
  field :participant_a_read,    :type => Boolean, :default => false
  field :participant_b_read,    :type => Boolean, :default => false
  field :participant_a_delete,  :type => Boolean, :default => false
  field :participant_b_delete,  :type => Boolean, :default => false

  has_many :messages, dependent: :delete

  search_in :subject, :last_message, :messages => :body_simple
  
  slug :subject
  

  api_accessible :all_conversations do |u|
    u.add :id
    u.add :subject
    u.add :last_message_simple
    u.add :image_68
    u.add :username
    u.add :updated_time
    u.add :slug
    u.add :read_status
  end

  api_accessible :created do |u|
    u.add :id
    u.add :subject
    u.add :last_message_simple
    u.add :image_68
    u.add :username
    u.add :updated_time
    u.add :slug
    u.add :read_status
  end

  def last_message_simple
    simple_format self.last_message
  end

  def read_status
    if self.participant_b != User.current.id
      return self.participant_a_read
    else
      return self.participant_b_read
    end
  end

  def updated_time
    distance_of_time_in_words_to_now(updated_at, true) + " ago"
  end

  def image_68
    if participant_b != User.current.id
      user = User.find(participant_b)
      return user.profile_image_68
    else
      user = User.find(participant_a)
      return user.profile_image_68
    end
  end

  def username
    if participant_b != User.current.id
      user = User.find(participant_b)
      return user.username
    else
      user = User.find(participant_a)
      return user.username
    end
  end

  scope :list_for, lambda { |entity| any_of({ :participant_a => entity.id }, { :participant_b => entity.id })}
  scope :author_active, where(:participant_a_delete => false)


  def check_sender_status user
    if self.participant_b != user.id
      return self.participant_b
    else
      return self.participant_a
    end
  end

  def mark_read! user
    if self.participant_b != user.id
      self.update_attributes!(participant_a_read: true)
      return
    else
      self.update_attributes!(participant_b_read: true)
      return
    end
  end

  class << self
    def first_message user, params={}
      entity = Entity.where(username: params[:recipient]).first

      if entity
        conversation = create(last_message: params[:body], participant_a: user.id,
                              participant_b: entity.id, subject: params[:subject],
                              count: 1, participant_a_read: true)
        message = conversation.messages.create(body: params[:body], author_id: user.id)
        return message
      else
        return nil
      end
    end

    def append_message user, params={}
      conversation = find_by_slug(params[:conversation_id])
      if (conversation.participant_b == user.id || conversation.participant_a == user.id)
        conversation.update_attributes!(last_message: params[:body], count: conversation.count + 1)
        message = conversation.messages.create!(body: params[:body], author_id: user.id)
        return message
      end
    end
  end
end
