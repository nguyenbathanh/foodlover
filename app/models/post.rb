class Post
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActionView::Helpers::DateHelper
  acts_as_api

  #TODO remove this and replace references in code
  include Mongoid::Document::TaggableOn
  cw_taggable_on :food, :album, :global_user

  attr_accessor :post_to_facebook, :post_to_twitter, :album_tag_list

  field :food_names, type: String
  field :message, type: String

  field :image_uid
  image_accessor :image


  belongs_to :user, index: true
  belongs_to :picture, index: true
  belongs_to :location, index: true
#   belongs_to :album, index: true
  has_and_belongs_to_many :albums
  # When you create a post (via pop-up) you can create one post shared with annothers users
  # One side Many-to-Many relationship
  has_and_belongs_to_many :shared_users, :class_name => 'User', inverse_of: nil
  has_and_belongs_to_many :foods, inverse_of: nil, dependent: :nullify

  validates_presence_of :food_names, :picture_id, :user_id
  attr_accessible :picture_id, :food_names, :user_names, :message, :album_tag_list

  before_save :parse_food_names, if: lambda { |p| p.food_names_changed? }

  def parse_food_names
    return if self.food_names.to_s.empty?
    self.foods.clear
    self.food_names.split(",").uniq.each do |name|
      self.foods.push Food.where(name: Regexp.new("^#{name}$", Regexp::IGNORECASE)).first || Food.create!(name: name)
    end
  end

  api_accessible :default do |u|
    u.add :id
    u.add :food_names
    u.add :food_ids
  end

  ###

  field :repin_message, type: String, :default => ''

  field :like_count,     type: Integer, default: 0
  field :comment_count,  type: Integer, default: 0
  field :reshare_count,  type: Integer, default: 0
  field :repin_count,    type: Integer, default: 0

  belongs_to :repinned_post, :class_name => 'Post', index: true
  has_many :comments
  has_many :timelines, as: :subject, dependent: :destroy
  has_many :notifications, as: :subject, dependent: :destroy
  has_many :flags
  has_many :as_timeline_secondary_subjects, as: :secondary_subject, dependent: :destroy, :class_name => 'Timeline'
  has_many :rewards, as: :subject, dependent: :destroy
  embeds_many :promotions

  #before_save :check_badges_availability, :if => Proc.new {|x| x.food_tag_list_changed?}
  #after_save :notify_tagged_global_user, :if => :has_global_user_tagged?
  #after_create :post_to_creator_socials
  before_create :generate_repinned_message, :unless => Proc.new {|p| p.repinned_post.nil?}
  after_create :increase_repin_counter, :unless => Proc.new {|p| p.repinned_post.nil?}
  after_create :create_notification
  after_create :reward_user_point
  after_create :reward_user_credit
  after_create :reward_user_popularity, :unless => Proc.new {|p| p.repinned_post.nil?}
  after_create :add_to_timeline
  before_update :update_timeline, :if => Proc.new{|p| p.food_ids_changed?}

  def first_shared_users
    self.shared_users[0..2] || []
  end

  def extra_shared_users
    self.shared_users[3..self.shared_users.size] || []
  end

  def extra_shared_users_count
    return self.shared_users.size - 3 if self.shared_users.size > 3
    0
  end

  def notify_tagged_global_user
    global_users.each do |u|
      if user = User.find_by_slug(u) || User.where(:username => u).first
        Timeline.add_activity(self.user, 'tagged_user', user, self) unless self.as_timeline_secondary_subjects.where("event_type" => 'tagged_user').exists?
        return if self.user == user
        return if self.notifications.where(user_id: user.id).present?
        self.notifications.create({:user_id => user.id, :event_type => 'being_tagged'}) if user.notification_settings['being_tagged'] == '1'
        UserMailer.being_tagged(user, self).deliver if user.email_notification_settings['being_tagged'] == '1'
      end
    end
  end

  def add_to_timeline
    Timeline.add_post(self.user, self)
  end

  def update_timeline
    self.timelines.each do |t|
      t.food_ids = self.food_ids
      t.save!
    end
  end

  def post_to_creator_socials
    if repinned_post?
      self.user.publish_to_facebook(self) if self.post_to_facebook == '1'
      self.user.publish_to_twitter(self) if self.post_to_twitter == '1'
    else
      self.user.reshare(self)
    end
  end

  #TODO make working with updated tagging
  def check_badges_availability
    tag_changes = self.food_tag_list_change.flatten.uniq - ((self.food_tag_list_change.first || []) & (self.food_tag_list_change.last || []))
    removed_tags = tag_changes - (self.food_tag_list_change.last || [])
    added_tags = tag_changes - (self.food_tag_list_change.first || [])
    self.check_badged_availability(removed_tags) unless removed_tags.blank?
    self.check_badging_availability(added_tags) unless added_tags.blank?
    # self.delay.reward_badges(tag_changes) # IT IS AS SIMPLE AS THIS IF DJ ENABLED
  end

  # def reward_badges(tags)
  #   badges = Badge.any_in(:name => tags)
  #   badges.each do |b|
  #     if b.qualified?(self.user)
  #       self.user.reward_badge(b)
  #     else
  #       self.user.remove_badge(b)
  #     end
  #   end if badges.exists?
  # end

  def check_badged_availability(tags)
    badges = Badge.any_in(:name => tags)
    badges.each do |b|
      self.user.remove_badge(b) unless b.qualified?(self.user, 1)
    end if badges.exists?
  end

  def check_badging_availability(tags)
    badges = Badge.any_in(:name => tags)
    badges.each do |b|
      self.user.reward_badge(b) if b.qualified?(self.user, -1)
    end if badges.exists?
  end

  api_accessible :all_posts do |u|
    u.add :id
    u.add :message
    u.add :repin_message
    u.add :image_200, as: :post_image
    u.add :created_at_time
    u.add :username
    u.add :image_68, as: :profile_image
    u.add :like_count
    u.add :reshare_count
    u.add :repin_count
    u.add :comment_count
    u.add :like
    u.add :comments, template: :created
    u.add :creator_slug
    u.add :flagger_ids
    u.add :promoter_ids
  end

  api_accessible :post do |u|
    u.add :id
    u.add :image_497, as: :post_image
    u.add :username
    u.add :created_at_time
    u.add :message
    u.add :repin_message
    u.add :created_at_time
    u.add :image_68, as: :profile_image
    u.add :like
    u.add :comments, template: :created
    u.add :like_count
    u.add :comment_count
    u.add :reshare_count
    u.add :repin_count
    u.add :creator_slug
    u.add :user_id
    u.add :flagger_ids
    u.add :promoter_ids
  end
  
  api_accessible :post_feed do |u|
    u.add :id
    u.add :image_200, as: :post_image
    u.add :username
    u.add :created_at_time
    u.add :message
    u.add :repin_message
    u.add :created_at_time
    u.add :image_68, as: :profile_image
    u.add :like
    u.add :comments, template: :created
    u.add :like_count
    u.add :comment_count
    u.add :reshare_count
    u.add :repin_count
    u.add :all_taggings
    u.add :creator_slug
    u.add :flagger_ids
    u.add :promoter_ids
  end

  api_accessible :photos do |u|
    u.add :image_130
    u.add :id
    u.add :username, as: :owner_name
  end

  delegate :username, :to => :user, :allow_nil => true
  alias_method :owner_name, :username

  def medium_image
    self.picture.medium_sized_url
  end

  #for album small thumbs :almazom
  def image_58
    self.image_file.thumb('58x58#').url
  end

  def image_130
    self.image_file.thumb('130x130#').url
  end

  def image_497
    self.image_file.thumb('497x331').url
  end

  def image_200
    self.image_file.thumb('360x300#').url
  end

  def image_68
    self.user.profile_image_68
  end

  def image_file
    self.picture ? self.picture.file : self.image
  end

  def like
    User.current ? (User.current.like? self) : false
  end

  def created_at_time
    distance_of_time_in_words_to_now(created_at, true) + " ago"
  end

  def backbone_show_url
    "#{Rails.application.routes.url_helpers.dashboard_show_url(:host => Settings.app_host)}/post/#{self.id.to_s}"
  end

  def repinned_post?
    !self.repinned_post.blank?
  end

  def repinned_via_message
    return '' if not repinned_post?
    # If the current post is a repinned post, we show the correct information:
    #   User from: self.repinned_post.user.slug
    #   Album from: not exist
#     "Repinned via <span class='user'><a href='/users/#{(self.repinned_post.user.id)}'>#{self.repinned_post.user.slug}</a></span> from <span class='album'><a href='/#{self.repinned_post.user.slug}/albums/#{self.repinned_post.album_tag_list.try(:first)}' >#{self.repinned_post.album_tag_list.try(:first)}</a></span>"
    "Repinned via <span class='user'><a href='/#{(self.repinned_post.user.slug)}'>#{self.repinned_post.user.slug}</a></span>"
  end
  
  protected

  def creator_slug
    self.user.slug
  end

  def generate_repinned_message
    # NOTE: was Pinned from @demo3 from
#     html = "Repinned via <span class='user'><a href='/users/#{(self.repinned_post.user.id)}'>#{self.repinned_post.user.slug}</a></span> from <span class='album'><a href='/#{self.repinned_post.user.slug}/albums/#{self.repinned_post.album_tag_list.try(:first)}' >#{self.repinned_post.album_tag_list.try(:first)}</a></span>"
    html = "Repinned via <span class='user'><a href='/#{(self.repinned_post.user.slug)}'>#{self.repinned_post.user.slug}</a></span>"
    #html += " #{self.repinned_post.album_tag_list.try(:join, ', @')}" if self.repinned_post.album_tag_list.present?
    self.repin_message = html
  end
  
  def increase_repin_counter
    self.repinned_post.inc(:repin_count, 1)
  end
  
  def flagger_ids
    self.flags.map(&:user_id)
  end
  
  def promoter_ids
    self.promotions.map(&:user_id)
  end

  def create_notification
    return unless self.repinned_post?
    return if repinned_post.user == self.user
    Timeline.add_activity(self.user, 'pinned', self)
    
    self.notifications.create({
      :user_id => repinned_post.user.id, 
      :event_type => 'being_repinned', 
      :secondary_subject_type => self.user.class.to_s, 
      :secondary_subject_id => self.user.id
    }) if repinned_post.user.notification_settings['being_repinned'] == '1'
    
    UserMailer.being_repinned(repinned_post.user, repinned_post, self.user).deliver if repinned_post.user.email_notification_settings['being_repinned'] == '1'
  end
  
  def reward_user_point
    user.points.create(:subject => self, :value => 5, :event_type => 'new_post')
  end
  
  def reward_user_credit
    user.credits.create(:subject => self, :value => 5, :event_type => 'new_post')
  end
  
  def reward_user_popularity
    repinned_post.user.popularities.create(:subject => self, :value => 2, :event_type => 'repinned')
  end

#   before_save :save_album_tag_list, :if => Proc.new{|p| p.album_tag_list_changed?}
# 
#   def save_album_tag_list
#     
#   end

  #Below are candidates to removal

  #before_save :process_global_user_tags, :if => Proc.new{|p| p.message_changed?}
  #before_save :process_album_tags, :if => Proc.new{|p| p.message_changed?}

  #before_save :process_food_tags, :if => Proc.new{|p| p.message_changed?}
  #
  #def has_food_tagged?
  #  @has_food_tagged ||= !message.scan(/{[a-zA-Z0-9_ ]*}/).blank?
  #end
  #
  #def process_food_tags
  #  # TODO: wanted to move to DJ, but client wants to save cost until he thinks it is very slow
  #  if has_food_tagged?
  #    tags = message.scan(/{[a-zA-Z0-9_ ]*}/)
  #    tags.each do |t|
  #      slug = t.delete('{}')
  #      message.gsub!(t, "<a href='#foods/#{slug.parameterize}'>#{t}</a>")
  #    end
  #    tags.collect{|x| x.delete!('{}')}
  #    self.food_list = tags
  #
  #    Timeline.add_activity(self.user, 'tagged_food', self) unless self.as_timeline_secondary_subjects.where("event_type" => 'tagged_food').exists?
  #  else
  #    self.food_tag_list = nil
  #  end
  #end

  #def has_global_user_tagged?
  #  @has_global_user_tagged ||= !message.scan(/@\w+/).blank?
  #end
  #
  #def process_global_user_tags
  #  if has_global_user_tagged?
  #    tags = message.scan(/@\w+/)
  #    tags.each do |t|
  #      slug = t.delete("@")
  #      message.gsub!(t, "<a href='#users/#{slug}'>#{t}</a>")
  #    end
  #    tags.collect{|x| x.delete!("@")}
  #    self.global_user_list = tags
  #  else
  #    self.global_user_tag_list = nil
  #  end
  #end

  #def has_album_tagged?
  #  @has_album_tagged ||= !message.scan(/\([a-zA-Z0-9_ ]*\)/).blank?
  #end
  #
  #def process_album_tags
  #  if has_album_tagged?
  #    tags = message.scan(/\([a-zA-Z0-9_ ]*\)/)
  #    album_tags = []
  #    tags.each do |t|
  #      slug = t.delete("()")
  #      album_tags.push("<a href='##{user.slug}/albums/#{slug.parameterize}'>#{t}</a>")
  #      message.gsub!(t, '')
  #    end
  #    self.message += " <i>grouped under #{album_tags.join(' ')}</i>"
  #    tags.collect{|x| x.delete!("()")}
  #    self.album_list = tags
  #  else
  #    self.album_tag_list = nil
  #  end
  #end
end
