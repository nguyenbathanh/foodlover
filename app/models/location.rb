class Location
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String

  index :name

  has_many :posts, dependent: :nullify

  validates :name, presence: true, uniqueness: true

  attr_accessible :name

  acts_as_api

  api_accessible :default do |u|
    u.add :id
    u.add :name
  end
  
end