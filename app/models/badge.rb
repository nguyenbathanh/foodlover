class Badge
  include Mongoid::Document
  include Mongoid::Timestamps
  
  field :icon_image_uid
  field :name, type: String
  field :min_count, type: Integer
  
  image_accessor :icon_image
  
  validates_presence_of :name, :icon_image, :min_count
  validates :min_count, :numericality => {:greater_than => 0}

  #this will not scale. in case of many users Badge instances will not be able to store all ids. solution:
  #TODO make association one-sided with inverse: nil at User side
  has_and_belongs_to_many :users
  
  def qualified?(user, adjustment=0)
    user.posts.only(:food_tag_list).tagged_with(self.name, :on => 'food').count >= (self.min_count + adjustment)
  end
end
