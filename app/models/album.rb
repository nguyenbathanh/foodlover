class Album
  include Mongoid::Document
  include Mongoid::Timestamps

  field :name, type: String

  index :name

  belongs_to :user, index: true
  has_and_belongs_to_many :posts, dependent: :nullify

  validates :name, presence: true
  validates :user_id, presence: true

  attr_accessible :name, :user_id, :user

  before_create :validate_name

  protected
  def validate_name
    if Album.where(:name => self.name, :user_id => self.user_id).all.count > 0
      self.errors.add :name, "already exist"
      return false
    end
    true
  end
end