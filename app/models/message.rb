class Message
  include Mongoid::Document
  include Mongoid::Timestamps
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::TextHelper
  include ActionView::Helpers::TagHelper
  acts_as_api

  field :body,      :type => String
  field :author_id

  api_accessible :all_messages do |u|
    u.add :subject_simple
    u.add :body_simple
    u.add :username
    u.add :created_time
    u.add :image_50
    u.add :id
  end

  api_accessible :created do |u|
    u.add :subject_simple
    u.add :body_simple
    u.add :username
    u.add :created_time
    u.add :image_50
    u.add :id
  end

  def body_simple
    simple_format self.body
  end

	def subject_simple
		self.conversation.subject
	end

  def image_50
    user = User.find(author_id)
    user.profile_image_50    
  end

  def username
    user = User.find(author_id)
    user.username
  end

  def created_time
    distance_of_time_in_words_to_now(created_at, true) + " ago"
  end

  belongs_to :conversation
end
