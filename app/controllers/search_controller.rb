class SearchController < ApplicationController
  def tags
    tags = Tag.all
    render_for_api :search_tag, json: tags, status: 200
  end
end
