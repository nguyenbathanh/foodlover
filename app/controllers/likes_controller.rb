class LikesController < ApplicationController
  before_filter :authenticate_user!

  def create
    post = Post.find(params[:id])
    if current_user.like! post
      head :created
    else
      head :bad_requests
    end
  end

  def destroy
    post = Post.find(params[:id])
    if current_user.unlike! post
      head :ok
    else
      head :bad_requests
    end
  end
end
