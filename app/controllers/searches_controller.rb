class SearchesController < ApplicationController
  def username
    entity = Entity.all
    render_for_api :search_username, json: entity, status: 200
  end
end
