class InvitesController < ApplicationController
  before_filter :authenticate_user!
  layout 'foodpedia'
  
  def provider
    if params[:provider] == 'facebook'
      render :template => '/invites/facebook'
    elsif params[:provider] == 'gmail'
      render :template => '/invites/gmail'
    # needs imfoodlover.my to work as for the domain restriction in hotmail and yahoo
    elsif params[:provider] == 'yahoo'
      render :template => '/invites/yahoo'
    elsif params[:provider] == 'hotmail'
      render :template => '/invites/hotmail'
    else
      render :template => '/invites/email'
    end
  end
  
  def loads
    users = []
    if params[:provider] == 'facebook'
      me = FbGraph::User.me(current_user.token)
      begin
        fb_friends = [me.friends.new, me.friends.new]
        fb_friends.each do |fb|
          user = User.where(:facebook_id => fb.identifier).first
          users.push({
            :fb_id => fb.identifier,
            :name => fb.name || '',
            :id => user.try(:id) || '',
            :slug => user.try(:slug) || '',
            :following => (user.nil? ? false : current_user.follow?(user))
          })
        end
        render :json => users
      rescue
        render :json => {:require_authentication => true}
      end
    else
      contacts = current_user.send(params[:provider].pluralize)
      contacts.each do |c|
        user = User.where(:email => c.email).first
        users.push({
          :email => c.email,
          :name => c.name || '',
          :id => user.try(:id) || '',
          :slug => user.try(:slug) || '',
          :following => (user.nil? ? false : current_user.follow?(user))
        })
      end
      render :json => users
    end
  end
  
  def email_form
    @email = params[:email]
    render :layout => false ,:template => "/invites/email_form"
  end
end
