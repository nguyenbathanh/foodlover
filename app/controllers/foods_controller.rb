class FoodsController < ApplicationController
  inherit_resources

  layout "foodpedia"
  
  def show
    if params[:id] =~ /[{}\(\)\^$&._%#!@=<>:;,~\`\'\"\s\t\*\?\/\+\|\[\\\]]/
      # If the url is like "coca_cola", "coca cola" or "coca$cola·$$%&&&" then, we redirect to "coca-cola"
      redirect_to food_path(params[:id].gsub(/[{}\(\)\^$&._%#!@=<>:;,~\`\'\"\s\t\*\?\/\+\|\[\\\]\-]/, ' ').strip.gsub(/[\s]+/, '-'))
    elsif params[:id] =~ /[\-]{2,}/
      redirect_to food_path(params[:id].gsub(/[\-]{2,}/, '-'))
    elsif resource.nil?
      render :text => "Food not found"
    else
      show!
    end
  end

  def index
    index! { |success| success.json { render_for_api :tags, json: collection } }
  end

  protected

  def collection
    Food.lookup(params[:term].gsub(/[{}\(\)\^$&._%#!@=<>:;,~\`\'\"\s\t\*\?\/\+\|\[\\\]]/, ' ').strip)
  end

  def resource
    end_of_association_chain.where(name: Regexp.new("^#{params[:id].to_s.gsub(/[{}\(\)\^$&._%#!@=<>:;,~\`\'\"\s\t\*\?\/\+\|\[\\\]]/, ' ').strip}", Regexp::IGNORECASE)).first #where(name: params[:id].gsub("_", " ")).first
  end
end