class OutletsController < ApplicationController
  before_filter :set_current_user

  def index
    outlets = Outlet.all
    render_for_api :all_outlets, json: outlets, status: 200
  end

  def featured
    outlets = Outlet.all
    render_for_api :all_outlets, json: outlets, status: 200
  end

  def promotion
    outlets = Outlet.all
    render_for_api :all_outlets, json: outlets, status: 200
  end
end
