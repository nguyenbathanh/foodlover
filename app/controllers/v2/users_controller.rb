class V2::UsersController < V2::ApplicationController
  before_filter :find_user
  
  def show
    @badges = @user.badges
  end
  
  def invitation
    cookies['invited_by'] = { :value => @user.id, :expires => 5.years.from_now }
    redirect_to root_url
  end

  def activities
  end
  
  protected
  def find_user
    @user = User.find_by_slug(params[:slug]) || User.where(:username => params[:slug]).first
  end
end