class V2::ActivitiesController < V2::ApplicationController
  before_filter :authenticate_user!
  
  def index
    @activities = current_user.activities.desc('created_at').page(params[:page]).per(10)
  end
end
