class V2::HomeController < V2::ApplicationController
  def show
    if user_signed_in?
      render
    else
      render 'landing'
    end
  end
end
