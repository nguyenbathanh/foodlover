class V2::FeedsController < V2::ApplicationController
  before_filter :authenticate_user!
  
  def index
    feeds = if !params[:last_post_id].blank?
      current_user.feeds.desc(:created_at).where(:_id.lt => params[:last_post_id]).limit(10)
    else
      current_user.feeds.desc(:created_at).limit(10)
    end
    
    render_for_api :all_feeds, json: feeds, status: 200
  end
end
