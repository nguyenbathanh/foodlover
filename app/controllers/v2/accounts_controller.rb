class V2::AccountsController < V2::ApplicationController
  before_filter :authenticate_user!
  
  def show
    render
  end
  
  def update
    params[:user].delete(:password) if params[:user][:password].blank? # ignore password if blank
    current_user.attributes = params[:user]
    if current_user.save
      sign_in current_user, :bypass => true
      redirect_to v2_account_url
    else
      render :action => :edit
    end
  end
  
  def revoke_twitter
    current_user.update_attributes({:twitter_oauth_token => nil, :twitter_oauth_secret => nil})
    redirect_to v2_account_url
  end
end
