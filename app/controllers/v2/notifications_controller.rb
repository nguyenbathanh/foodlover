class V2::NotificationsController < V2::ApplicationController
  before_filter :authenticate_user!, except: :list
  after_filter :mark_as_read, only: :list

  def index
    @notifications = current_user.notifications.unread.desc('created_at').page(params[:page]).per(10)
  end


  def list
    @notifications = current_user.notifications.unread
    feeds = Post.any_in(_id: @notifications.map(&:post_id)).all
    render_for_api :all_posts, json: feeds, status: 200
  end

  protected

  def mark_as_read
    @notifications.update_all(:read => true)
  end
end
