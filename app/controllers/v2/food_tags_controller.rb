class V2::FoodTagsController < V2::ApplicationController
  before_filter :authenticate_user!
  
  def index
    @posts = current_user.subscribed_posts.desc('created_at').page(params[:page]).per(10)
  end
end
