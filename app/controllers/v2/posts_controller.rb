class V2::PostsController < V2::ApplicationController
  def new
    render :layout => false
  end
  
  def tagged_with
    @posts = Post.tagged_with(params[:tag].gsub('-', ' '), :on => 'food').desc('created_at').page(params[:page]).per(10)
  end
end
