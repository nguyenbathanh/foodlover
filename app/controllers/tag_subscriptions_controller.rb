class TagSubscriptionsController < ApplicationController
  before_filter :authenticate_user!
    
  def create
    subscription = current_user.tag_subscriptions.create(name: params[:tag_subscription][:name])
    render_for_api :tag_subscriptions, json: subscription
  end
  
  def destroy
    subscription = current_user.tag_subscriptions.find(params[:id])
    subscription.destroy
    render :json => subscription
  end
  
  def update
    subscription = current_user.tag_subscriptions.find(params[:id])
    subscription.update_attribute(:name, params[:tag_subscription][:name])
    render_for_api :tag_subscriptions, json: subscription
  end
end
