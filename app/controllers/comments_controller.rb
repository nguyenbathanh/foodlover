class CommentsController < ApplicationController
  before_filter :authenticate_user!

  def index
    post = Post.find(params[:id])
    render_for_api :created, json: post.comments, status: 201
  end

  def create
    post = Post.find(params[:id])
    comment = post.comments.new(message: params[:message], user_id: current_user.id)
    if comment.save!
      render_for_api :created, json: comment, status: 201
    end
  end

  def destroy
  end
end

