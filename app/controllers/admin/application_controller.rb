class Admin::ApplicationController < ApplicationController
  before_filter :authenticate_user!
  layout 'basic'
  
  load_and_authorize_resource
end
