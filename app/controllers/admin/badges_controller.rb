class Admin::BadgesController < Admin::ApplicationController
  before_filter :find_badge, :only => [:edit, :update, :destroy]
  def index
    @badges = Badge.page(params[:page])
  end
  
  def new
    @badge = Badge.new
  end
  
  def create
    @badge = Badge.new(params[:badge])
    if @badge.save
      redirect_to admin_badges_url, notice: "Successful."
    else
      render :action => :new
    end
  end
  
  def edit
    render
  end
  
  def update
    @badge.attributes = params[:badge]
    if @badge.save
      redirect_to admin_badges_url, notice: "Successful."
    else
      render :action => :edit
    end
  end
  
  def destroy
    if @badge.destroy
      flash[:error] = @badge.errors.full_messages.join(", ")
    else
      flash[:notice] = "Successful."
    end
    redirect_to admin_badges_url
  end
  
  protected
  def find_badge
    @badge = Badge.find(params[:id])
  end
end
