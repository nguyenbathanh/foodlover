class Admin::UsersController < Admin::ApplicationController
  helper_method :sort_direction
  
  def index
    @users = if (sort_direction == 'desc')
      User.where({:_id => {"$ne" => current_user.id}}).desc(params[:sort] || 'username').page(params[:page])
    else
      User.where({:_id => {"$ne" => current_user.id}}).asc(params[:sort] || 'username').page(params[:page])
    end
  end
  
  def manage_status
    if params[:commit] == 'Deactivate'
      if params[:users]['select-all'] == "1"
        User.where({:_id => {"$ne" => current_user.id}}).update_all(active: false)
        users = User.where({:_id => {"$ne" => current_user.id}})
      else
        User.any_in(:_id => params[:users][:ids]).update_all(active: false)
        users = User.any_in(:_id => params[:users][:ids])
      end
      users.each do |u|
        u.credits.create(:subject => u, :value => -10, :event_type => 'deactivated')
        u.points.create(:subject => u, :value => -10, :event_type => 'deactivated')
        if u.inviter
          u.inviter.credits.create(:subject => u, :value => -2, :event_type => 'deactivated')
          u.inviter.points.create(:subject => u, :value => -10, :event_type => 'deactivated')
        end
      end
      msg = "User deactivated."
    elsif params[:commit] == 'Activate'
      if params[:users]['select-all'] == "1"
        User.where({:_id => {"$ne" => current_user.id}}).update_all(active: true)
      else
        User.any_in(:_id => params[:users][:ids]).update_all(active: true)
      end
      msg = "User activated."
    elsif params[:commit] == 'Send Mail'
      if !params[:users][:title].present? && !params[:users][:note].present?
        msg = "Please enter title and note."
      else
        if params[:users]['select-all'] == "1"
          users = User.where({:_id => {"$ne" => current_user.id}})
          users.each {|u| UserMailer.custom(u, params[:users][:title], params[:users][:note]).deliver}
          msg = "Mail sent to user."
        elsif params[:users][:ids]
          users = User.any_in(:_id => params[:users][:ids])
          users.each {|u| UserMailer.custom(u, params[:users][:title], params[:users][:note]).deliver}
          msg = "Mail sent to user."
        else
          msg = "Please at least select one."
        end
      end
    end
    redirect_to :back, notice: msg
  end
  
  private
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
