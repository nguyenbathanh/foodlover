class Admin::PostsController < Admin::ApplicationController
  helper_method :sort_direction
  
  def index
    @flags = if (sort_direction == 'desc')
      Flag.desc(params[:sort] || 'id').page(params[:page])
    else
      Flag.asc(params[:sort] || 'id').page(params[:page])
    end
  end
  
  private
  def sort_direction
    %w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"
  end
end
