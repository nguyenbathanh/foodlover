class Ios::Api::PostsController < Ios::Api::ApplicationController
  before_filter :find_post, :only => [:destroy, :update, :reshare, :repin]
  
  def create
    post = current_user.posts.new(params[:post])
    if post.save
      timeline = Timeline.add_post(current_user, post)
      render_for_api :api_all_feeds, :json => timeline, :meta => {:error => false}, :root => :post
    else
      render :json => {:error => post.errors.full_messages.join(", ")}
    end
  end
  
  def destroy
    if @post.destroy
      render :json => {:error => false}
    else
      render :json => {:error => @post.errors.full_messages.join(", ")}
    end
  end
  
  def update
    if params[:post].blank?
      render :json => {:error => 'Missing post new attributes'}
    elsif @post.update_attributes(params[:post])
      render :json => {:error => false}
    else
      render :json => {:error => @post.errors.full_messages.join(", ")}
    end
  end
  
  def reshare
    current_user.reshare(@post)
    render :json => {:error => false}
  end
  
  def repin
    album_tag = params[:post].delete(:album_tag_list)
    @repinned_post = current_user.posts.new({:message => params[:post][:message], :post_to_facebook => params[:post][:post_to_facebook], :post_to_twitter => params[:post][:post_to_twitter]})
    @repinned_post.message += " (#{album_tag})" if !album_tag.blank?
    @repinned_post.image = @post.image
    @repinned_post.repinned_post = @post
    if @repinned_post.save
      timeline = Timeline.add_post(current_user, @repinned_post)
      render_for_api :api_all_feeds, :json => timeline, :meta => {:error => false}, :root => :repinned_post
    else
      render :json => {:error => @repinned_post.errors.full_messages.join(", ")}
    end
  end
  
  protected  
  def find_post
    @post = Post.where(:_id => params[:id]).first
    render :json => {:error => 'Post not found'} unless @post
  end
end

