class Ios::Api::ApplicationController < Ios::ApplicationController
  skip_before_filter :verify_authenticity_token
  before_filter :ensure_secure_connection, :authenticate_user!
  
  protected
  def ensure_secure_connection
    return render :json => {:error => "empty key"} if params[:key].blank?
    return render :json => {:error => "empty key token"} if params[:key_token].blank?
    expected_token = Digest::MD5.hexdigest((params[:key] + Settings.secret_key))
    return render :json => {:error => "invalid key token"} if expected_token != params[:key_token]
  end
end
