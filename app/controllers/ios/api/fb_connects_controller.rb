class Ios::Api::FbConnectsController < Ios::Api::ApplicationController
  skip_before_filter :authenticate_user!
  
  def create
    if params[:user][:facebook_id].blank?
      render :json => {:error => 'Facebook id can not be blank.'}
    else
      if @user = User.where(:facebook_id => params[:user][:facebook_id]).first
        @user.reset_authentication_token!
        render :json => {:error => false, :auth_token => @user.authentication_token, :user => {:id => @user.id, :username => @user.username, :profile_image => Settings.app_url+'assets/'+@user.profile_image_50}}
      else
        render :json => {:error => 'Account not exists.'}
      end
    end
  end
end
