class Ios::Api::UsersController < Ios::Api::ApplicationController  
  skip_before_filter :authenticate_user!, :except => [:test]
  
  def create
    user = User.new(params[:user])
    # facebook_id, token
    if user.save
      render :json => {:error => false, :auth_token => user.authentication_token, :user => {:id => user.id}}
    else
      render :json => {:error => user.errors.full_messages.join(", ")}
    end
  end
  
  def login
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    if user = User.where(:email => params[:user][:email]).first
      if user.valid_password?(params[:user][:password]) && user.update_attribute(:devise_token, params[:user][:devise_token])
        user.reset_authentication_token!
        render :json => {:error => false, :auth_token => user.authentication_token, :user => {:id => user.id, :username => user.username, :profile_image => Settings.app_url+"assets/"+user.profile_image_50}}
      else
        render :json => {:error => 'Invalid password.'}
      end
    else
      render :json => {:error => 'Invalid email or devise token.'}
    end
  end
  
  def new
    render :json => {:error => false}
  end
  
  def test
    render :json => {:login_user_id => current_user.try(:id)}
  end  
  
  def logout
    Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name)
    render :json => {:error => false}
  end
end

