class Ios::Api::LikesController < Ios::Api::ApplicationController
  before_filter :find_post
  
  def create
    if !params[:like_action]
      render :json => {:error => 'Missing action'}
    elsif params[:like_action] == 'like'
      if current_user.like! @post
        render :json => {:error => false}
      else
        render :json => {:error => 'Duplicate record'}
      end
    else params[:like_action] == 'unlike'
      if current_user.unlike! @post
        render :json => {:error => false}
      else
        render :json => {:error => 'Duplicate record'}
      end
    end    
  end
  
  protected
  def find_post
    @post = Post.where(:_id => params[:post_id]).first
    render :json => {:error => 'Post not found'} unless @post
  end
end

