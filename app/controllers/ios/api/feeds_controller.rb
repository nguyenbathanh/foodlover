class Ios::Api::FeedsController < Ios::Api::ApplicationController
  def index
    feeds = if (!params[:last_post_id].blank? && params[:last_post_id] != '0')
      current_user.feeds.desc(:created_at).where(:_id.lt => params[:last_post_id]).limit(10)
    else
      current_user.feeds.desc(:created_at).limit(10)
    end
    render_for_api :api_all_feeds, :json => feeds, :meta => {:error => false}, :root => :feeds
  end
end

