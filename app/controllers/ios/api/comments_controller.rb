class Ios::Api::CommentsController < Ios::Api::ApplicationController
  before_filter :find_post
  
  def create
    if params[:comment].blank?
      render :json => {:error => 'Missing comment'}
    else
      comment = @post.comments.new(message: params[:comment], user_id: current_user.id)
      if comment.save
        render_for_api :api_created, :json => comment, :meta => {:error => false}, :root => :comment
      else
        render :json => {:error => comment.errors.full_messages.join(", ")}
      end
    end
  end
  
  protected
  def find_post
    @post = Post.where(:_id => params[:post_id]).first
    render :json => {:error => 'Post not found'} unless @post
  end
end

