class ConversationsController < ApplicationController
  def index
    redirect_to user_session_path if current_user.nil? # We need be on-line
    
    page = 1    
    page = Integer(params[:page].to_s) if !params[:page].nil?    

    if params[:keywords].nil? or params[:keywords] == "" 
      conversations = Conversation.author_active.list_for(current_user).desc(:updated_at).all.page(page).per(5)
    else  
      conversations = Conversation.search(params[:keywords]).author_active.list_for(current_user).desc(:updated_at).all.page(page).per(5)
    end
    @conversations = conversations
    
    respond_to do |format|
      format.json {render_for_api :all_conversations, json: conversations, status: 200}
      format.html {render :index, :layout => 'foodpedia', :layout => 'conversations'}
      format.js {}
    end
  end

  def show
    conversation = Conversation.find_by_slug(params[:id])
    conversation.mark_read! current_user
    messages = conversation.messages.desc(:create_at).all
    @conversation = conversation
    @messages = messages
    respond_to do |format|
      format.json {render_for_api :all_messages, json: messages, status: 200}
      format.html {render :show, :layout => 'foodpedia'}
    end
  end

  def create
    # check whether this is the first message in a new conversation
    if params[:conversation][:conversation_id]
      # old conversation
      if message = Conversation.append_message(current_user, params[:conversation])
        render_for_api :created, json: message, status: 200
      end
    else
      # new conversation
      if message = Conversation.first_message(current_user, params[:conversations])
        render_for_api :created, json: message.conversation, status: 200
        return
      end
    end
  end

  def recipient
    conversation = Conversation.find_by_slug(params[:id])
    if conversation
      recipient = conversation.check_sender_status current_user
      user = User.find(recipient)
      render_for_api :recipient, json: user, status: 200
    end
  end

  def read
    conversation = Conversation.find(params[:id])
    if conversation
      conversation.mark_read! current_user
      render json: {}, status: 200
    end
  end
end
