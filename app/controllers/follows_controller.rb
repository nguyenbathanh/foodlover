class FollowsController < ApplicationController
  before_filter :authenticate_user!, :except => [:following, :follower]

  def create
    entity = Entity.find_by_slug(params[:slug])
    if current_user.follow! entity
      render :json => entity
    else
      head :bad_requests
    end
  end

  def destroy
    entity = Entity.find_by_slug(params[:slug])
    if current_user.unfollow! entity
      render :json => {:status => '200'}
    else
      head :bad_requests
    end
  end

  def following
    user = User.find_by_slug(params[:slug]) || User.where(:username => params[:slug]).first
    following = user.following
    @following = following
    respond_to do |format|
      format.json {render_for_api :foodies, json: following , status: 200}
      format.html {render :following, :layout => 'followers'}
    end
  end

  def follower
    user = User.find_by_slug(params[:slug]) || User.where(:username => params[:slug]).first
    followers = user.followers
    @followers = followers
    respond_to do |format|
      format.json {render_for_api :foodies, json: followers , status: 200}
      format.html {render :followers, :layout => 'followers'}
    end
  end
end
