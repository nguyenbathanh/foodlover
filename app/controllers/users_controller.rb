class UsersController < ApplicationController
  before_filter :find_user, :except => [:you, :revoke_twitter]
  
  def search
    respond_to do |format|
      users = User.lookup(params[:term]).to_a
      users.delete current_user # Remove the current user from the list
      format.json {render_for_api :seach, json: users}
    end
  end
  
  def show
    render_for_api :show, json: @user, status: 200
  end

  def you
    render_for_api :you, json: current_user, status: 200
  end
  
  def edit
    render :layout => nil # to be rendered in iframe, so we ignore layout
  end
  
  def update
    params[:user].delete(:password) if params[:user][:password].blank? # ignore password if blank
    @user.attributes = params[:user]
    if @user.save
      sign_in @user, :bypass => true
      render :layout => nil # render js to update the backbone
    else
      render :action => :edit
    end
  end
  
  def revoke_twitter
    return unless user_signed_in?
    current_user.update_attributes({:twitter_oauth_token => nil, :twitter_oauth_secret => nil})
    redirect_to edit_user_url(:id => current_user.slug)
  end
  
  protected
  def find_user
    @user = User.find_by_slug(params[:id])
  end
end
