class PhotosController < ApplicationController


  def update_album_position
    positions = params[:positions]

    positions.each_with_index do |album_name, index|
      album = current_user.albums.where(:name => album_name).first

      unless album
        raise "no album"
      end

      Albumposition.where(:user_id => current_user.id, :album_id => album.id).delete_all
      Albumposition.create!(:user_id => current_user.id, :album_id => album.id, :position => index)
    end

    render :nothing => true
  end


  # /:slug/album/:tag
  # Display all photos in the album :tag (the albmus are tags inside the model Photo, album_tag_list)
  def index
    if params[:tag] =~ /[{}\(\)\^$&._%#!@=<>:;,~\`\'\"\s\t\*\?\/\+\|\[\\\]]/
      # If the url is like "coca_cola", "coca cola" or "coca$cola·$$%&&&" then, we redirect to "coca-cola"
      redirect_to photos_album_path(params[:slug], params[:tag].gsub(/[{}\(\)\^$&._%#!@=<>:;,~\`\'\"\s\t\*\?\/\+\|\[\\\]\-]/, ' ').strip.gsub(/[\s]+/, '-'))
    elsif params[:tag] =~ /[\-]{2,}/
      redirect_to photos_album_path(params[:slug], params[:tag].gsub(/[\-]{2,}/, '-'))
    else
      user = User.find_by_slug(params[:slug]) || User.where(:username => params[:slug]).first

      album_id = Album.where(:name => params[:tag].gsub(/[{}\(\)\^$&._%#!@=<>:;,~\`\'\"\s\t\*\?\/\+\|\[\\\]]/, ' ').strip, :user_id => user.id).first.try(:id)


      posts = Post.where(:album_ids.in => [album_id]).all

#       posts = if params[:tag]
#         user.posts.tagged_with(params[:tag].gsub(/[^a-zA-Z0-9]/, ' ').strip, :on => :album)
#       else
#         user.posts.where(:album_tag_list => nil).all
#       end
      @posts = posts
      if posts.size != 0 and not album_id.nil?
        respond_to do |format|
          format.json { render_for_api :photos, json: posts, status: 200 }
          format.html { render :index, :layout => 'foodpedia' }
        end
      else
        # If the album not exist, we return a 404 page
        render :text => "Album not found", :status => 404
      end
    end
  end

  def new
  end

  def photos
    user = User.find_by_slug(params[:slug]) || User.where(:username => params[:slug]).first

    positions = Albumposition.where(:user => user)
    #album_names = Album.where(:user_id => user.id).all
    #albums = []
    #album_names.each do |album|
    #  image = album.posts.first
    #  image_url = album.posts.first.image_130
    #  count = album.posts.count
    #  album_slug = album.name.parameterize
    #  albums.push({:id => image.id, :image_130 => image_url, :count => count, :album_name => album.name, :album_slug => album_slug, :owner_name => user.slug})
    #end
    #@user = user
    #@albums = albums
    @albums = user.albums
    @photos = user.posts

    #---almazom code for albums and posts collection

    @selected_user_albums = user.albums


    #----


    #respond_to do |format|
    #  format.json { render :json => albums }
    #  format.html {render :albums, :layout => 'foodpedia'}
    #end


    respond_to do |format|
      format.json { render :json => albums }
      format.html {
        render :albums,
               :layout => 'foodpedia',
               :locals => {
                   selected_user: user,
                   positions: Albumposition.where(:user_id => user.id)
               }
      }
    end
  end

  def albums
    user = User.find_by_slug(params[:slug]) || User.where(:username => params[:slug]).first
    album_names = Album.where(:user_id => u.id).all
#     album_names = user.posts.all.only(:album_tag_list).collect{ |ms| ms.album_tag_list}.flatten.uniq.compact
    albums = []
    album_names.each do |album|
      image = album.posts.first.image_130
#       image = user.posts.tagged_with(name.to_sym, :on => :album).first.image_130
      count = album.posts.count
#       count = user.posts.tagged_with(name, :on => :album).count
      album_slug = album.name.parameterize
#       album_slug = name.parameterize
      albums.push({:image_130 => image, :count => count, :album_name => album.name, :album_slug => album_slug, :owner_name => user.slug})
    end
    @user = user
    @albums = albums
    respond_to do |format|
      format.json { render :json => albums }
      format.html { render :albums, :layout => 'foodpedia' }
    end
  end

  # :slug/albums/:tag/update
  # When we update the name we create a new tag :album_tag_list with the name :new_tab_name and we delete the older tag name.
  # After update the album name, we redirect to the new album
  # Te name only accept /^[a-zA-Z0-9 ]+$/
  def update_album_name


    # If we can't rename the album
    if not current_user.nil? and (current_user.slug != params[:slug])
      redirect_to :back, error: "You can't rename this album"
    else
      user = User.find_by_slug(params[:slug]) || User.where(:username => params[:slug]).first
      album = posts = Album.where(:name => params[:tag].gsub(/[^a-zA-Z0-9]/, ' ').strip, :user_id => user.id).first
      album.name = params[:new_tag_name].gsub(/[^a-zA-Z0-9]/, ' ').strip
      album.save

#       posts = if params[:tag]
#         user.posts.tagged_with(params[:tag].gsub(/[^a-zA-Z0-9]/, ' ').strip, :on => :album)
#       else
#         user.posts.where(:album_tag_list => nil).all
#       end
#       posts.each do |post|
#         tags = post.albums
#         tags.delete(params[:tag].gsub(/[^a-zA-Z0-9]/, ' ').strip)
#         tags << params[:new_tag_name].gsub(/[^a-zA-Z0-9]/, ' ').strip
#         post[:album_tag_list] = tags
#         
#         post.save
#       end


      if  params[:controller] == "photos"
        redirect_to user_photos_path(clear_slug(params[:slug]))
      else
        redirect_to photos_album_path(params[:slug], params[:new_tag_name].gsub(/[^a-zA-Z0-9]/, '-').gsub(/\-*$/, '').gsub(/^\-*/, '').strip)
      end
    end
  end


  def update_dashboard
    render :partial => "photos/album_blocks", :locals => {selected_user: current_user, positions: Albumposition.where(:user_id => current_user.id)}
  end

  # :slug/albums/:tag/remove
  # Remove the album :tag from the user with the slug name :slug.
  # The album is a tag inside of post class, when we remove the album, we remove the tag :tag from the :album_tag_list.
  # After remove the album, we redirect to :slug/photos
  def remove_album
    # If we can't remve the album
    if not current_user.nil? and (current_user.slug != params[:slug])
      redirect_to :back, error: "You can't remove this album"
    else
      #album = Album.where(:name => params[:tag], :user_id => current_user.id)
      #album.delete_all


      user = User.find_by_slug(params[:slug]) || User.where(:username => params[:slug]).first
      album = Album.where(:name => clear_slug(params[:tag]), :user_id => user.id).first
      if album.present?
        orphan_posts = album.posts
        default_album = user.albums.where(:name => "Default").first
        orphan_posts.each do |post|
          default_album.posts << post
        end

        if album.destroy
          Albumposition.where(:user_id => current_user.username, :album_id => album.id).delete_all
        end
      else
        redirect_to :back, :notice => "Album not exist!" and return
      end

#       posts = if params[:tag]
#         user.posts.tagged_with(params[:tag].gsub(/[^a-zA-Z0-9]/, ' ').strip, :on => :album)
#       else
#         user.posts.where(:album_tag_list => nil).all
#       end
#       posts.each do |post|
#         tags = post.albums
#         tags.delete(params[:tag].gsub(/[^a-zA-Z0-9]/, ' ').strip)
#         post[:album_tag_list] = tags
# 
#         post.save
#       end
      redirect_to user_photos_path
    end
  end
end
