
class PostsController < ApplicationController
  inherit_resources

  actions :create

  before_filter :find_post, :only => [:destroy, :update, :reshare, :repin, :flag, :promote]

  def index
    page = 1
    if !params[:page].nil?
      page = Integer(params[:page].to_s)
    end
    
    sorted =  current_user.feeds.sort_by { |obj| obj.created_at }
    
    
    feeds = Kaminari.paginate_array(sorted.reverse).page(page).per(10)   
    render_for_api :all_feeds, json: feeds, status: 200
  end

  def show
    post = Post.find(params[:id])
    @post = post
    respond_to do |format|
      format.json { render_for_api :post, json: post, status: 200 }
      format.html { render :show, :layout => 'photo' }
    end
  end
  
  def last_since
    id = params[:id]
    
    newfeeds = []

    sorted =  current_user.feeds.sort_by { |obj| obj.created_at }
    found = false
    sorted.reverse.each do |feed|

      if (!feed.subject.id.to_s.eql?(id.to_s))
        newfeeds.push(feed)
      else
        found = true;
        break
      end
    end
    
    if !found
      newfeeds = []
    end
      
    render_for_api :all_feeds, json: newfeeds, status: 200
    #render :json => {:error=> '', :feeds=> newfeeds }
  end

  def new
  end

  #
  #def create
  #  post = Post.new params["post"]
  #  post.user_id = current_user.id
  #  tag_list = params["post"]["album_tag_list"].split(",")
  #
  #  tag_list.each do |tag|
  #    tag_existance = Tagposition.where(:user_id => current_user.username, :tag_name => tag)
  #    unless tag_existance.any?
  #      #new_position = Tagposition.where(:user_id => "almazom").map(&:position).max
  #      Tagposition.create!(:user_id => current_user.username, :tag_name => tag, :position => 500)
  #    end
  #  end
  #
  #  if params["post"]["album_tag_list"].length > 3
  #    post.tags = params["post"]["album_tag_list"]
  #  else
  #    post.tags = "default"
  #  end
  #
  #  if post.save
  #    render_for_api :default, json: post
  #  else
  #    render json: resource.errors, status: :unprocessable_entity
  #  end
  #
  #end
  #


  def create

    #raise params.to_json

    post = Post.new params["post"]
    post.user_id = current_user.id
    post[:album_tag_list] = []
    # Search the album, if don't exist, we create it

    album_names_list = []
    
    if !params["post"]["album_tag_list"].nil?
      album_names_list = params["post"]["album_tag_list"].split(",")
    end
    

    #if album_names_list.length < 1
    #  album_names_list = "default"
    #end

    if post.save
      if params["post"]["album_tag_list"].present? && album_names_list.length > 0


        album_names_list.each do |album_name|
          album = Album.where(:name => clear_slug(album_name), :user_id => current_user.id).first

          if album.blank?
            #  create new album and Albumposition
            new_album = current_user.albums.new
            new_album.name = album_name

            if new_album.save
              #position of new album

              #user = current_user.username
              begin
                last_position = Albumposition.where(:user_id => current_user.id) ? (Albumposition.where(:user_id => current_user.id).map(&:position).max)+1 : 0
              rescue NoMethodError
                last_position = 0
              end

              p ap = Albumposition.new
              p ap.user = current_user
              p ap.album = new_album
              p ap.position = last_position
              unless ap.save
                render :js => "alert('error saving')" and return
              end

              #add post to album
              post.albums << new_album
            else
              logger.debug("can not save")
            end

          else
            #no need to create ALBUM
            album.posts << post
          end
        end
      else
        default_album = current_user.albums.where(:name => "Default").first

        if default_album
          #raise "default exists"
          default_album.posts << post

        else

          new_default_album = current_user.albums.create!(:name => "Default")

          ap = Albumposition.new
          ap.user = current_user
          p ap.album = new_default_album
          p ap.position = 500
          ap.save

          new_default_album.posts << post
        end
      end


      # TODO: Add "With who"
      params[:shared_users].split(',').each do |user_slug|
        if current_user.slug != user_slug.strip # Only we can share with non-current user
          user = User.find_by_slug user_slug.strip
          if not user.nil? # Only add non-nil users
            post.shared_users << user
            # Now we notificate to the user
            notificatioin = Notification.new(:event_type => 'being_tagged', :subject => post)
            notificatioin.save
            puts "==========> #{user.class} -- #{post.class}"
            user.notifications << notificatioin
          end
        end
      end

      # At Where
      location = Location.where(:name => params[:location]).first
      if location.nil?
        location = Location.new :name => params[:location]
        location.save
      end
      
      
      post.location_id = location.id if not location.nil?
      post.save
      
      render_for_api :default, json: post
    else
      begin
        render json: resource.errors, status: :unprocessable_entity
      rescue Mongoid::Errors::InvalidFind
        render :js => "alert('Error, can not add post!')"
      end

    end #post.save


#if post.save
#  # Now, if the user want to share with facebook or twitter, we do it
#  if params[:share_facebook].to_i == 1
#    current_user.publish_to_facebook(post)
#  end
#  if params[:share_twitter].to_i == 1
#    current_user.publish_to_twitter(post)
#  end
#
#  if album
#    #position of new album
#    user = current_user.username
#
#    album_exist_check = Albumposition.where(:user_id => user, :album_id => album.id)
#    unless album_exist_check.any?
#      last_position = Albumposition.where(:user => user).any? ? (Albumposition.where(:user => user).map(&:position).max)+1 : 0
#      Albumposition.create!(:user_id => user, :album_id => album.id, :position => last_position)
#    end
#  end
#
#
#  render_for_api :default, json: post
#else
#  render json: resource.errors, status: :unprocessable_entity
#end

  end

  def others
    user = User.find_by_slug(params[:slug])
    feeds = user.feeds.desc(:created_at).all.reverse

    render_for_api :all_feeds, json: feeds, status: 200
  end

  def destroy
    @post.destroy
    render :json => @post
  end

  def update
    params[:post].delete(:comments)
    @post.update_attributes(params[:post])
    render_for_api :post_feed, json: @post, status: 200
  end

  def tagged_with
    posts = Post.tagged_with(params[:tag].gsub('-', ' '), :on => 'food')
    render_for_api :all_posts, json: posts, status: 200
  end

  def reshare
    if current_user.reshare(@post)
      head :created
    else
      head :bad_requests
    end
  end


  
  
  def repin
    if request.get?
      @post.message = ''
      render :layout => false
    else
      @oldpost = Post.find(params[:post][:id])
      
      if params[:select_album_flag].to_s.eql? 'true'
        album_tag = params[:post][:album_tag_list]
      
      else
        new_album = current_user.albums.new
        new_album.name = params[:new_album]

        new_album.save
        album_tag = new_album.name
      end
      @repinned_post = current_user.posts.new(params[:post])
      @repinned_post.message += " (#{album_tag})" if !album_tag.blank?
      @repinned_post.picture = @oldpost.picture
      @repinned_post.food_names = @oldpost.food_names
      @repinned_post.repinned_post = @oldpost
      if @repinned_post.save
        Timeline.add_post(current_user, @repinned_post)
      end
      
      render :template => '/posts/repin_post'
    end
  end

  def flag
    if request.get?
      @flag = @post.flags.new
      render :layout => false
    else
      @flag = @post.flags.new(params[:flag])
      @flag.user_id = current_user.id
      @flag.save
      render :template => '/posts/flag_post'
    end
  end

  def promote
    if current_user.promote!(@post)
      head :created
    else
      head :bad_requests
    end
  end

  protected

  def find_post
    @post = Post.find(params[:id])
  end

  def begin_of_association_chain
    current_user
  end
end
