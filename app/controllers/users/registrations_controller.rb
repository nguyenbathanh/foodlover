class Users::RegistrationsController < Devise::RegistrationsController
  def new
    @facebook_id = session["devise.facebook_id"]
    @token = session["devise.facebook_token"]
    @email = session["devise.facebook_email"]
    
    @weibo_uid = session["devise.weibo_uid"]
    @weibo_access_token = session["devise.weibo_access_token"]
    @weibo_access_token_secret = session["devise.weibo_access_token_secret"]
    
    @tqq_uid = session["devise.tqq_uid"]
    @tqq_access_token = session["devise.tqq_access_token"]
    @tqq_access_token_secret = session["devise.tqq_access_token_secret"]

    build_resource({})
    render :new
  end

  def create
    build_resource
    if session["devise.facebook_token"]
      resource.profile_image = open("http://graph.facebook.com/#{params[:user][:facebook_id]}/picture").read
    end
    
    if inviter = User.find(cookies['invited_by'])
      resource.inviter = inviter
    end if cookies['invited_by']

    if resource.save
      if resource.active_for_authentication?
        set_flash_message :notice, :signed_up if is_navigational_format?
        sign_in(resource_name, resource)
        respond_with resource, :location => after_sign_up_path_for(resource)
      else
        set_flash_message :notice, :inactive_signed_up, :reason => inactive_reason(resource) if is_navigational_format?
        expire_session_data_after_sign_in!
        respond_with resource, :location => after_inactive_sign_up_path_for(resource)
      end
    else
      clean_up_passwords(resource)
      respond_with_navigational(resource) { render :new }
    end
  end
end

