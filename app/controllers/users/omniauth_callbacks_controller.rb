class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  after_filter :set_last_login_country, :only => [:facebook]
  
  def passthru
    session[:return_to] = params[:redirect_uri]
    render :file => "#{Rails.root}/public/404.html", :status => 404, :layout => false
  end
  
  def failure
    set_flash_message :alert, :failure, :kind => failed_strategy.name.to_s.humanize, :reason => failure_message
    redirect_to session[:return_to] || root_url
  end
  
  def facebook
    @user = User.find_for_facebook_oauth(env["omniauth.auth"])
    if @user
      unless @user.active_for_authentication?
        render
      else
        # We save the facebook toke, this give us permission to share with facebook
        @user.update_attribute(:token, env['omniauth.auth']['credentials']['token'])
        sign_in :user, @user, :event => :authentication
      end
    else
      if user_signed_in?
        current_user.update_attributes({:token => env['omniauth.auth']['credentials']['token'], :facebook_id => env["omniauth.auth"]["uid"]})
      else
        session["devise.facebook_id"] = env['omniauth.auth']['uid'] || ''
        session["devise.facebook_token"] = env['omniauth.auth']['credentials']['token'] || ''
        session["devise.facebook_email"] = env['omniauth.auth']['info']['email'] || ''
      end
    end
    if session[:return_to]
      url = session[:return_to]
      session[:return_to] = nil
      redirect_to url
    else
      render
    end
  end
  
  def twitter
    if user_signed_in?
      current_user.update_attributes({
        :twitter_oauth_token => env["omniauth.auth"]['credentials']['token'], 
        :twitter_oauth_secret => env["omniauth.auth"]['credentials']['secret']
      })
    end    
    redirect_to v2_account_url
  end
  
  # Will try to ask permission from client for refactor the various callback into IF he request more omniauth handler
  def weibo
    @user = User.find_for_weibo_oauth(env["omniauth.auth"])
    if @user
      unless @user.active_for_authentication?
        render
      else
        @user.update_attributes({:weibo_access_token => env["omniauth.auth"]["credentials"]["token"],
          :weibo_access_token_secret => env["omniauth.auth"]["credentials"]["secret"]
        })
        sign_in :user, @user, :event => :authentication
      end
    else
      if user_signed_in?
        current_user.update_attributes({:weibo_access_token => env["omniauth.auth"]["credentials"]["token"],
          :weibo_access_token_secret => env["omniauth.auth"]["credentials"]["secret"],
          :weibo_uid => env["omniauth.auth"]["uid"]
        })
      else
        session["devise.weibo_uid"] = env['omniauth.auth']['uid'] || ''
        session["devise.weibo_access_token"] = env['omniauth.auth']['credentials']['token'] || ''
        session["devise.weibo_access_token_secret"] = env['omniauth.auth']['credentials']['secret'] || ''
      end
    end
    if session[:return_to]
      url = session[:return_to]
      session[:return_to] = nil
      redirect_to url
    else
      render 'facebook'
    end
  end
  
  def tqq
    @user = User.find_for_tqq_oauth(env["omniauth.auth"])
    if @user
      unless @user.active_for_authentication?
        render
      else
        @user.update_attributes({:tqq_access_token => env["omniauth.auth"]["credentials"]["token"], 
          :tqq_access_token_secret => env["omniauth.auth"]["credentials"]["secret"]
        })
        sign_in :user, @user, :event => :authentication
      end
    else
      if user_signed_in?
        current_user.update_attributes({:tqq_access_token => env["omniauth.auth"]["credentials"]["token"], 
          :tqq_access_token_secret => env["omniauth.auth"]["credentials"]["secret"], 
          :tqq_uid => env["omniauth.auth"]["uid"]
        })
      else
        session["devise.tqq_uid"] = env['omniauth.auth']['extra']['user_hash']['data']['openid'] || ''
        session["devise.tqq_access_token"] = env['omniauth.auth']['credentials']['token'] || ''
        session["devise.tqq_access_token_secret"] = env['omniauth.auth']['credentials']['secret'] || ''
      end
    end
    if session[:return_to]
      url = session[:return_to]
      session[:return_to] = nil
      redirect_to url
    else
      render 'facebook'
    end
  end
end

