class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_current_user

  def set_current_user
    User.current = current_user
  end

  def set_last_login_country
    return unless user_signed_in?
    current_user.update_attribute(:country, request.location.data['country_name'])
  end

  def clear_slug slug
    slug.gsub(/[^a-zA-Z0-9]/, ' ').strip
  end

  helper_method :clear_slug
end
