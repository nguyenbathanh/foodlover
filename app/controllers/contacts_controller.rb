class ContactsController < ApplicationController
  before_filter :authenticate_user!
  
  def callback
    @contacts = request.env['omnicontacts.contacts']
    @contacts.each do |contact|
      current_user.send(params[:importer].pluralize).create({:name => contact[:name], :email => contact[:email]}) unless contact[:email].blank?
    end
    current_user.update_attribute("#{params[:importer]}_authenticated", true) if !@contacts.blank?
    redirect_to provider_invite_path(params[:importer])
  end
  
  def disconnect
    current_user.update_attribute("#{params[:importer]}_authenticated", false)
    current_user.send(params[:importer].pluralize).delete_all
    redirect_to provider_invite_path(params[:importer])
  end
  
  def invite
    emails = params[:invites][:email].select {|x| !x.blank? && x =~ /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
    if emails.blank?
      redirect_to :back, notice: "Please enter at least 1 valid email address."
    else
      emails.each do |e|
        UserMailer.invitation(e, current_user, params[:invites][:note]).deliver
      end
      respond_to do |format|
        format.html { redirect_to :back, notice: "Inviation sent." }
        format.js do
          render
        end
      end
    end
  end
end