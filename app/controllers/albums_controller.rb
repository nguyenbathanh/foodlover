class AlbumsController < ApplicationController
  def search
    if current_user.nil?
      render :json => [].to_json
    else
      @albums = Album.where(:user_id => current_user.id, :name => /#{params[:term]}/).all
      
      render :json => @albums.collect{|a| {:label => a.name} }.to_json
    end
  end
end

