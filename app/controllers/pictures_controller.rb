class PicturesController < ApplicationController
  inherit_resources

  layout "foodpedia"

  def create
    create! do |success, failure|
      success.json { render_for_api :default, json: resource }
      failure.json { render text: "Error uploading image", status: :unprocessable_entity }
    end
  end

  protected

  def begin_of_association_chain
    current_user
  end
end
