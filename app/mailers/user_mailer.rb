class UserMailer < ActionMailer::Base
  default from: "admin@imfoodlover.com"
  
  include ApplicationHelper # include helper methods into this class
  helper ApplicationHelper # include helper methods into views
  include ActionView::Helpers::DateHelper
  include ActionView::Helpers::NumberHelper
  
  def invitation(email, user, note='')
    @note = note
    @user = user
    mail(:to => email, :subject => "#{user.username} has invited you to join ImFoodLover.com")
  end
  
  def custom(recipient, title, note)
    @recipient = recipient
    @title = title
    @note = note
    mail(:to => recipient.email, :subject => "#{title} From ImFoodLover.com")
  end
  
  def being_tagged(recipient, post)
    @user = recipient
    @post = post
    mail(:to => recipient.email, :subject => "#{post.user.slug} tagged you in a post on ImFoodLover")
  end
  
  def being_followed(recipient, follower)
    @user = recipient
    @follower = follower
    mail(:to => recipient.email, :subject => "#{follower.username} is now following you on ImFoodLover")
  end
  
  def being_liked(recipient, likeship)
    @user = recipient
    @likeship = likeship
    @post = Post.find(likeship.target_id)
    mail(:to => recipient.email, :subject => "#{likeship.user.slug} loved your post on ImFoodLover")
  end
  
  def being_commented(recipient, post, comment)
    @commentor = comment.user
    @user = recipient
    @post = post
    mail(:to => recipient.email, :subject => "#{@commentor.slug} commented on your post on ImFoodLover")
  end
  
  def being_repinned(recipient, post, repinner)
    @user = recipient
    @post = post
    @repinner = repinner
    mail(:to => recipient.email, :subject => "#{repinner.slug} repinned your photo on ImFoodLover")
  end
  
  def being_shared(recipient, post, sharer)
    @user = recipient
    @post = post
    @sharer = sharer
    mail(:to => recipient.email, :subject => "#{sharer.slug} shared your post on ImFoodLover")
  end
end
