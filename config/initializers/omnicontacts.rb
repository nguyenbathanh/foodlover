require "omnicontacts"

Rails.application.middleware.use OmniContacts::Builder do
  importer :gmail, Settings.gmail_id, Settings.gmail_token, {:redirect_path => "/contacts/gmail/callback"}
  importer :yahoo, Settings.yahoo_id, Settings.yahoo_token, {:callback_path => '/contacts/yahoo/callback'}
  importer :hotmail, Settings.hotmail_id, Settings.hotmail_token, {:redirect_path => "/contacts/hotmail/callback"}
end