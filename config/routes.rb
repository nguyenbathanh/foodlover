Foodstreet::Application.routes.draw do
  root :to => "home#index"
  match '/d/' => "home#show", :as => :dashboard_show

  devise_for :users, :controllers => {
    omniauth_callbacks: 'users/omniauth_callbacks',
    registrations: 'users/registrations',
    sessions: 'users/sessions'
  }, :skip => [:sessions]

  devise_scope :user do
    get '/signup' => 'users/registrations#new', as: :new_user_registration
    get '/logout' => 'users/sessions#logout', as: :destroy_user_session
    post '/login' => 'users/sessions#create', :as => :user_session
    get '/login' => 'users/sessions#new', :as => :new_user_session
    get '/users/auth/:provider' => 'users/omniauth_callbacks#passthru'
    get '/current_user/update_dashboard' => 'photos#update_dashboard', :as => :update_dashboard

  end

  resources :users, only: [:show, :edit, :update] do
    collection do
      get 'you'
      get 'search'
    end
    member do
      get 'revoke_twitter'
    end
  end
  
  resource :follow, only: [] do
    collection do
      get 'following'
      get 'follower'
    end
  end

  resource :search, only: [] do
    collection do
      get 'username'
    end
  end

  #resources :notifications, :only => [:index]
  resources :photos do
    collection do
      get "albums"
    end
  end
  
  resources :outlets do
    collection do
      get 'featured'
      get 'promotion'
    end
  end
  resource :follow, only: [:create, :destroy]
  resource :like,   only: [:create, :destroy]
  resources :comments, only: [:create, :destroy, :index]

  resources :conversations do
    member do
      get 'recipient'
    end
    collection do
      get 'read'
    end
  end

  resources :search do
    collection do
      get 'tags'
      get 'locations'
    end
  end

  resources :feed, only: [:index]

  # devise_scope :user do
  namespace :ios, :defaults => {:format => 'json'} do
    namespace :api do
      resources :users, :only => [:create, :new] do
        collection do
          post 'login'
          get 'test'
          get 'logout'
        end
      end
      resources :fb_connects, :only => [:create]
      resources :posts, :only => [:create, :destroy, :update] do
        member do
          post 'reshare'
          post 'repin'
        end
      end
      resources :feeds, :only => [:index]
      resources :likes, :only => [:create]
      resources :comments, :only => [:create]
    end
  end
  # end

  match 'invites/email_form' => 'invites#email_form', :as => :invite_email
  match 'invites/:provider' => 'invites#provider', :as => :provider_invite
  match 'invites/:provider/loads' => 'invites#loads', :as => :invite_loads

  match 'contacts/:importer/callback' => 'contacts#callback', :as => :contacts_callback
  match 'contacts/:importer/disconnect' => 'contacts#disconnect', :as => :contacts_disconnect
  match 'contacts/invite' => 'contacts#invite', :as => :contacts_invite

  match 'posts/last_since/:id' => 'posts#last_since', :as => :posts_last_since
  match 'post/:id' => 'posts#show', :as => :single_post
  match ':slug/albums/:tag' => 'photos#index', :as => :photos_album
  match ':slug/albums/:tag/remove' => 'photos#remove_album', :as => :remove_photos_album
  match ':slug/albums/:tag/update' => 'photos#update_album_name', :as => :update_photos_album
  #   match ':slug/followers' => 'follows#follower', :as => :follower_of
  #   match ':slug/following' => 'follows#following', :as => :followig_to
  match 'inbox' => 'conversations#index', :as => :inbox
  match ':slug/photos' => 'photos#photos', :as => :user_photos

  namespace :admin do
    resources :users do
      collection do
        post 'manage_status'
      end
    end
    resources :posts
    resources :badges
  end

  scope :module => 'v2', :as => 'v2' do #;for future switch; to maintain same name helper, controllers but with root path
    resource :account, :only => [:show, :update] do
      collection do
        get 'revoke_twitter'
      end
    end
    resources :notifications, :only => [:index] do
      collection do
        get 'list'
      end
    end
    resources :activities, :only => [:index]
  end

  # scope :module => 'v2', :as => 'v2' do #;for future switch; to maintain same name helper, controllers but with root path
  namespace :v2 do
    root :to => "home#show"
    resource :comments, only: [:create, :destroy, :index]
    resources :feeds, :only => [:index]
    resources :posts do
      collection do
        get 'tagged_with/:tag' => 'posts#tagged_with', :as => :tagged_with
      end
    end
    resources :food_tags, :only => [:index]
    match ':slug' => 'users#show'
    match ':slug/invitation' => 'users#invitation', :as => "user_invitation"
  end

  resources :tag_subscriptions, :only => [:create, :destroy, :update]

  # Foodpedia and Post related

  match '/posts/tagged_with' => 'posts#tagged_with', as: :posts_tagged_with
  resources :posts do
    collection do
      get 'others'
    end
    member do
      post 'reshare'
      match 'repin'
      match 'flag'
      match 'promote'
    end
  end
  resources :pictures
  resources :foods
  resource :account

  # Any other URL is used like the user home page
  match ':slug', :controller=>"v2/users", :action=>"show", :as => :slug
  match ':slug/activities', :controller=>"v2/users", :action=>"activities", :as => :user_activities
  match ':slug/about', :controller=>"v2/users", :action=>"about", :as => :user_about
  match ':slug/subscriptions', :controller=>"v2/users", :action=>"subscriptions", :as => :user_subscriptions
  match ':slug/badges', :controller=>"v2/users", :action=>"badges", :as => :user_badges
  match ':slug/followers', :controller=>"v2/users", :action=>"followers", :as => :user_followers
  match ':slug/following', :controller=>"v2/users", :action=>"following", :as => :user_following
  match 'albums/search' => "albums#search"
  match 'location/search/:lat/:long' => "locations#search", :constraints => {:lat => /-?[0-9]+(\.[0-9]+)?/, :long => /-?[0-9]+(\.[0-9]+)?/}
  match 'current_user/update_album_position' => 'photos#update_album_position'

end
